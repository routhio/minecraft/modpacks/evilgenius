# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading bibliocraft.zs");
	
# Bibliocraft Clipboard
recipes.remove(<bibliocraft:biblioclipboard>);
	
# Creative Bookcase
mods.extendedcrafting.TableCrafting.addShaped(0, <bibliocraft:bookcasecreative>, 
	[[<ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>], 
	 [<cyclicmagic:block_library_ctrl>, <cyclicmagic:block_library_ctrl>, <cyclicmagic:block_library_ctrl>, <cyclicmagic:block_library_ctrl>, <cyclicmagic:block_library_ctrl>], 
	 [<minecraft:enchanted_book>, <mysticalagradditions:stuff:69>, <bibliocraft:bigbook>, <mysticalagradditions:stuff:69>, <minecraft:enchanted_book>], 
	 [<cyclicmagic:block_library_ctrl>, <cyclicmagic:block_library_ctrl>, <cyclicmagic:block_library_ctrl>, <cyclicmagic:block_library_ctrl>, <cyclicmagic:block_library_ctrl>], 
	 [<ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>]]);

# Switching between plank type
recipes.addShapeless("Creative Bookcase0", <bibliocraft:bookcasecreative>, [<bibliocraft:bookcasecreative:6>]);
recipes.addShapeless("Creative Bookcase6", <bibliocraft:bookcasecreative:6>, [<bibliocraft:bookcasecreative:5>]);
recipes.addShapeless("Creative Bookcase5", <bibliocraft:bookcasecreative:5>, [<bibliocraft:bookcasecreative:4>]);
recipes.addShapeless("Creative Bookcase4", <bibliocraft:bookcasecreative:4>, [<bibliocraft:bookcasecreative:3>]);
recipes.addShapeless("Creative Bookcase3", <bibliocraft:bookcasecreative:3>, [<bibliocraft:bookcasecreative:2>]);
recipes.addShapeless("Creative Bookcase2", <bibliocraft:bookcasecreative:2>, [<bibliocraft:bookcasecreative:1>]);
recipes.addShapeless("Creative Bookcase1", <bibliocraft:bookcasecreative:1>, [<bibliocraft:bookcasecreative>]);

