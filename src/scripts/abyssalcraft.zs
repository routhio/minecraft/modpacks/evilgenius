# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading abyssalcraft.zs");

# Necronomicon
recipes.remove(<abyssalcraft:necronomicon>);
recipes.addShaped("Necronomicon", <abyssalcraft:necronomicon>.withTag({PotEnergy: 0.0 as float}),
    [[<bloodmagic:blood_shard>, <minecraft:rotten_flesh>, <bloodmagic:slate:3>],
     [<minecraft:rotten_flesh>, <minecraft:book>, <minecraft:rotten_flesh>],
     [<bloodmagic:blood_shard>, <minecraft:rotten_flesh>, <bloodmagic:slate:3>]]);