# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading cyclic.zs");

# Storage Bag
recipes.remove(<cyclicmagic:storage_bag>);
recipes.addShaped("Storage Bag", <cyclicmagic:storage_bag>,
    [[<ore:leather>, <ore:string>, <ore:leather>],
     [<ore:leather>, <ore:blockGold>, <ore:leather>],
     [<ore:leather>, <ore:blockRedstone>, <ore:leather>]]);

# Auto crafter
recipes.remove(<cyclicmagic:auto_crafter>);
recipes.addShaped("Cyclic Autocrafter", <cyclicmagic:auto_crafter>,
    [[<ore:dyePurple>, <ore:craftingTableWood>, <ore:dyePurple>],
     [<minecraft:piston>, <nuclearcraft:part:10>, <minecraft:observer>],
     [<ore:dyePurple>, <ore:blockBone>, <ore:dyePurple>]]);

# Automated User
recipes.remove(<cyclicmagic:block_user>);
recipes.addShaped("Cyclic Automated User", <cyclicmagic:block_user>,
    [[<ore:ingotGold>, <minecraft:dispenser>, <ore:ingotGold>],
     [null, <ore:blockMagma>, null],
     [<ore:obsidian>, <nuclearcraft:part:10>, <ore:obsidian>]]);

# Block Placer
recipes.remove(<cyclicmagic:placer_block>);
recipes.addShaped("Cyclic Block Placer", <cyclicmagic:placer_block>,
    [[<minecraft:dispenser>, <ore:stone>, <minecraft:dispenser>],
     [<ore:ingotIron>, <ore:dustRedstone>, <ore:ingotIron>],
     [<ore:cobblestone>, <nuclearcraft:part:10>, <ore:cobblestone>]]);

# Controlled Miner
recipes.remove(<cyclicmagic:block_miner_smart>);
recipes.addShaped("Cyclic Controlled Miner", <cyclicmagic:block_miner_smart>,
    [[<ore:blockLapis>, <minecraft:observer>, <ore:blockLapis>],
     [<ore:gemDiamond>, <ore:blockMagma>, <ore:gemDiamond>],
     [<ore:obsidian>, <nuclearcraft:part:10>, <ore:obsidian>]]);

# Structure Builder
recipes.remove(<cyclicmagic:builder_block>);
recipes.addShaped("Cyclic Structure Builder", <cyclicmagic:builder_block>,
    [[<ore:blockRedstone>, <minecraft:dispenser>, <ore:blockRedstone>],
     [<minecraft:observer>, <ore:blockMagma>, <minecraft:observer>],
     [<ore:obsidian>, <nuclearcraft:part:10>, <ore:obsidian>]]);

# Powered Enchanter
recipes.remove(<cyclicmagic:block_enchanter>);
recipes.addShaped("Cyclic Powered Enchanter", <cyclicmagic:block_enchanter>,
    [[null, <minecraft:enchanting_table>, null],
     [<ore:gemEmerald>, <ore:obsidian>, <ore:gemEmerald>],
     [<ore:obsidian>, <nuclearcraft:part:10>, <ore:obsidian>]]);

# Harvester
recipes.remove(<cyclicmagic:harvester_block>);
recipes.addShaped("Cyclic Harvester", <cyclicmagic:harvester_block>,
    [[<ore:gemEmerald>, <minecraft:dispenser>, <ore:gemEmerald>],
     [<ore:gemQuartz>, <ore:gemDiamond>, <ore:gemQuartz>],
     [<ore:obsidian>, <nuclearcraft:part:10>, <ore:obsidian>]]);

# Powered Diamond Anvil
recipes.remove(<cyclicmagic:block_anvil>);
recipes.addShaped("Cyclic Powered Diamond Anvil", <cyclicmagic:block_anvil>,
    [[<ore:gemDiamond>, <ore:gemDiamond>, <ore:gemDiamond>],
     [<ore:dustRedstone>, <minecraft:enchanting_table>, <ore:dustRedstone>],
     [<ore:blockIron>, <nuclearcraft:part:10>, <ore:blockIron>]]);

# Magma Anvil
recipes.remove(<cyclicmagic:block_anvil_magma>);
recipes.addShaped("Cyclic Magma Anvil", <cyclicmagic:block_anvil_magma>,
    [[<ore:blockMagma>, <ore:blockMagma>, <ore:blockMagma>],
     [<ore:blockGold>, <minecraft:anvil>, <ore:blockGold>],
     [<ore:blockIron>, <nuclearcraft:part:10>, <ore:blockIron>]]);

# Hydrator
recipes.remove(<cyclicmagic:block_hydrator>);
recipes.addShaped("Cyclic Hydrator", <cyclicmagic:block_hydrator>,
    [[<ore:listAllwater>, <minecraft:dropper>, <ore:listAllwater>],
     [<minecraft:clay>, <nuclearcraft:part:10>, <minecraft:clay>],
     [<minecraft:hardened_clay>, <minecraft:hardened_clay>, <minecraft:hardened_clay>]]);

# Block Miner
recipes.remove(<cyclicmagic:block_miner>);
recipes.addShaped("Cyclic Block Miner", <cyclicmagic:block_miner>,
    [[<ore:bone>, <minecraft:dispenser>, <ore:bone>],
     [null, <minecraft:iron_pickaxe>, null],
     [<ore:blockMossy>, <nuclearcraft:part:10>, <ore:blockMossy>]]);

# Item Collector
recipes.remove(<cyclicmagic:block_vacuum>);
recipes.addShaped("Cyclic Item Collector", <cyclicmagic:block_vacuum>,
    [[<ore:ingotIron>, <nuclearcraft:part:10>, <ore:ingotIron>],
     [<minecraft:dropper>, <ore:blockLapis>, <minecraft:dropper>],
     [<minecraft:hopper>, <minecraft:hopper>, <minecraft:hopper>]]);

# Interdiction Pulsar
recipes.remove(<cyclicmagic:magnet_anti_block>);
recipes.addShaped("Cyclic Interdiction Pulsar", <cyclicmagic:magnet_anti_block>,
    [[<ore:dyeBlue>, <ore:blockGlowstone>, <ore:dyeBlue>],
     [<ore:blockGlowstone>, <ore:gemDiamond>, <ore:blockGlowstone>],
     [<ore:dyeBlue>, <nuclearcraft:part:10>, <ore:dyeBlue>]]);

# Wireless Receiver
recipes.remove(<cyclicmagic:wireless_receiver>);
recipes.addShaped("Cyclic Wireless Receiver", <cyclicmagic:wireless_receiver>,
    [[<ore:ingotIron>, <ore:stone>, <ore:ingotIron>],
     [<ore:stone>, <minecraft:redstone_torch>, <ore:stone>],
     [<ore:ingotIron>, <nuclearcraft:part:10>, <ore:ingotIron>]]);

# Wireless Transmitter
recipes.remove(<cyclicmagic:wireless_transmitter>);
recipes.addShaped("Cyclic Wireless Transmitter", <cyclicmagic:wireless_transmitter>,
    [[<ore:ingotIron>, <ore:stone>, <ore:ingotIron>],
     [<ore:stone>, <minecraft:repeater>, <ore:stone>],
     [<ore:ingotIron>, <nuclearcraft:part:10>, <ore:ingotIron>]]);

# Fire Starter
recipes.remove(<cyclicmagic:fire_starter>);
recipes.addShaped("Cyclic Fire Starter", <cyclicmagic:fire_starter>,
    [[<minecraft:flint>, <minecraft:dispenser>, <minecraft:flint>],
     [<ore:nuggetIron>, <nuclearcraft:part:10>, <ore:nuggetIron>],
     [<ore:cobblestone>, <ore:cobblestone>, <ore:cobblestone>]]);
