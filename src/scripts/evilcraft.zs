# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#


import crafttweaker.item.IItemStack as IItemStack;
import mods.jei.JEI.removeAndHide as rh;

print(">>> loading evilcraft.zs");

# Darkened Apple
recipes.remove(<evilcraft:darkened_apple>);
mods.thaumcraft.ArcaneWorkbench.registerShapelessRecipe("Darkened Apple", "",
    10, [], <evilcraft:darkened_apple>,
    [<mysticalagriculture:prudentium_apple>, <ore:gemDark>]);

# Spikes
recipes.remove(<evilcraft:dark_spike>);
recipes.addShaped("Dark Spike", <evilcraft:dark_spike> * 16, [[<ore:gemDark>], [<ore:ingotSteel>]]);

# Eternal Water Block (removed)
rh(<evilcraft:eternal_water_block>);

# Vengeance Ring
recipes.remove(<evilcraft:vengeance_ring>);
recipes.addShaped("Vengeance Ring", <evilcraft:vengeance_ring>, 
    [[<ore:gemDarkCrushed>, <ore:ingotThaumium>, <ore:gemDarkCrushed>],
     [<ore:ingotThaumium>, null, <ore:ingotThaumium>],
     [<ore:gemDarkCrushed>, <ore:ingotThaumium>, <ore:gemDarkCrushed>]]);

# Vengeance Focus
recipes.remove(<evilcraft:vengeance_focus>);
recipes.addShaped("Vengeance Focus", <evilcraft:vengeance_focus>, 
    [[<ore:gemDarkCrushed>, <ore:ingotThaumium>, <ore:gemDarkCrushed>],
     [<ore:ingotThaumium>, <evilcraft:vengeance_ring>, <ore:ingotThaumium>],
     [<ore:gemDarkCrushed>, <ore:ingotThaumium>, <ore:gemDarkCrushed>]]);

# Blood Extractor
recipes.remove(<evilcraft:blood_extractor>);
recipes.addShaped("Blood Extractor", <evilcraft:blood_extractor>, 
    [[<evilcraft:dark_spike>, <evilcraft:dark_spike>, <evilcraft:dark_spike>],
     [null, <thaumcraft:phial>, null],
     [null, <ore:gemDark>, null]]);

# Blood Infusion Core
recipes.remove(<evilcraft:blood_infusion_core>);
mods.thaumcraft.Infusion.registerRecipe("Blood Infusion Core", "", <evilcraft:blood_infusion_core>, 4,
    [<aspect:potentia>*15, <aspect:victus>*40, <aspect:praecantatio>*20],
    <ore:gemDarkPower>,
    [<evilcraft:hardened_blood_shard>, <evilcraft:hardened_blood_shard>, <evilcraft:hardened_blood_shard>, 
     <evilcraft:hardened_blood_shard>, <evilcraft:hardened_blood_shard>, <evilcraft:hardened_blood_shard>, 
     <evilcraft:hardened_blood_shard>, <evilcraft:hardened_blood_shard>]);

# Promises
recipes.remove(<evilcraft:promise>);
mods.thaumcraft.Infusion.registerRecipe("Promise of Tenacity I", "", <evilcraft:promise>, 2,
    [<aspect:praecantatio>*20],
    <evilcraft:promise_acceptor>,
    [<ore:materialBowlOfPromises0>, <minecraft:spider_eye>]);

recipes.remove(<evilcraft:promise:1>);
mods.thaumcraft.Infusion.registerRecipe("Promise of Tenacity II", "", <evilcraft:promise:1>, 2,
    [<aspect:praecantatio>*40],
    <evilcraft:promise_acceptor:1>,
    [<ore:materialBowlOfPromises1>, <minecraft:ender_pearl>]);

recipes.remove(<evilcraft:promise:2>);
mods.thaumcraft.Infusion.registerRecipe("Promise of Tenacity III", "", <evilcraft:promise:2>, 2,
    [<aspect:praecantatio>*60],
    <evilcraft:promise_acceptor:2>,
    [<ore:materialBowlOfPromises2>, <minecraft:ender_eye>]);

recipes.remove(<evilcraft:promise:3>);
mods.thaumcraft.Infusion.registerRecipe("Promise of Velocity", "", <evilcraft:promise:3>, 2,
    [<aspect:praecantatio>*40],
    <evilcraft:promise_acceptor:1>,
    [<ore:materialBowlOfPromises1>, <minecraft:redstone_block>]);

recipes.remove(<evilcraft:promise:4>);
mods.thaumcraft.Infusion.registerRecipe("Promise of Productivity", "", <evilcraft:promise:4>, 2,
    [<aspect:praecantatio>*40],
    <evilcraft:promise_acceptor:1>,
    [<ore:materialBowlOfPromises1>, <minecraft:lapis_block>]);

# Creative Blood Drop
mods.extendedcrafting.TableCrafting.addShaped(0, <evilcraft:creative_blood_drop>.withTag({}),
	[[null, null, null, null, <bloodarsenal:blood_infused_iron_block>, null, null, null, null], 
	 [null, null, null, <bloodarsenal:blood_infused_iron_block>, <bloodmagic:blood_orb>, <bloodarsenal:blood_infused_iron_block>, null, null, null], 
	 [null, null, <bloodarsenal:blood_infused_iron_block>, <ore:blockSupremium>, <bloodmagic:blood_tank:6>, <ore:blockSupremium>, <bloodarsenal:blood_infused_iron_block>, null, null], 
	 [null, <bloodarsenal:blood_infused_iron_block>, <bloodmagic:blood_tank:6>, <ore:blockSupremium>, <bloodmagic:blood_tank:6>, <ore:blockSupremium>, <bloodmagic:blood_tank:6>, <bloodarsenal:blood_infused_iron_block>, null], 
	 [<bloodmagic:decorative_brick>, <ore:carminite>, <bloodmagic:blood_tank:6>, <ore:blockSupremium>, <bloodmagic:blood_tank:9>, <ore:blockSupremium>, <bloodmagic:blood_tank:6>, <ore:carminite>, <bloodmagic:decorative_brick>], 
	 [<bloodmagic:decorative_brick>, <ore:carminite>, <bloodmagic:blood_tank:6>, <ore:blockSupremium>, <bloodmagic:blood_tank:6>, <ore:blockSupremium>, <bloodmagic:blood_tank:6>, <ore:carminite>, <bloodmagic:decorative_brick>], 
	 [<bloodmagic:decorative_brick>, <evilcraft:hardened_blood>, <bloodarsenal:blood_diamond:2>, <evilcraft:blood_pearl_of_teleportation>, <bloodmagic:blood_tank:6>, <evilcraft:blood_pearl_of_teleportation>, <bloodarsenal:blood_diamond:2>, <evilcraft:hardened_blood>, <bloodmagic:decorative_brick>], 
	 [null, <bloodmagic:decorative_brick>, <thaumcraft:creative_flux_sponge>, <evilcraft:hardened_blood>, <evilcraft:hardened_blood>, <evilcraft:hardened_blood>, <thaumcraft:creative_flux_sponge>, <bloodmagic:decorative_brick>, null], 
	 [null, null, <ore:blockSlime>, <ore:blockSlime>, <ore:blockSlime>, <ore:blockSlime>, <ore:blockSlime>, null, null]]);
