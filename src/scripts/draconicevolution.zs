# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading draconicevolution.zs");

# Draconium Blocks
mods.bloodmagic.BloodAltar.addRecipe(<draconicevolution:draconium_block>,
    <mysticalagradditions:storage:1>, 5, 120000, 100, 200);

# Draconic Core
recipes.remove(<draconicevolution:draconic_core>);
recipes.addShapedMirrored("Draconic Core 1", <draconicevolution:draconic_core>, 
    [[<ore:ingotDraconium>, <ore:ingotKnightmetal>, <ore:ingotDraconium>],
     [<ore:ingotEthaxium>, <evilcraft:garmonbozia>, <ore:ingotEthaxium>],
     [<ore:ingotDraconium>, <ore:ingotKnightmetal>, <ore:ingotDraconium>]]);
recipes.addShapedMirrored("Draconic Core 2", <draconicevolution:draconic_core>, 
    [[<ore:ingotDraconium>, <ore:ingotKnightmetal>, <ore:ingotDraconium>],
     [<ore:ingotEthaxium>, <evilcraft:promise_acceptor:2>, <ore:ingotEthaxium>],
     [<ore:ingotDraconium>, <ore:ingotKnightmetal>, <ore:ingotDraconium>]]);

# Creative Flux Capacitor
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:draconium_capacitor:2>.withTag({Energy: 1073741823}),
	[[<ore:blockDraconiumAwakened>, null, null, null, <ore:blockDraconiumAwakened>, null, null, null, <ore:blockDraconiumAwakened>], 
	 [null, <ore:blockDraconiumAwakened>, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, <ore:blockDraconiumAwakened>, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, <ore:blockDraconiumAwakened>, null], 
	 [null, <draconicevolution:draconium_block:1>, <mysticalagradditions:stuff:69>, <ore:blockBlackIron>, <mysticalagradditions:stuff:69>, <ore:blockBlackIron>, <mysticalagradditions:stuff:69>, <draconicevolution:draconium_block:1>, null], 
	 [null, <draconicevolution:draconium_block:1>, <ore:blockBlackIron>, <ore:blockFiery>, <draconicevolution:draconic_energy_core>, <ore:blockFiery>, <ore:blockBlackIron>, <draconicevolution:draconium_block:1>, null], 
	 [<ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <mysticalagradditions:stuff:69>, <draconicevolution:draconium_capacitor:1>, <simplyjetpacks:itemfluxpack>, <draconicevolution:draconium_capacitor:1>, <mysticalagradditions:stuff:69>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>], 
	 [null, <draconicevolution:draconium_block:1>, <ore:blockBlackIron>, <ore:blockFiery>, <draconicevolution:draconic_energy_core>, <ore:blockFiery>, <ore:blockBlackIron>, <draconicevolution:draconium_block:1>, null], 
	 [null, <draconicevolution:draconium_block:1>, <mysticalagradditions:stuff:69>, <ore:blockBlackIron>, <mysticalagradditions:stuff:69>, <ore:blockBlackIron>, <mysticalagradditions:stuff:69>, <draconicevolution:draconium_block:1>, null], 
	 [null, <ore:blockDraconiumAwakened>, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, <ore:blockDraconiumAwakened>, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, <ore:blockDraconiumAwakened>, null], 
	 [<ore:blockDraconiumAwakened>, null, null, null, <ore:blockDraconiumAwakened>, null, null, null, <ore:blockDraconiumAwakened>]]);

# Creative Block Exchanger
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:creative_exchanger>.withTag({Profile_0: {fillLogic: 0 as byte, AOE: 0, replaceSame: 0 as byte, replaceVisible: 0 as byte}}),
	[[null, null, <extendedcrafting:singularity:48>, null, null, null, null, null, null], 
	 [null, null, <extendedcrafting:singularity:48>, null, null, null, null, null, null], 
	 [<extendedcrafting:singularity:48>, null, <storagedrawers:upgrade_creative>, <extendedcrafting:singularity:50>, null, null, null, null, null], 
	 [null, <extendedcrafting:singularity:48>, <extendedcrafting:singularity:48>, <cyclicmagic:tool_swap_match>, <extendedcrafting:singularity:50>, null, null, null, null], 
	 [null, null, null, <extendedcrafting:singularity:50>, <extendedcrafting:singularity:50>, null, null, null, null], 
	 [null, null, null, null, null, <extendedcrafting:singularity:50>, <extendedcrafting:singularity:50>, null, null], 
	 [null, null, null, null, null, <extendedcrafting:singularity:50>, <exchangers:resonant_exchanger>, <extendedcrafting:singularity:50>, null], 
	 [null, null, null, null, null, null, <extendedcrafting:singularity:34>, <extendedcrafting:singularity:34>, null], 
	 [null, null, null, null, null, null, null, null, <draconicevolution:draconium_capacitor:1>]]);
