# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import mods.jei.JEI.removeAndHide as rh;
import crafttweaker.item.IItemStack as IItemStack;

print(">>> loading oredict.zs");
  
# Pam's Apple
<ore:cropApple>.add(<harvestcraft:pamapple>);
<ore:listAllfruit>.add(<harvestcraft:pamapple>);

# Boat Oredict 
val boats = [
  <minecraft:boat>,
  <minecraft:spruce_boat>,
  <minecraft:birch_boat>,
  <minecraft:jungle_boat>,
  <minecraft:acacia_boat>,
  <minecraft:dark_oak_boat>,
  <biomesoplenty:boat_sacred_oak>,
  <biomesoplenty:boat_cherry>,
  <biomesoplenty:boat_umbran>,
  <biomesoplenty:boat_fir>,
  <biomesoplenty:boat_ethereal>,
  <biomesoplenty:boat_magic>,
  <biomesoplenty:boat_mangrove>,
  <biomesoplenty:boat_palm>,
  <biomesoplenty:boat_redwood>,
  <biomesoplenty:boat_willow>,
  <biomesoplenty:boat_pine>,
  <biomesoplenty:boat_hellbark>,
  <biomesoplenty:boat_jacaranda>,
  <biomesoplenty:boat_mahogany>,
  <biomesoplenty:boat_ebony>,
  <biomesoplenty:boat_eucalyptus>
] as IItemStack[];

for boat in boats {
  <ore:boat>.add(boat);
}

# Mycelium Oredict
<ore:mycelium>.add(<biomesoplenty:grass:8>);

# Venison Oredict
<ore:listAllbeefraw>.add(<twilightforest:raw_venison>);
<ore:listAllmeatraw>.add(<twilightforest:raw_venison>);
  
<ore:listAllbeefcooked>.add(<twilightforest:cooked_venison>);
<ore:listAllmeatcooked>.add(<twilightforest:cooked_venison>);

# Void Metal Block
<ore:blockVoid>.add(<thaumcraft:metal_void>);

# Amber Oredict
<ore:blockAmber>.add(<biomesoplenty:gem_block:7>);
<ore:blockAmber>.add(<thaumcraft:amber_block>);
<ore:blockAmber>.add(<thaumcraft:amber_brick>);

# Thermal Expansion Dynamos
val dynamos = [
  <thermalexpansion:dynamo>,
  <thermalexpansion:dynamo:1>,
  <thermalexpansion:dynamo:2>,
  <thermalexpansion:dynamo:3>,
  <thermalexpansion:dynamo:4>,
  <thermalexpansion:dynamo:5>
] as IItemStack[];

for dynamo in dynamos {
  <ore:dynamo>.add(dynamo);
}

# Blood Magic Runes
val runes = [
  <bloodmagic:blood_rune>,
  <bloodmagic:blood_rune:1>,
  <bloodmagic:blood_rune:2>,
  <bloodmagic:blood_rune:3>,
  <bloodmagic:blood_rune:4>,
  <bloodmagic:blood_rune:5>,
  <bloodmagic:blood_rune:6>,
  <bloodmagic:blood_rune:7>,
  <bloodmagic:blood_rune:8>,
  <bloodmagic:blood_rune:9>,
  <bloodmagic:blood_rune:10>
] as IItemStack[];

for rune in runes {
  <ore:runeBlood>.add(rune);
}
  
# CakeDict(TM)
val cakes = [
  <minecraft:cake>,
  <harvestcraft:carrotcakeitem>,
  <harvestcraft:cheesecakeitem>,
  <harvestcraft:cherrycheesecakeitem>,
  <harvestcraft:chocolatesprinklecakeitem>,
  <harvestcraft:holidaycakeitem>,
  <harvestcraft:pineappleupsidedowncakeitem>,
  <harvestcraft:pumpkincheesecakeitem>,
  <harvestcraft:redvelvetcakeitem>,
  <harvestcraft:lemondrizzlecakeitem>
] as IItemStack[];

for cake in cakes {
  <ore:foodCake>.add(cake);
}
  
# Wither Dust
<ore:dustWither>.add(<darkutils:material>);
<ore:dustWither>.add(<quark:black_ash>);

# Astral Sorcery compatibility for Chisel marble
<ore:blockMarble>.add(<chisel:marble2:7>);
  
# Fertilizer
var fertilizer = <ore:fertilizer>;
fertilizer.add(<minecraft:dye:15>);
fertilizer.add(<industrialforegoing:fertilizer>);
fertilizer.add(<forestry:fertilizer_compound>);
  
# Sawdust compat
<ore:pulpWood>.add(<thermalfoundation:material:800>);
<ore:dustWood>.add(<forestry:wood_pulp>);
  
# Diamond Chip
<ore:chipDiamond>.add(<thermalfoundation:material:16>);

# Various stones
val stonemarble = <ore:stoneMarble>;
val stonemarblepolished = <ore:stoneMarblePolished>;
val andesite = <ore:stoneAndesite>;
val diorite = <ore:stoneDiorite>;
val granite = <ore:stoneGranite>;
val sandstone = <ore:sandstone>;
val basalt = <ore:stoneBasalt>;
val limestone = <ore:stoneLimestone>;
  
# Forestry gears
val gearTin = <ore:gearTin>;
val gearCopper = <ore:gearCopper>;
val gearBronze = <ore:gearBronze>;  
    
# Removing gears from JEI "cycling"
gearTin.remove(<forestry:gear_tin>);
gearCopper.remove(<forestry:gear_copper>);
gearBronze.remove(<forestry:gear_bronze>);  
  
sandstone.add(<quark:sandstone_new>);
sandstone.add(<quark:sandstone_new:1>);
sandstone.add(<quark:sandstone_new:2>);
sandstone.add(<quark:sandstone_new:3>);
  
diorite.add(<quark:world_stone_bricks:1>);
andesite.add(<quark:world_stone_bricks:2>);
granite.add(<quark:world_stone_bricks>);
  
basalt.add(<quark:world_stone_bricks:3>);
