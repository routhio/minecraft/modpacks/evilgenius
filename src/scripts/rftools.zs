# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import crafttweaker.item.IItemStack as IItemStack;
import mods.jei.JEI.removeAndHide as rh;

print(">>> loading rftools.zs");

# Machine Frame
recipes.remove(<rftools:machine_frame>);
recipes.addShaped("rftools machine frame", <rftools:machine_frame>,
    [[<ore:plateThaumium>, <ore:gearGold>, <ore:plateThaumium>],
     [<ore:plateEnderium>, <nuclearcraft:part:10>, <ore:plateEnderium>],
     [<ore:itemRubber>, <contenttweaker:chip>, <ore:itemRubber>]]);

# Power Cell
recipes.remove(<rftools:powercell>);
recipes.addShaped("rftools powercell", <rftools:powercell>,
    [[<ore:blockRedstone>, <ore:gemDiamond>, <ore:blockRedstone>],
     [<ore:gemPrismarine>, <integrateddynamics:energy_battery>, <ore:gemPrismarine>], 
     [<ore:blockRedstone>, <ore:gemEmerald>, <ore:blockRedstone>]]);

# Advanced Power Cell
recipes.remove(<rftools:powercell_advanced>);
recipes.addShaped("rftools advanced powercell", <rftools:powercell_advanced>,
    [[<integrateddynamics:energy_battery>, <rftools:infused_diamond>, <integrateddynamics:energy_battery>],
     [<rftools:infused_diamond>, <rftools:powercell>, <rftools:infused_diamond>],
     [<integrateddynamics:energy_battery>, <rftools:infused_diamond>, <integrateddynamics:energy_battery>]]);

# Powercell Card
recipes.remove(<rftools:powercell_card>);
recipes.addShaped("rftools powercell card", <rftools:powercell_card>,
    [[<ore:dustRedstone>, <ore:nuggetGold>, <ore:dustRedstone>],
     [<ore:nuggetGold>, <refinedstorage:processor:5>, <ore:nuggetGold>],
     [<ore:dustRedstone>, <ore:nuggetGold>, <ore:dustRedstone>]]);

# Builder
recipes.remove(<rftools:builder>);
recipes.addShaped("rftools builder 1", <rftools:builder>,
    [[<ore:platePlatinum>, <ore:enderpearl>, <ore:platePlatinum>],
     [<industrialforegoing:block_placer>, <rftools:machine_frame>, <industrialforegoing:block_destroyer>],
     [<mysticalagradditions:prudentium_paxel>, <ore:dustRedstone>, <mysticalagradditions:prudentium_paxel>]]);
recipes.addShaped("rftools builder 2", <rftools:builder>,
    [[<ore:platePlatinum>, <ore:enderpearl>, <ore:platePlatinum>],
     [<industrialforegoing:block_destroyer>, <rftools:machine_frame>, <industrialforegoing:block_placer>],
     [<mysticalagradditions:prudentium_paxel>, <ore:dustRedstone>, <mysticalagradditions:prudentium_paxel>]]);

# Shape card
recipes.remove(<rftools:shape_card>);
recipes.addShapedMirrored("rftools shape card", <rftools:shape_card>,
    [[<ore:itemRubber>, <refinedstorage:quartz_enriched_iron>, <ore:itemRubber>],
     [<ore:dustRedstone>, <ore:itemRubber>, <ore:dustRedstone>],
     [<ore:itemRubber>, <refinedstorage:quartz_enriched_iron>, <ore:itemRubber>]]);

# Shape card (quarry)
recipes.remove(<rftools:shape_card:2>);
recipes.addShaped("rftools shape card quarry", <rftools:shape_card:2>,
    [[<ore:itemRubber>, <refinedstorage:processor:5>, <ore:itemRubber>],
     [<minecraft:diamond_pickaxe>, <rftools:shape_card>, <minecraft:diamond_shovel>],
     [<ore:dustRedstone>, <industrialforegoing:block_destroyer>, <ore:dustRedstone>]]);

# Shape card (pump)
recipes.remove(<rftools:shape_card:8>);
recipes.addShaped("rftools shape card pump", <rftools:shape_card:8>,
    [[<ore:itemRubber>, <refinedstorage:processor:5>, <ore:itemRubber>],
     [<ore:listAllwater>, <rftools:shape_card>, <minecraft:lava_bucket>],
     [<ore:dustRedstone>, <industrialforegoing:fluid_pump>, <ore:dustRedstone>]]);

# Dimensional Shard
recipes.remove(<rftools:dimensional_shard>);

# Dimensional Tab
recipes.remove(<rftoolsdim:empty_dimension_tab>);
mods.extendedcrafting.TableCrafting.addShaped(0, <rftoolsdim:empty_dimension_tab>,
    [[null, null, <draconicevolution:wyvern_core>, null, null],
     [null, <ore:paper>, <ore:dustRedstone>, <ore:paper>, null],
     [<draconicevolution:wyvern_core>, <ore:dustRedstone>, <ore:paper>, <ore:dustRedstone>, <draconicevolution:wyvern_core>],
     [null, <ore:paper>, <ore:dustRedstone>, <ore:paper>, null],
     [null, null, <draconicevolution:wyvern_core>, null, null]]);

# Dimensional Builder
recipes.remove(<rftoolsdim:dimension_builder>);
recipes.addShaped("rftools dimension builder", <rftoolsdim:dimension_builder>,
    [[<minecraft:ender_pearl>, <rftoolsdim:empty_dimension_tab>, <minecraft:ender_pearl>],
     [<minecraft:diamond>, <rftools:machine_frame>, <minecraft:diamond>],
     [<ore:ingotGold>, <ore:ingotGold>, <ore:ingotGold>]]);

# Dimensional Enscriber
recipes.remove(<rftoolsdim:dimension_enscriber>);
recipes.addShaped("rftools dimension encriber", <rftoolsdim:dimension_enscriber>,
    [[<minecraft:redstone>, <rftoolsdim:empty_dimension_tab>, <minecraft:redstone>],
     [<minecraft:dye>, <rftools:machine_frame>, <minecraft:dye>],
     [<ore:ingotIron>, <ore:ingotIron>, <ore:ingotIron>]]);

# Dimension Editor
recipes.remove(<rftoolsdim:dimension_editor>);
recipes.addShaped("rftools dimension editor", <rftoolsdim:dimension_editor>,
    [[<minecraft:redstone>, <rftoolsdim:empty_dimension_tab>, <minecraft:redstone>],
     [<minecraft:diamond>, <rftools:machine_frame>, <minecraft:diamond>],
     [<ore:ingotGold>, <ore:ingotGold>, <ore:ingotGold>]]);

# Dimlet Workbench
recipes.remove(<rftoolsdim:dimlet_workbench>);
recipes.addShaped("rftools dimlet workbench", <rftoolsdim:dimlet_workbench>,
    [[<minecraft:gold_nugget>, <rftoolsdim:dimlet_base>, <minecraft:gold_nugget>],
     [<rftoolsdim:empty_dimension_tab>, <rftools:machine_frame>, <rftoolsdim:empty_dimension_tab>],
     [<minecraft:gold_nugget>, <minecraft:redstone>, <minecraft:gold_nugget>]]);

# Creative Powercell
mods.extendedcrafting.TableCrafting.addShaped(0, <rftools:powercell_creative>,
	[[<twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>], 
	 [<twilightforest:castle_brick>, <abyssalcraft:engraving_elder>, null, null, null, null, null, <abyssalcraft:engraving_elder>, <twilightforest:castle_brick>], 
	 [<twilightforest:castle_brick>, null, <ore:dropRoyalJelly>, <ore:dropRoyalJelly>, <ore:dropRoyalJelly>, <ore:dropRoyalJelly>, <ore:dropRoyalJelly>, null, <twilightforest:castle_brick>], 
	 [<twilightforest:castle_brick>, null, <ore:dropRoyalJelly>, <extendedcrafting:singularity:49>, <thaumcraft:voidseer_charm>, <extendedcrafting:singularity:49>, <ore:dropRoyalJelly>, null, <twilightforest:castle_brick>], 
	 [<twilightforest:castle_brick>, null, <ore:dropRoyalJelly>, <bloodmagic:teleposition_focus:3>, <integrateddynamics:creative_energy_battery>, <bloodmagic:teleposition_focus:3>, <ore:dropRoyalJelly>, null, <twilightforest:castle_brick>], 
	 [<twilightforest:castle_brick>, null, <ore:dropRoyalJelly>, <extendedcrafting:singularity:49>, <thaumcraft:turret:2>, <extendedcrafting:singularity:49>, <ore:dropRoyalJelly>, null, <twilightforest:castle_brick>], 
	 [<twilightforest:castle_brick>, null, <ore:dropRoyalJelly>, <ore:dropRoyalJelly>, <ore:dropRoyalJelly>, <ore:dropRoyalJelly>, <ore:dropRoyalJelly>, null, <twilightforest:castle_brick>], 
	 [<twilightforest:castle_brick>, <abyssalcraft:engraving_elder>, null, null, null, null, null, <abyssalcraft:engraving_elder>, <twilightforest:castle_brick>], 
	 [<twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>]]);

