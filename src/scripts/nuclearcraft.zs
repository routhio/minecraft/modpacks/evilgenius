# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import mods.jei.JEI.removeAndHide as rh;

print(">>> loading nuclearcraft.zs");

# Machine Chassis
recipes.remove(<nuclearcraft:part:10>);
recipes.addShaped(<nuclearcraft:part:10>, 
    [[<ore:ingotTough>, <ore:ingotSteel>, <ore:ingotTough>],
     [<ore:ingotSteel>, <thermalexpansion:frame>, <ore:ingotSteel>], 
     [<ore:ingotTough>, <ore:ingotSteel>, <ore:ingotTough>]]);

# Infinite Cobblestone/Water Generators
recipes.remove(<nuclearcraft:cobblestone_generator>);
recipes.addShaped("nuclearcraft cobblestone gen", <nuclearcraft:cobblestone_generator>, 
    [[<nuclearcraft:part>, <ore:ingotTin>, <nuclearcraft:part>],
     [<thermalexpansion:machine:15>, null, <thermalexpansion:machine:15>],
     [<nuclearcraft:part>, <ore:ingotTin>, <nuclearcraft:part>]]);

recipes.remove(<nuclearcraft:water_source>);
recipes.addShaped("nuclearcraft water source", <nuclearcraft:water_source>, 
    [[<nuclearcraft:part>, <ore:ingotTin>, <nuclearcraft:part>],
     [<thermalexpansion:device>, null, <thermalexpansion:device>], 
     [<nuclearcraft:part>, <ore:ingotTin>, <nuclearcraft:part>]]);

# Manufactory
recipes.remove(<nuclearcraft:manufactory_idle>);
recipes.addShaped("nuclearcraft manufactory", <nuclearcraft:manufactory_idle>,
    [[<ore:plateLead>, <ore:dustRedstone>, <ore:plateLead>],
     [<minecraft:piston>, <thermalexpansion:machine>, <minecraft:piston>],
     [<ore:plateLead>, <nuclearcraft:part:4>, <ore:plateLead>]]);

# Remove and Hide
	
rh(<nuclearcraft:ore>);
rh(<nuclearcraft:ore:1>);
rh(<nuclearcraft:ore:2>);
rh(<nuclearcraft:ore:4>);
rh(<nuclearcraft:shovel_tough>);
rh(<nuclearcraft:pickaxe_tough>);
rh(<nuclearcraft:sword_tough>);
rh(<nuclearcraft:spaxelhoe_boron>);
rh(<nuclearcraft:hoe_boron>);
rh(<nuclearcraft:axe_boron>);
rh(<nuclearcraft:shovel_boron>);
rh(<nuclearcraft:pickaxe_boron>);
rh(<nuclearcraft:sword_boron>);
rh(<nuclearcraft:dust>);
rh(<nuclearcraft:dust:2>);
rh(<nuclearcraft:dust:1>);
rh(<nuclearcraft:ingot:4>);
rh(<nuclearcraft:ingot:2>);
rh(<nuclearcraft:ingot:1>);
rh(<nuclearcraft:dust:4>);
rh(<nuclearcraft:alloy>);
rh(<nuclearcraft:alloy:5>);
rh(<nuclearcraft:ingot>);
rh(<nuclearcraft:pickaxe_boron_nitride>);
rh(<nuclearcraft:spaxelhoe_boron_nitride>);
rh(<nuclearcraft:hoe_boron_nitride>);
rh(<nuclearcraft:axe_boron_nitride>);
rh(<nuclearcraft:shovel_boron_nitride>);
rh(<nuclearcraft:sword_boron_nitride>);
rh(<nuclearcraft:spaxelhoe_hard_carbon>);
rh(<nuclearcraft:hoe_hard_carbon>);
rh(<nuclearcraft:axe_hard_carbon>);
rh(<nuclearcraft:shovel_hard_carbon>);
rh(<nuclearcraft:pickaxe_hard_carbon>);
rh(<nuclearcraft:sword_hard_carbon>);
rh(<nuclearcraft:spaxelhoe_tough>);
rh(<nuclearcraft:hoe_tough>);
rh(<nuclearcraft:axe_tough>);
rh(<nuclearcraft:lithium_ion_cell>);
rh(<nuclearcraft:lithium_ion_battery_basic>);

