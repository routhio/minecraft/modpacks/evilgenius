# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix

#

import crafttweaker.item.IItemStack as IItemStack;
import crafttweaker.liquid.ILiquidDefinition;
import mods.jei.JEI.removeAndHide as rh;

print(">>> loading tinkerscontruct.zs");

var brick = <tconstruct:materials>;

# Controller Blocks
recipes.remove(<tconstruct:smeltery_controller>);
recipes.remove(<tconstruct:seared_furnace_controller>);
mods.tconstruct.Casting.removeBasinRecipe(<tconstruct:seared_furnace_controller>);
recipes.remove(<tconstruct:tinker_tank_controller>);

recipes.addShaped("smeltery controller", <tconstruct:smeltery_controller>,
    [[brick, brick, brick],
     [brick, null, brick],
     [brick, brick, brick]]);
recipes.addShaped("seared furnace controller", <tconstruct:seared_furnace_controller>,
    [[brick, brick, brick],
     [brick, null, brick],
     [brick, <minecraft:furnace>, brick]]);
recipes.addShaped("tinker tank controller", <tconstruct:tinker_tank_controller>,
    [[brick, brick, brick],
     [brick, null, brick],
     [brick, <minecraft:bucket>, brick]]);

# Gear cast
mods.tconstruct.Casting.addTableRecipe(<tconstruct:cast_custom:4>, <ore:gearWood>, <liquid:gold>, 288, true);
mods.tconstruct.Casting.addTableRecipe(<tconstruct:cast_custom:4>, <ore:gearWood>, <liquid:alubrass>, 144, true);
mods.tconstruct.Casting.addTableRecipe(<tconstruct:cast_custom:4>, <ore:gearWood>, <liquid:brass>, 144, true);
mods.tconstruct.Casting.addTableRecipe(<tconstruct:cast_custom:4>, <ore:gearStone>, <liquid:gold>, 288, true);
mods.tconstruct.Casting.addTableRecipe(<tconstruct:cast_custom:4>, <ore:gearStone>, <liquid:alubrass>, 144, true);
mods.tconstruct.Casting.addTableRecipe(<tconstruct:cast_custom:4>, <ore:gearStone>, <liquid:brass>, 144, true);
mods.tconstruct.Casting.addTableRecipe(<tconstruct:cast_custom:4>, <ore:gearDiamond>, <liquid:gold>, 288, true);
mods.tconstruct.Casting.addTableRecipe(<tconstruct:cast_custom:4>, <ore:gearDiamond>, <liquid:alubrass>, 144, true);
mods.tconstruct.Casting.addTableRecipe(<tconstruct:cast_custom:4>, <ore:gearDiamond>, <liquid:brass>, 144, true);
mods.tconstruct.Casting.addTableRecipe(<tconstruct:cast_custom:4>, <ore:gearEmerald>, <liquid:gold>, 288, true);
mods.tconstruct.Casting.addTableRecipe(<tconstruct:cast_custom:4>, <ore:gearEmerald>, <liquid:alubrass>, 144, true);
mods.tconstruct.Casting.addTableRecipe(<tconstruct:cast_custom:4>, <ore:gearEmerald>, <liquid:brass>, 144, true);


# Smeltery fuels
<liquid:pyrotheum>.definition.temperature = 5300;
<liquid:refined_fuel>.definition.temperature = 4100;
	
mods.tconstruct.Fuel.registerFuel(<liquid:pyrotheum> * 25, 400);
mods.tconstruct.Fuel.registerFuel(<liquid:refined_fuel> * 25, 600);

# Creative Tool Modifier
mods.extendedcrafting.TableCrafting.addShaped(0, <tconstruct:materials:50>,
	[[<tconstruct:large_plate>, <tconstruct:large_plate>, <tconstruct:large_plate>, <tconstruct:large_plate>, <tconstruct:large_plate>], 
	 [<tconstruct:large_plate>, <quark:biotite_block>, <tconstruct:metal:6>, <quark:biotite_block>, <tconstruct:large_plate>], 
	 [<tconstruct:metal:6>, <tconstruct:metal:6>, <ore:itemSkull>, <tconstruct:metal:6>, <tconstruct:metal:6>], 
	 [<tconstruct:metal:6>, <tconstruct:materials:19>, <draconicevolution:draconium_capacitor:2>, <tconstruct:materials:19>, <tconstruct:metal:6>], 
	 [<tconstruct:metal:6>, <tconstruct:metal:6>, <tconstruct:metal:6>, <tconstruct:metal:6>, <tconstruct:metal:6>]]);


# *======= Alloying =======*

//mods.tconstruct.Alloy.addRecipe(ILiquidStack output, ILiquidStack[] inputs);
//mods.tconstruct.Alloy.removeRecipe(ILiquidStack output);

# *======= Casting =======* Wrong Info on docs

//mods.tconstruct.Casting.addTableRecipe(IItemStack output, IItemStack cast, ILiquidStack fluid, int amount, @Optional boolean consumeCast);
//mods.tconstruct.Casting.addBasinRecipe(IItemStack output, IItemStack cast, ILiquidStack fluid, int amount, @Optional boolean consumeCast);

//mods.tconstruct.Casting.removeTableRecipe(IItemStack output);
//mods.tconstruct.Casting.removeBasinRecipe(IItemStack output);

# *======= Drying =======*

//mods.tconstruct.Drying.addRecipe(IItemStack output, IItemStack input, int time);
//mods.tconstruct.Drying.removeRecipe(IItemStack output);

# *======= Melting =======*

//mods.tconstruct.Melting.addRecipe(ILiquidStack output, IItemStack input, @Optional int temp);
//mods.tconstruct.Melting.removeRecipe(ILiquidStack output);
