# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import crafttweaker.item.IItemStack as IItemStack;
import mods.jei.JEI.removeAndHide as rh;

print(">>> loading refinedstorage.zs");

# Quartz Enriched Iron
recipes.remove(<refinedstorage:quartz_enriched_iron>);
mods.thermalexpansion.Transposer.addFillRecipe(<refinedstorage:quartz_enriched_iron>*4, <minecraft:iron_ingot>*3, <liquid:quartz>*666, 2000);

# Machine Casing
recipes.remove(<refinedstorage:machine_casing>);
recipes.addShaped("rs machine casing", <refinedstorage:machine_casing>, 
    [[<refinedstorage:quartz_enriched_iron>, <refinedstorage:quartz_enriched_iron>, <refinedstorage:quartz_enriched_iron>],
    [<refinedstorage:quartz_enriched_iron>, <rftools:modular_storage>, <refinedstorage:quartz_enriched_iron>], 
    [<refinedstorage:quartz_enriched_iron>, <refinedstorage:quartz_enriched_iron>, <refinedstorage:quartz_enriched_iron>]]);

# Processor Binding
recipes.remove(<refinedstorage:processor_binding>);
recipes.addShaped(<refinedstorage:processor_binding>*16,
	[[null, null, null],
	 [<minecraft:string>, <ore:slimeball>, <minecraft:string>],
	 [null, null, null]]);

# Creative Controller
mods.extendedcrafting.TableCrafting.addShaped(0, <refinedstorage:controller:1>.withTag({Energy: 32000}),
	[[<vampirism:castle_block:4>, <vampirism:castle_block:4>, <vampirism:castle_block:4>, <vampirism:castle_block:4>, <vampirism:castle_block:4>, <vampirism:castle_block:4>, <vampirism:castle_block:4>, <vampirism:castle_block:4>, <vampirism:castle_block:4>], 
	 [<vampirism:castle_block:4>, <extendedcrafting:singularity:6>, <ore:blockKnightmetal>, <extendedcrafting:singularity:34>, <ore:blockKnightmetal>, <extendedcrafting:singularity:34>, <ore:blockKnightmetal>, <extendedcrafting:singularity:6>, <vampirism:castle_block:4>], 
	 [<vampirism:castle_block:4>, <ore:blockKnightmetal>, <extendedcrafting:singularity:6>, <ore:blockKnightmetal>, <extendedcrafting:singularity:6>, <ore:blockKnightmetal>, <extendedcrafting:singularity:6>, <ore:blockKnightmetal>, <vampirism:castle_block:4>], 
	 [<vampirism:castle_block:4>, <extendedcrafting:singularity:34>, <ore:blockKnightmetal>, <extendedcrafting:singularity:6>, <ore:blockKnightmetal>, <extendedcrafting:singularity:6>, <ore:blockKnightmetal>, <extendedcrafting:singularity:34>, <vampirism:castle_block:4>], 
	 [<vampirism:castle_block:4>, <ore:blockKnightmetal>, <extendedcrafting:singularity:6>, <ore:blockKnightmetal>, <rftools:powercell_creative>, <ore:blockKnightmetal>, <extendedcrafting:singularity:6>, <ore:blockKnightmetal>, <vampirism:castle_block:4>], 
	 [<vampirism:castle_block:4>, <extendedcrafting:singularity:34>, <ore:blockKnightmetal>, <extendedcrafting:singularity:6>, <ore:blockKnightmetal>, <extendedcrafting:singularity:6>, <ore:blockKnightmetal>, <extendedcrafting:singularity:34>, <vampirism:castle_block:4>], 
	 [<vampirism:castle_block:4>, <ore:blockKnightmetal>, <extendedcrafting:singularity:6>, <ore:blockKnightmetal>, <extendedcrafting:singularity:6>, <ore:blockKnightmetal>, <extendedcrafting:singularity:6>, <ore:blockKnightmetal>, <vampirism:castle_block:4>], 
	 [<vampirism:castle_block:4>, <extendedcrafting:singularity:6>, <ore:blockKnightmetal>, <extendedcrafting:singularity:34>, <ore:blockKnightmetal>, <extendedcrafting:singularity:34>, <ore:blockKnightmetal>, <extendedcrafting:singularity:6>, <vampirism:castle_block:4>], 
	 [<vampirism:castle_block:4>, <vampirism:castle_block:4>, <vampirism:castle_block:4>, <vampirism:castle_block:4>, <vampirism:castle_block:4>, <vampirism:castle_block:4>, <vampirism:castle_block:4>, <vampirism:castle_block:4>, <vampirism:castle_block:4>]]);

# Creative Storage Disk
mods.extendedcrafting.TableCrafting.addShaped(0, <refinedstorage:storage_disk:4>.withTag({IdLeast: -6491928737631701110 as long, IdMost: 4631177500344340031 as long}),
	[[null, null, null, null, null, null, null, null, null], 
	 [null, null, null, null, <twilightforest:castle_brick>, <twilightforest:castle_brick>, null, null, null], 
	 [null, null, null, <twilightforest:castle_brick>, <refinedstorage:controller:1>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, null, null], 
	 [null, null, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <extendedcrafting:singularity:5>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, null], 
	 [null, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <extendedcrafting:singularity:17>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <ore:blockDimensionalShard>], 
	 [<twilightforest:castle_brick>, <twilightforest:castle_brick>, <extendedcrafting:singularity:25>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <ore:blockDimensionalShard>, <rebornstorage:storagecell:3>], 
	 [null, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <ore:blockDimensionalShard>, <rebornstorage:storagecell:3>, null], 
	 [null, null, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <ore:blockDimensionalShard>, <rebornstorage:storagecell:3>, null, null], 
	 [null, null, null, <twilightforest:castle_brick>, <ore:blockDimensionalShard>, <rebornstorage:storagecell:3>, null, null, null]]);

# Createive Fluid Storage Disk
mods.extendedcrafting.TableCrafting.addShaped(0, <refinedstorage:fluid_storage_disk:4>.withTag({IdLeast: -5171541055663388721 as long, IdMost: -4553592680437562219 as long}),
	[[null, null, null, null, null, null, null, null, null], 
	 [null, null, null, null, <twilightforest:castle_brick>, <twilightforest:castle_brick>, null, null, null], 
	 [null, null, null, <twilightforest:castle_brick>, <refinedstorage:controller:1>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, null, null], 
	 [null, null, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <extendedcrafting:singularity:65>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, null], 
	 [null, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <extendedcrafting:singularity:2>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <ore:blockDimensionalShard>], 
	 [<twilightforest:castle_brick>, <twilightforest:castle_brick>, <extendedcrafting:singularity:22>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <ore:blockDimensionalShard>, <rebornstorage:storagecellfluid:3>], 
	 [null, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <ore:blockDimensionalShard>, <rebornstorage:storagecellfluid:3>, null], 
	 [null, null, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <ore:blockDimensionalShard>, <rebornstorage:storagecellfluid:3>, null, null], 
	 [null, null, null, <twilightforest:castle_brick>, <ore:blockDimensionalShard>, <rebornstorage:storagecellfluid:3>, null, null, null]]);

# Createive Wireless Crafting Grid
mods.extendedcrafting.TableCrafting.addShaped(0, <refinedstorageaddons:wireless_crafting_grid:1>,
	[[null, null, null, null, null, null, <refinedstorage:quartz_enriched_iron_block>, null, null], 
	 [null, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, null], 
	 [null, <rftoolsdim:dimensional_blank_block>, <draconicevolution:energy_crystal:8>, <draconicevolution:energy_crystal:8>, <draconicevolution:energy_crystal:8>, <draconicevolution:energy_crystal:8>, <draconicevolution:energy_crystal:8>, <rftoolsdim:dimensional_blank_block>, null], 
	 [null, <rftoolsdim:dimensional_blank_block>, <draconicevolution:energy_crystal:8>, <refinedstorage:wireless_transmitter>, <refinedstorage:controller:1>, <refinedstorage:wireless_transmitter>, <draconicevolution:energy_crystal:8>, <rftoolsdim:dimensional_blank_block>, null], 
	 [null, <rftoolsdim:dimensional_blank_block>, <draconicevolution:energy_crystal:8>, <xnet:wireless_router>, <refinedstorageaddons:wireless_crafting_grid>, <xnet:wireless_router>, <draconicevolution:energy_crystal:8>, <rftoolsdim:dimensional_blank_block>, null], 
	 [null, <rftoolsdim:dimensional_blank_block>, <draconicevolution:energy_crystal:8>, <draconicevolution:energy_crystal:8>, <draconicevolution:energy_crystal:8>, <draconicevolution:energy_crystal:8>, <draconicevolution:energy_crystal:8>, <rftoolsdim:dimensional_blank_block>, null], 
	 [null, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, null], 
	 [null, <refinedstorage:quartz_enriched_iron_block>, <refinedstorage:quartz_enriched_iron_block>, <refinedstorage:quartz_enriched_iron_block>, <refinedstorage:quartz_enriched_iron_block>, <refinedstorage:quartz_enriched_iron_block>, <refinedstorage:quartz_enriched_iron_block>, <refinedstorage:quartz_enriched_iron_block>, null], 
	 [<rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>, <rftoolsdim:dimensional_blank_block>]]);
