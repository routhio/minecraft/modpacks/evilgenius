# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import crafttweaker.item.IItemStack as IItemStack;
import mods.jei.JEI.removeAndHide as rh;

print(">>> loading thermal.zs");

# Fluxduct
recipes.remove(<thermaldynamics:duct_0>);
recipes.addShaped("leadstone fluxduct", <thermaldynamics:duct_0> * 6, 
    [[<ore:dustRedstone>, <ore:dustRedstone>, <ore:dustRedstone>],
     [<ore:plateLead>, <ore:blockGlassColorless>, <ore:plateLead>],
     [<ore:dustRedstone>, <ore:dustRedstone>, <ore:dustRedstone>]]);

recipes.remove(<thermaldynamics:duct_0:6>);
recipes.addShaped("redstone energy fluxduct", <thermaldynamics:duct_0:6> * 6, 
    [[null, null, null],
    [<ore:plateElectrum>, <ore:blockGlassHardened>, <ore:plateElectrum>], 
    [null, null, null]]);

recipes.remove(<thermaldynamics:duct_0:9>);
recipes.addShaped("cryo-stabilized fluxduct (empty)", <thermaldynamics:duct_0:9>,
    [[<ore:plateElectrum>, <ore:blockGlassHardened>, <ore:plateElectrum>],
     [<ore:blockGlassHardened>, <thermaldynamics:duct_0:4>, <ore:blockGlassHardened>], 
     [<ore:plateElectrum>, <ore:blockGlassHardened>, <ore:plateElectrum>]]);

# Fluiduct
rh(<thermaldynamics:duct_16>);
rh(<thermaldynamics:duct_16:2>);
rh(<thermaldynamics:duct_16:4>);
rh(<thermaldynamics:duct_16:6>);

recipes.remove(<thermaldynamics:duct_16:1>);
recipes.addShaped("fluiduct", <thermaldynamics:duct_16:1> * 6, 
    [[null, null, null],
     [<ore:plateCopper>, <ore:plateLead>, <ore:plateCopper>], 
     [null, null, null]]);

recipes.remove(<thermaldynamics:duct_16:3>);
recipes.addShaped("hardened fluiduct", <thermaldynamics:duct_16:3> * 6, 
    [[null, null, null],
    [<ore:plateInvar>, <ore:plateLead>, <ore:plateInvar>], 
    [null, null, null]]);

recipes.remove(<thermaldynamics:duct_16:7>);
recipes.addShaped("super-laminar fluiduct", <thermaldynamics:duct_16:7>, 
    [[<ore:plateBronze>, <ore:blockGlassHardened>, <ore:plateBronze>],
     [<ore:blockGlassHardened>, <thermaldynamics:duct_16:3>, <ore:blockGlassHardened>], 
     [<ore:plateBronze>, <ore:blockGlassHardened>, <ore:plateBronze>]]);


# Itemduct
rh(<thermaldynamics:duct_32>.withTag({DenseType: 2 as byte}));
rh(<thermaldynamics:duct_32>.withTag({DenseType: 1 as byte}));
rh(<thermaldynamics:duct_32>);
rh(<thermaldynamics:duct_32:2>.withTag({DenseType: 2 as byte}));
rh(<thermaldynamics:duct_32:2>.withTag({DenseType: 1 as byte}));
rh(<thermaldynamics:duct_32:2>);
rh(<thermaldynamics:duct_32:4>.withTag({DenseType: 2 as byte}));
rh(<thermaldynamics:duct_32:4>.withTag({DenseType: 1 as byte}));
rh(<thermaldynamics:duct_32:4>);
rh(<thermaldynamics:duct_32:6>.withTag({DenseType: 2 as byte}));
rh(<thermaldynamics:duct_32:6>.withTag({DenseType: 1 as byte}));
rh(<thermaldynamics:duct_32:6>);

recipes.remove(<thermaldynamics:duct_32:1>);
recipes.addShaped(<thermaldynamics:duct_32:1> * 6, 
    [[null, null, null],
     [<ore:plateTin>, <ore:plateLead>, <ore:plateTin>], 
     [null, null, null]]);

# Unused cell frames
rh(<thermalexpansion:frame:129>);
rh(<thermalexpansion:frame:130>);
rh(<thermalexpansion:frame:131>);
rh(<thermalexpansion:frame:132>);
rh(<thermalexpansion:frame:146>);
rh(<thermalexpansion:frame:147>);
rh(<thermalexpansion:frame:148>);

# Basic Cell Frame
recipes.remove(<thermalexpansion:frame:128>);
mods.forestry.ThermionicFabricator.addCast(<thermalexpansion:frame:128>,
    [[<ore:ingotElectrum>, <ore:ingotLead>, <ore:ingotElectrum>],
     [<ore:ingotLead>, <minecraft:redstone>, <ore:ingotLead>],
     [<ore:ingotElectrum>, <ore:ingotLead>, <ore:ingotElectrum>]], <liquid:glass> * 1000);

# Device Frame
recipes.remove(<thermalexpansion:frame:64>);
mods.forestry.ThermionicFabricator.addCast(<thermalexpansion:frame:64>,
    [[<ore:ingotAlubrass>, <ore:ingotTin>, <ore:ingotAlubrass>],
     [<ore:ingotTin>, <minecraft:redstone>, <ore:ingotTin>],
     [<ore:ingotAlubrass>, <ore:ingotTin>, <ore:ingotAlubrass>]], <liquid:glass> * 1000);

# Machine Frame
recipes.remove(<thermalexpansion:frame>);
mods.forestry.ThermionicFabricator.addCast(<thermalexpansion:frame>,
    [[<ore:ingotInvar>, <ore:ingotIron>, <ore:ingotInvar>],
     [<ore:ingotIron>, <minecraft:redstone>, <ore:ingotIron>],
     [<ore:ingotInvar>, <ore:ingotIron>, <ore:ingotInvar>]], <liquid:glass> * 1000);

# Servo
recipes.remove(<thermalfoundation:material:512>);
recipes.addShaped("Servo", <thermalfoundation:material:512>, 
    [[null, <minecraft:redstone>, null],
     [null, <forestry:thermionic_tubes:1>, null], 
     [null, <minecraft:redstone>, null]]);

# Dynamos

recipes.remove(<thermalexpansion:dynamo>);
recipes.remove(<thermalexpansion:dynamo:1>);
recipes.remove(<thermalexpansion:dynamo:2>);
recipes.remove(<thermalexpansion:dynamo:3>);
recipes.remove(<thermalexpansion:dynamo:4>);
recipes.remove(<thermalexpansion:dynamo:5>);

recipes.addShaped("Steam Dynamo", <thermalexpansion:dynamo>,
    [[null, <thermalfoundation:material:514>, null],
     [<ore:ingotIron>, <ore:gearCopper>, <ore:ingotIron>],
     [<ore:ingotCopper>, <thermalexpansion:frame:128>, <ore:ingotCopper>]]);

recipes.addShaped("Magmatic Dynamo", <thermalexpansion:dynamo:1>,
    [[null, <thermalfoundation:material:514>, null],
     [<ore:ingotIron>, <ore:gearInvar>, <ore:ingotIron>],
     [<ore:ingotInvar>, <thermalexpansion:frame:128>, <ore:ingotInvar>]]);

recipes.addShaped("Compression Dynamo", <thermalexpansion:dynamo:2>,
    [[null, <thermalfoundation:material:514>, null],
     [<ore:ingotIron>, <ore:gearTin>, <ore:ingotIron>],
     [<ore:ingotTin>, <thermalexpansion:frame:128>, <ore:ingotTin>]]);

recipes.addShaped("Reactant Dynamo", <thermalexpansion:dynamo:3>,
    [[null, <thermalfoundation:material:514>, null],
     [<ore:ingotIron>, <ore:gearLead>, <ore:ingotIron>],
     [<ore:ingotLead>, <thermalexpansion:frame:128>, <ore:ingotLead>]]);

recipes.addShaped("Enervation Dynamo", <thermalexpansion:dynamo:4>,
    [[null, <thermalfoundation:material:514>, null],
     [<ore:ingotIron>, <ore:gearElectrum>, <ore:ingotIron>],
     [<ore:ingotElectrum>, <thermalexpansion:frame:128>, <ore:ingotElectrum>]]);

recipes.addShaped("Numismatic Dynamo", <thermalexpansion:dynamo:5>,
    [[null, <thermalfoundation:material:514>, null],
     [<ore:ingotIron>, <ore:gearConstantan>, <ore:ingotIron>],
     [<ore:ingotConstantan>, <thermalexpansion:frame:128>, <ore:ingotConstantan>]]);

# Creative Flux Capacitor
mods.extendedcrafting.TableCrafting.addShaped(0, <thermalexpansion:capacitor:32000>.withTag({Energy: 25000000}),
	[[null, <ore:blockManyullyn>, <ore:blockManyullyn>, <ore:blockManyullyn>, <ore:blockManyullyn>, <ore:blockManyullyn>, null], 
	 [null, <ore:blockManyullyn>, <ore:blockManyullyn>, <ore:blockManyullyn>, <ore:blockManyullyn>, <ore:blockManyullyn>, null], 
	 [<refinedstorage:quartz_enriched_iron_block>, <ore:blockManyullyn>, <ore:blockDreadium>, <ore:blockManyullyn>, <ore:blockSuperium>, <ore:blockManyullyn>, <refinedstorage:quartz_enriched_iron_block>], 
	 [null, <ore:blockManyullyn>, <mysticalagradditions:stuff:69>, <ore:blockDraconium>, <mysticalagradditions:stuff:69>, <ore:blockManyullyn>, null], 
	 [null, <ore:blockDraconium>, <ore:blockDraconium>, <draconicevolution:draconium_capacitor:1>, <ore:blockDraconium>, <ore:blockDraconium>, null], 
	 [null, <ore:blockDraconium>, <ore:blockDraconium>, <ore:blockDraconium>, <ore:blockDraconium>, <ore:blockDraconium>, null], 
	 [null, null, null, <refinedstorage:quartz_enriched_iron_block>, null, null, null]]);

# Creative Satchel
mods.extendedcrafting.TableCrafting.addShaped(0, <thermalexpansion:satchel:32000>.withTag({Accessible: 1 as byte}),
	[[<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:coinEnderium>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <ore:coinEnderium>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:blockManyullyn>, <ore:coinEnderium>, <ore:dustAerotheum>, <ore:dustPetrotheum>, <ore:dustAerotheum>, <ore:coinEnderium>, <ore:blockManyullyn>, <thaumcraft:metal_void>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustPyrotheum>, <ore:blockDraconium>, <draconicevolution:draconium_capacitor:2>, <ore:blockDraconium>, <ore:dustCryotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustCryotheum>, <ore:blockDraconium>, <thermalexpansion:satchel:4>, <ore:blockDraconium>, <ore:dustPyrotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustPyrotheum>, <ore:blockDraconium>, <ore:blockCurium246>, <ore:blockDraconium>, <ore:dustCryotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<thaumcraft:metal_void>, <ore:blockManyullyn>, <ore:coinEnderium>, <ore:dustPetrotheum>, <ore:dustAerotheum>, <ore:dustPetrotheum>, <ore:coinEnderium>, <ore:blockManyullyn>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:coinEnderium>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <ore:coinEnderium>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>]]);

# Creative Reservoir
mods.extendedcrafting.TableCrafting.addShaped(0, <thermalexpansion:reservoir:32000>.withTag({}),
	[[<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:coinEnderium>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <ore:coinEnderium>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:blockManyullyn>, <ore:coinEnderium>, <ore:dustAerotheum>, <ore:dustPetrotheum>, <ore:dustAerotheum>, <ore:coinEnderium>, <ore:blockManyullyn>, <thaumcraft:metal_void>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustPyrotheum>, <ore:blockDraconium>, <draconicevolution:draconium_capacitor:2>, <ore:blockDraconium>, <ore:dustCryotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustCryotheum>, <ore:blockDraconium>, <thermalexpansion:reservoir:4>, <ore:blockDraconium>, <ore:dustPyrotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustPyrotheum>, <ore:blockDraconium>, <ore:blockCurium246>, <ore:blockDraconium>, <ore:dustCryotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<thaumcraft:metal_void>, <ore:blockManyullyn>, <ore:coinEnderium>, <ore:dustPetrotheum>, <ore:dustAerotheum>, <ore:dustPetrotheum>, <ore:coinEnderium>, <ore:blockManyullyn>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:coinEnderium>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <ore:coinEnderium>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>]]);

# Creative Watering Can
mods.extendedcrafting.TableCrafting.addShaped(0, <thermalcultivation:watering_can:32000>.withTag({Water: 600000, Mode: 4}),
	[[<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:coinEnderium>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <ore:coinEnderium>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:blockManyullyn>, <ore:coinEnderium>, <ore:dustAerotheum>, <ore:dustPetrotheum>, <ore:dustAerotheum>, <ore:coinEnderium>, <ore:blockManyullyn>, <thaumcraft:metal_void>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustPyrotheum>, <ore:blockDraconium>, <draconicevolution:draconium_capacitor:2>, <ore:blockDraconium>, <ore:dustCryotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustCryotheum>, <ore:blockDraconium>, <thermalcultivation:watering_can:4>, <ore:blockDraconium>, <ore:dustPyrotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustPyrotheum>, <ore:blockDraconium>, <ore:blockCurium246>, <ore:blockDraconium>, <ore:dustCryotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<thaumcraft:metal_void>, <ore:blockManyullyn>, <ore:coinEnderium>, <ore:dustPetrotheum>, <ore:dustAerotheum>, <ore:dustPetrotheum>, <ore:coinEnderium>, <ore:blockManyullyn>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:coinEnderium>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <ore:coinEnderium>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>]]);

# Creative Fluxomagnet
mods.extendedcrafting.TableCrafting.addShaped(0, <thermalinnovation:magnet:32000>.withTag({Energy: 600000}),
	[[<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:coinEnderium>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <ore:coinEnderium>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:blockManyullyn>, <ore:coinEnderium>, <ore:dustAerotheum>, <ore:dustPetrotheum>, <ore:dustAerotheum>, <ore:coinEnderium>, <ore:blockManyullyn>, <thaumcraft:metal_void>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustPyrotheum>, <ore:blockDraconium>, <draconicevolution:draconium_capacitor:2>, <ore:blockDraconium>, <ore:dustCryotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustCryotheum>, <ore:blockDraconium>, <thermalinnovation:magnet:4>, <ore:blockDraconium>, <ore:dustPyrotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustPyrotheum>, <ore:blockDraconium>, <ore:blockCurium246>, <ore:blockDraconium>, <ore:dustCryotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<thaumcraft:metal_void>, <ore:blockManyullyn>, <ore:coinEnderium>, <ore:dustPetrotheum>, <ore:dustAerotheum>, <ore:dustPetrotheum>, <ore:coinEnderium>, <ore:blockManyullyn>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:coinEnderium>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <ore:coinEnderium>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>]]);

# Creative Fluxbore
mods.extendedcrafting.TableCrafting.addShaped(0, <thermalinnovation:drill:32000>.withTag({Energy: 600000, Mode: 4}),
	[[<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:coinEnderium>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <ore:coinEnderium>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:blockManyullyn>, <ore:coinEnderium>, <ore:dustAerotheum>, <ore:dustPetrotheum>, <ore:dustAerotheum>, <ore:coinEnderium>, <ore:blockManyullyn>, <thaumcraft:metal_void>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustPyrotheum>, <ore:blockDraconium>, <draconicevolution:draconium_capacitor:2>, <ore:blockDraconium>, <ore:dustCryotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustCryotheum>, <ore:blockDraconium>, <thermalinnovation:drill:4>, <ore:blockDraconium>, <ore:dustPyrotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustPyrotheum>, <ore:blockDraconium>, <ore:blockCurium246>, <ore:blockDraconium>, <ore:dustCryotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<thaumcraft:metal_void>, <ore:blockManyullyn>, <ore:coinEnderium>, <ore:dustPetrotheum>, <ore:dustAerotheum>, <ore:dustPetrotheum>, <ore:coinEnderium>, <ore:blockManyullyn>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:coinEnderium>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <ore:coinEnderium>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>]]);

# Creative Fluxsaw
mods.extendedcrafting.TableCrafting.addShaped(0, <thermalinnovation:saw:32000>.withTag({Energy: 600000, Mode: 4}),
	[[<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:coinEnderium>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <ore:coinEnderium>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:blockManyullyn>, <ore:coinEnderium>, <ore:dustAerotheum>, <ore:dustPetrotheum>, <ore:dustAerotheum>, <ore:coinEnderium>, <ore:blockManyullyn>, <thaumcraft:metal_void>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustPyrotheum>, <ore:blockDraconium>, <draconicevolution:draconium_capacitor:2>, <ore:blockDraconium>, <ore:dustCryotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustCryotheum>, <ore:blockDraconium>, <thermalinnovation:saw:4>, <ore:blockDraconium>, <ore:dustPyrotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustPyrotheum>, <ore:blockDraconium>, <ore:blockCurium246>, <ore:blockDraconium>, <ore:dustCryotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<thaumcraft:metal_void>, <ore:blockManyullyn>, <ore:coinEnderium>, <ore:dustPetrotheum>, <ore:dustAerotheum>, <ore:dustPetrotheum>, <ore:coinEnderium>, <ore:blockManyullyn>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:coinEnderium>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <ore:coinEnderium>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>]]);

# Creative Alchemical Quiver
mods.extendedcrafting.TableCrafting.addShaped(0, <thermalinnovation:quiver:32000>.withTag({}),
	[[<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:coinEnderium>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <ore:coinEnderium>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:blockManyullyn>, <ore:coinEnderium>, <ore:dustAerotheum>, <ore:dustPetrotheum>, <ore:dustAerotheum>, <ore:coinEnderium>, <ore:blockManyullyn>, <thaumcraft:metal_void>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustPyrotheum>, <ore:blockDraconium>, <draconicevolution:draconium_capacitor:2>, <ore:blockDraconium>, <ore:dustCryotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustCryotheum>, <ore:blockDraconium>, <thermalinnovation:quiver:4>, <ore:blockDraconium>, <ore:dustPyrotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustPyrotheum>, <ore:blockDraconium>, <ore:blockCurium246>, <ore:blockDraconium>, <ore:dustCryotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<thaumcraft:metal_void>, <ore:blockManyullyn>, <ore:coinEnderium>, <ore:dustPetrotheum>, <ore:dustAerotheum>, <ore:dustPetrotheum>, <ore:coinEnderium>, <ore:blockManyullyn>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:coinEnderium>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <ore:coinEnderium>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>]]);

# Creative Hypoinfuser
mods.extendedcrafting.TableCrafting.addShaped(0, <thermalinnovation:injector:32000>.withTag({}),
	[[<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:coinEnderium>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <ore:coinEnderium>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:blockManyullyn>, <ore:coinEnderium>, <ore:dustAerotheum>, <ore:dustPetrotheum>, <ore:dustAerotheum>, <ore:coinEnderium>, <ore:blockManyullyn>, <thaumcraft:metal_void>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustPyrotheum>, <ore:blockDraconium>, <draconicevolution:draconium_capacitor:2>, <ore:blockDraconium>, <ore:dustCryotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustCryotheum>, <ore:blockDraconium>, <thermalinnovation:injector:4>, <ore:blockDraconium>, <ore:dustPyrotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<ore:blockThaumium>, <ore:blockManyullyn>, <ore:dustPyrotheum>, <ore:blockDraconium>, <ore:blockCurium246>, <ore:blockDraconium>, <ore:dustCryotheum>, <ore:blockManyullyn>, <ore:blockThaumium>], 
	 [<thaumcraft:metal_void>, <ore:blockManyullyn>, <ore:coinEnderium>, <ore:dustPetrotheum>, <ore:dustAerotheum>, <ore:dustPetrotheum>, <ore:coinEnderium>, <ore:blockManyullyn>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <ore:coinEnderium>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <quark:duskbound_block>, <ore:coinEnderium>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>]]);

# Creative Conversion Kit
mods.extendedcrafting.TableCrafting.addShaped(0, <thermalfoundation:upgrade:256>,
	[[<ore:blockManyullyn>, <ore:blockManyullyn>, <ore:blockManyullyn>, <ore:blockManyullyn>, <ore:blockManyullyn>, <ore:blockManyullyn>, <ore:blockManyullyn>, <ore:blockManyullyn>, <ore:blockManyullyn>], 
	 [<ore:blockManyullyn>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockManyullyn>], 
	 [<ore:blockDraconium>, <ore:blockDark>, <thermalexpansion:reservoir:32000>, <thermalexpansion:satchel:32000>, <thermalcultivation:watering_can:32000>, <thermalinnovation:drill:32000>, <thermalinnovation:saw:32000>, <ore:blockDark>, <ore:blockDraconium>], 
	 [<ore:blockDraconium>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockDraconium>], 
	 [<ore:blockDraconium>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockDraconium>], 
	 [<ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <thermalinnovation:magnet:32000>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>], 
	 [<ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <thermalinnovation:quiver:32000>, <ore:blockDark>, <thermalinnovation:injector:32000>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>], 
	 [<thaumcraft:metal_void>, <ore:blockDark>, <mysticalagradditions:stuff:69>, <ore:blockDark>, <ore:blockDark>, <ore:blockDark>, <mysticalagradditions:stuff:69>, <ore:blockDark>, <thaumcraft:metal_void>], 
	 [<thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>, <thaumcraft:metal_void>]]);

#####

# Molten Quartz
mods.thermalexpansion.Crucible.addRecipe(<liquid:quartz>*74, <thaumcraft:nugget:9>, 250);
mods.thermalexpansion.Crucible.addRecipe(<liquid:quartz>*666, <minecraft:quartz>, 2000);
mods.thermalexpansion.Crucible.addRecipe(<liquid:quartz>*666, <nuclearcraft:gem_dust:2>, 2000);
mods.thermalexpansion.Crucible.addRecipe(<liquid:quartz>*1665, <minecraft:quartz_ore>, 5000);
mods.thermalexpansion.Crucible.addRecipe(<liquid:quartz>*2664, <minecraft:quartz_block>, 8000);

#############################################################################

## Reference:

# mods.thermalexpansion.Compactor.addMintRecipe(IItemStack output, IItemStack input, int energy);
# mods.thermalexpansion.Compactor.removeMintRecipe(IItemStack input);
 
# mods.thermalexpansion.Compactor.addPressRecipe(IItemStack output, IItemStack input, int energy);
# mods.thermalexpansion.Compactor.removePressRecipe(IItemStack input);
 
# mods.thermalexpansion.Compactor.addStorageRecipe(IItemStack output, IItemStack input, int energy);
# mods.thermalexpansion.Compactor.removeStorageRecipe(IItemStack input);
#    
# mods.thermalexpansion.Compactor.addGearRecipe(IItemStack output, IItemStack input, int energy);
# mods.thermalexpansion.Compactor.removeGearRecipe(IItemStack input);

# mods.thermalexpansion.Transposer.addExtractRecipe(ILiquidStack output, IItemStack input, int energy);
# mods.thermalexpansion.Transposer.addExtractRecipe(ILiquidStack output, IItemStack input, int energy, WeightedItemStack itemOut);
# mods.thermalexpansion.Transposer.addFillRecipe(IItemStack output, IItemStack input, ILiquidStack fluid, int energy);
 
# mods.thermalexpansion.Crucible.addRecipe(ILiquidStack output, IItemStack input, int energy);
# mods.thermalexpansion.Crucible.removeRecipe(IItemStack input);
 
# mods.thermalexpansion.Infuser.addRecipe(IItemStack output, IItemStack input, int energy);
# mods.thermalexpansion.Infuser.removeRecipe(IItemStack input);
 
# mods.thermalexpansion.InductionSmelter.addRecipe(IItemStack primaryOutput, IItemStack primaryInput, IItemStack secondaryInput, int energy, @Optional IItemStack secondaryOutput, @Optional int secondaryChance);
# mods.thermalexpansion.InductionSmelter.removeRecipe(input);
 
# mods.thermalexpansion.Insolator.addRecipe(IItemStack primaryOutput, IItemStack primaryInput, IItemStack secondaryInput, int energy, @Optional IItemStack secondaryOutput, @Optional int secondaryChance);
# mods.thermalexpansion.Insolator.removeRecipe(IItemStack primaryInput, IItemStack secondaryInput);
 
# mods.thermalexpansion.Pulverizer.addRecipe(IItemStack output, IItemStack input, int energy, @Optional IItemStack secondaryOutput, @Optional int secondaryChance);
# mods.thermalexpansion.Pulverizer.removeRecipe(IItemStack input);
 
# mods.thermalexpansion.Refinery.addRecipe(ILiquidStack output, IItemStack outputItem, ILiquidStack input, int energy);
# mods.thermalexpansion.Refinery.removeRecipe(ILiquidStack input);
 
# mods.thermalexpansion.Sawmill.addRecipe(IItemStack output, IItemStack input, int energy, @Optional IItemStack secondaryOutput, @Optional int secondaryChance);
# mods.thermalexpansion.Sawmill.removeRecipe(IItemStack input);
