# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import crafttweaker.item.IItemStack as IItemStack;
import mods.jei.JEI.removeAndHide as rh;

print(">>> loading integrateddynamics.zs");

# Batteries
recipes.remove(<integrateddynamics:energy_battery>);
recipes.addShaped("Energy Battery", <integrateddynamics:energy_battery>,
    [[<integrateddynamics:crystalized_menril_chunk>, <integrateddynamics:crystalized_menril_block>, <integrateddynamics:crystalized_menril_chunk>],
     [<integrateddynamics:crystalized_menril_chunk>, <thermalexpansion:frame:128>, <integrateddynamics:crystalized_menril_chunk>],
     [<integrateddynamics:crystalized_menril_chunk>, <integrateddynamics:crystalized_menril_block>, <integrateddynamics:crystalized_menril_chunk>]]);

# Logic Cables
recipes.remove(<integrateddynamics:cable>);
recipes.addShaped("logic cable 1", <integrateddynamics:cable> * 12,
    [[<integrateddynamics:crystalized_menril_chunk>, <integrateddynamics:crystalized_menril_chunk>, <integrateddynamics:crystalized_menril_chunk>],
     [<xnet:netcable>, <ore:dustRedstone>, <xnet:netcable>],
     [<integrateddynamics:crystalized_menril_chunk>, <integrateddynamics:crystalized_menril_chunk>, <integrateddynamics:crystalized_menril_chunk>]]);
recipes.addShaped("logic cable 2", <integrateddynamics:cable> * 12,
    [[<integrateddynamics:crystalized_menril_chunk>, <xnet:netcable>, <integrateddynamics:crystalized_menril_chunk>],
     [<integrateddynamics:crystalized_menril_chunk>, <ore:dustRedstone>, <integrateddynamics:crystalized_menril_chunk>],
     [<integrateddynamics:crystalized_menril_chunk>, <xnet:netcable>, <integrateddynamics:crystalized_menril_chunk>]]);

# Variable Card
recipes.remove(<integrateddynamics:variable>);
recipes.addShaped("variable card", <integrateddynamics:variable>,
    [[<integrateddynamics:crystalized_menril_chunk>, <integrateddynamics:crystalized_menril_chunk>, <integrateddynamics:crystalized_menril_chunk>],
     [<integrateddynamics:crystalized_menril_chunk>, <refinedstorage:processor:5>, <integrateddynamics:crystalized_menril_chunk>],
     [<integrateddynamics:crystalized_menril_chunk>, <integrateddynamics:crystalized_menril_chunk>, <integrateddynamics:crystalized_menril_chunk>]]);

# Creative Energy Battery
mods.extendedcrafting.TableCrafting.addShaped(0, <integrateddynamics:creative_energy_battery>.withTag({energy: 1000000}),
	[[<extendedcrafting:storage:7>, <extendedcrafting:storage:7>, <bloodarsenal:slate:3>, <bloodarsenal:slate:3>, <bloodarsenal:slate:3>, <bloodarsenal:slate:3>, <bloodarsenal:slate:3>, <extendedcrafting:storage:7>, <extendedcrafting:storage:7>], 
	 [<extendedcrafting:storage:7>, <ore:blockThorium230>, <ore:blockThorium230>, <ore:blockThorium230>, <ore:blockDimensionalShard>, <ore:blockThorium230>, <ore:blockThorium230>, <ore:blockThorium230>, <extendedcrafting:storage:7>], 
	 [<bloodarsenal:slate:3>, <ore:blockThorium230>, <rftools:powercell_advanced>, <ore:blockThorium230>, <ore:blockDimensionalShard>, <ore:blockThorium230>, <rftools:powercell_advanced>, <ore:blockThorium230>, <bloodarsenal:slate:3>], 
	 [<bloodarsenal:slate:3>, <ore:blockThorium230>, <ore:blockThorium230>, <ore:blockThorium230>, <ore:blockDimensionalShard>, <ore:blockThorium230>, <ore:blockThorium230>, <ore:blockThorium230>, <bloodarsenal:slate:3>], 
	 [<bloodmagic:input_routing_node>, <ore:blockDimensionalShard>, <simplyjetpacks:itemfluxpack>, <ore:blockDimensionalShard>, <nuclearcraft:fusion_core>, <ore:blockDimensionalShard>, <simplyjetpacks:itemfluxpack>, <ore:blockDimensionalShard>, <bloodmagic:output_routing_node>], 
	 [<bloodarsenal:slate:3>, <ore:blockThorium230>, <ore:blockThorium230>, <ore:blockThorium230>, <ore:blockDimensionalShard>, <ore:blockThorium230>, <ore:blockThorium230>, <ore:blockThorium230>, <bloodarsenal:slate:3>], 
	 [<bloodarsenal:slate:3>, <ore:blockThorium230>, <rftools:powercell_advanced>, <ore:blockThorium230>, <ore:blockDimensionalShard>, <ore:blockThorium230>, <rftools:powercell_advanced>, <ore:blockThorium230>, <bloodarsenal:slate:3>], 
	 [<extendedcrafting:storage:7>, <ore:blockThorium230>, <ore:blockThorium230>, <ore:blockThorium230>, <ore:blockDimensionalShard>, <ore:blockThorium230>, <ore:blockThorium230>, <ore:blockThorium230>, <extendedcrafting:storage:7>], 
	 [<extendedcrafting:storage:7>, <extendedcrafting:storage:7>, <bloodarsenal:slate:3>, <bloodarsenal:slate:3>, <bloodarsenal:slate:3>, <bloodarsenal:slate:3>, <bloodarsenal:slate:3>, <extendedcrafting:storage:7>, <extendedcrafting:storage:7>]]);
