# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading bloodmagic.zs");
	
# Rudimentary Snare
recipes.remove(<bloodmagic:soul_snare>);
recipes.addShaped("Rudimentary Snare", <bloodmagic:soul_snare>, 
    [[<natura:materials:7>, <evilcraft:dark_spike>, <natura:materials:7>],
     [<ore:ingotIron>, <ore:dustRedstone>, <ore:ingotIron>],
     [<natura:materials:7>, <evilcraft:dark_spike>, <natura:materials:7>]]);

# Hellfire Forge
recipes.remove(<bloodmagic:soul_forge>);
recipes.addShaped("Hellfire Forge", <bloodmagic:soul_forge>, 
    [[<ore:ingotIron>, null, <ore:ingotIron>],
     [<evilcraft:dark_power_gem_block>, <ore:ingotNickel>, <evilcraft:dark_power_gem_block>],
     [<evilcraft:dark_power_gem_block>, <thermalexpansion:frame:128>, <evilcraft:dark_power_gem_block>]]);

# Guide book
recipes.remove(<guideapi:bloodmagic-guide>);
recipes.addShapeless("Sanguine Scientium", <guideapi:bloodmagic-guide>, [<minecraft:book>,<bloodmagic:soul_snare>]);

# Blood Altar
recipes.remove(<bloodmagic:altar>);
recipes.addShaped("Blood Altar", <bloodmagic:altar>, 
    [[null, null, null],
     [<evilcraft:dark_power_gem_block>, <thermalexpansion:tank>, <evilcraft:dark_power_gem_block>], 
     [<thermalfoundation:material:357>, <bloodmagic:monster_soul>, <thermalfoundation:material:357>]]);

# Blood Orbs
mods.bloodmagic.BloodAltar.removeRecipe(<minecraft:diamond>);
mods.bloodmagic.BloodAltar.addRecipe(<bloodmagic:blood_orb>.withTag({orb: "bloodmagic:weak"}),
    <evilcraft:dark_power_gem>, 0, 2000, 12, 12);

mods.bloodmagic.BloodAltar.removeRecipe(<minecraft:redstone_block>);
mods.bloodmagic.BloodAltar.addRecipe(<bloodmagic:blood_orb>.withTag({orb: "bloodmagic:apprentice"}),
    <evilcraft:blood_orb:1>, 1, 5000, 25, 25);

mods.bloodmagic.BloodAltar.removeRecipe(<minecraft:gold_block>);
mods.bloodmagic.BloodAltar.addRecipe(<bloodmagic:blood_orb>.withTag({orb: "bloodmagic:magician"}),
    <thaumcraft:causality_collapser>, 2, 25000, 50, 50);

# master blood orb not changed

mods.bloodmagic.BloodAltar.removeRecipe(<minecraft:nether_star>);
mods.bloodmagic.BloodAltar.addRecipe(<bloodmagic:blood_orb>.withTag({orb: "bloodmagic:archmage"}),
    <minecraft:totem_of_undying>, 4, 80000, 200, 200);

# transcendant blood orb not changed

# Creative Activation Crystal
mods.extendedcrafting.TableCrafting.addShaped(0, <bloodmagic:activation_crystal:2>,
	[[null, null, null, null, <integrateddynamics:crystalized_chorus_block>, null, null, null, null], 
	 [null, null, null, <integrateddynamics:crystalized_chorus_block>, <bloodmagic:activation_crystal:1>, <integrateddynamics:crystalized_chorus_block>, null, null, null], 
	 [null, null, null, <integrateddynamics:crystalized_chorus_block>, <bloodmagic:item_demon_crystal:3>, <integrateddynamics:crystalized_chorus_block>, null, null, null], 
	 [null, null, <integrateddynamics:crystalized_chorus_block>, <ore:blockCrystalFlux>, <bloodmagic:item_demon_crystal:3>, <ore:blockCrystalFlux>, <integrateddynamics:crystalized_chorus_block>, null, null], 
	 [null, <integrateddynamics:crystalized_chorus_block>, <ore:blockCrystalFlux>, <bloodmagic:item_demon_crystal:3>, <evilcraft:creative_blood_drop>, <bloodmagic:item_demon_crystal:3>, <ore:blockCrystalFlux>, <integrateddynamics:crystalized_chorus_block>, null], 
	 [null, null, <integrateddynamics:crystalized_chorus_block>, <ore:blockCrystalFlux>, <bloodmagic:item_demon_crystal:3>, <ore:blockCrystalFlux>, <integrateddynamics:crystalized_chorus_block>, null, null], 
	 [null, null, null, <integrateddynamics:crystalized_chorus_block>, <bloodmagic:item_demon_crystal:3>, <integrateddynamics:crystalized_chorus_block>, null, null, null], 
	 [null, null, null, <integrateddynamics:crystalized_chorus_block>, <bloodmagic:activation_crystal:1>, <integrateddynamics:crystalized_chorus_block>, null, null, null], 
	 [null, null, null, null, <integrateddynamics:crystalized_chorus_block>, null, null, null, null]]);

# Creative Sacrificial Dagger
mods.extendedcrafting.TableCrafting.addShaped(0, <bloodmagic:sacrificial_dagger:1>.withTag({sacrifice: 0 as byte}),
	[[null, null, null, null, null, null, null, null, <ore:ingotEthaxium>], 
	 [null, null, null, null, null, null, <ore:ingotFiery>, <ore:ingotEthaxium>, null], 
	 [null, null, null, null, null, <ore:ingotFiery>, <ore:ingotEthaxium>, <ore:ingotTough>, null], 
	 [null, null, null, null, <ore:ingotFiery>, <ore:ingotEthaxium>, <ore:ingotTough>, null, null], 
	 [null, <integrateddynamics:crystalized_chorus_block>, null, <ore:ingotFiery>, <ore:ingotEthaxium>, <ore:ingotTough>, null, null, null], 
	 [null, <ore:blockWither>, <integrateddynamics:crystalized_chorus_block>, <ore:ingotEthaxium>, <ore:ingotTough>, null, null, null, null], 
	 [null, null, <ore:blockWither>, <integrateddynamics:crystalized_chorus_block>, null, null, null, null, null], 
	 [null, <ore:blockWither>, null, <ore:blockWither>, <integrateddynamics:crystalized_chorus_block>, null, null, null, null], 
	 [<bloodmagic:activation_crystal:2>, null, null, null, null, null, null, null, null]]);
