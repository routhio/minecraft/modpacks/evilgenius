# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import mods.jei.JEI.removeAndHide as rh;

print(">>> loading miscrecipes.zs");

  
val wool = <minecraft:wool:*>;

# Forester's Manual
recipes.remove(<forestry:book_forester>);
recipes.addShapeless("Foresters Manual", <forestry:book_forester>, 
  [<minecraft:book>, <ore:treeSapling>, <ore:treeSapling>]);
  
# String
recipes.removeShapeless(<minecraft:string> * 4, [wool]);
recipes.removeShaped(<minecraft:string> * 4, [[wool, null, null],[null, null, null], [null, null, null]]);  
 
# Block of Charcoal
rh(<gardenstuff:stone_block>);

# Beacon
recipes.remove(<minecraft:beacon>);
recipes.addShaped("beacon", <minecraft:beacon>, 
  [[<ore:blockGlass>, <ore:blockGlass>, <ore:blockGlass>],
   [<ore:blockGlass>, <ore:netherStar>, <ore:blockGlass>],
   [<ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>]]);

# Wool dyeing
recipes.addShaped("wool", <minecraft:wool> * 8, [[wool, wool, wool],[wool, <ore:dyeWhite>, wool], [wool, wool, wool]]);
recipes.addShaped("wool1", <minecraft:wool:1> * 8, [[wool, wool, wool],[wool, <ore:dyeOrange>, wool], [wool, wool, wool]]);
recipes.addShaped("wool2", <minecraft:wool:2> * 8, [[wool, wool, wool],[wool, <ore:dyeMagenta>, wool], [wool, wool, wool]]);
recipes.addShaped("wool3", <minecraft:wool:3> * 8, [[wool, wool, wool],[wool, <ore:dyeLightBlue>, wool], [wool, wool, wool]]);
recipes.addShaped("wool4", <minecraft:wool:4> * 8, [[wool, wool, wool],[wool, <ore:dyeYellow>, wool], [wool, wool, wool]]);
recipes.addShaped("wool5", <minecraft:wool:5> * 8, [[wool, wool, wool],[wool, <ore:dyeLime>, wool], [wool, wool, wool]]);
recipes.addShaped("wool6", <minecraft:wool:6> * 8, [[wool, wool, wool],[wool, <ore:dyePink>, wool], [wool, wool, wool]]);
recipes.addShaped("wool7", <minecraft:wool:7> * 8, [[wool, wool, wool],[wool, <ore:dyeGray>, wool], [wool, wool, wool]]);
recipes.addShaped("wool8", <minecraft:wool:8> * 8, [[wool, wool, wool],[wool, <ore:dyeLightGray>, wool], [wool, wool, wool]]);
recipes.addShaped("wool9", <minecraft:wool:9> * 8, [[wool, wool, wool],[wool, <ore:dyeCyan>, wool], [wool, wool, wool]]);
recipes.addShaped("wool10", <minecraft:wool:10> * 8, [[wool, wool, wool],[wool, <ore:dyePurple>, wool], [wool, wool, wool]]);
recipes.addShaped("wool11", <minecraft:wool:11> * 8, [[wool, wool, wool],[wool, <ore:dyeBlue>, wool], [wool, wool, wool]]);
recipes.addShaped("wool12", <minecraft:wool:12> * 8, [[wool, wool, wool],[wool, <ore:dyeBrown>, wool], [wool, wool, wool]]);
recipes.addShaped("wool13", <minecraft:wool:13> * 8, [[wool, wool, wool],[wool, <ore:dyeGreen>, wool], [wool, wool, wool]]);
recipes.addShaped("wool14", <minecraft:wool:14> * 8, [[wool, wool, wool],[wool, <ore:dyeRed>, wool], [wool, wool, wool]]);
recipes.addShaped("wool15", <minecraft:wool:15> * 8, [[wool, wool, wool],[wool, <ore:dyeBlack>, wool], [wool, wool, wool]]);
  
# BoP Grass, Dirt, and Netherrack.
recipes.addShaped("BoP Mycelial Netherrack", <biomesoplenty:grass:8> * 8, [[<ore:netherrack>, <ore:netherrack>, <ore:netherrack>],[<ore:netherrack>, <minecraft:mycelium>, <ore:netherrack>], [<ore:netherrack>, <ore:netherrack>, <ore:netherrack>]]);
recipes.addShaped("BoP Flowering Grass", <biomesoplenty:grass:7> * 8, [[<minecraft:double_plant:*>, <ore:grass>, <minecraft:double_plant:*>],[<ore:grass>, <minecraft:red_flower:8>, <ore:grass>], [<minecraft:double_plant:*>, <ore:grass>, <minecraft:double_plant:*>]]);
recipes.addShaped("BoP Overgrown Netherrack", <biomesoplenty:grass:6> * 8, [[<ore:netherrack>, <ore:netherrack>, <ore:netherrack>],[<ore:netherrack>, <ore:vine>, <ore:netherrack>], [<ore:netherrack>, <ore:netherrack>, <ore:netherrack>]]);
recipes.addShaped("BoP Origin Grass", <biomesoplenty:grass:5> * 16, [[<ore:sand>, <ore:grass>, <ore:sand>],[<biomesoplenty:sapling_1>, <biomesoplenty:sapling_1>, <biomesoplenty:sapling_1>], [<ore:sand>, <ore:grass>, <ore:sand>]]);
recipes.addShaped("BoP Silty Grass", <biomesoplenty:grass:4> * 8, [[<ore:sand>, <ore:grass>, <ore:sand>],[<ore:gravel>, <ore:dirt>, <ore:gravel>], [<ore:sand>, <ore:grass>, <ore:sand>]]);
recipes.addShaped("BoP Sandy Grass", <biomesoplenty:grass:3> * 8, [[<ore:sand>, <ore:grass>, <ore:sand>],[<ore:grass>, <ore:dirt>, <ore:grass>], [<ore:sand>, <ore:grass>, <ore:sand>]]);
recipes.addShaped("BoP Loamy Grass", <biomesoplenty:grass:2> * 8, [[<ore:grass>, <ore:grass>, <ore:grass>],[<ore:grass>, <ore:listAllwater>, <ore:grass>], [<ore:grass>, <ore:grass>, <ore:grass>]]);
recipes.addShaped("BoP Silty Dirt", <biomesoplenty:dirt:2> * 8, [[<ore:sand>, <ore:dirt>, <ore:sand>],[<ore:gravel>, <ore:dirt>, <ore:gravel>], [<ore:sand>, <ore:dirt>, <ore:sand>]]);
recipes.addShaped("BoP Sandy Dirt", <biomesoplenty:dirt:1> * 8, [[<ore:sand>, <ore:dirt>, <ore:sand>],[<ore:dirt>, <ore:dirt>, <ore:dirt>], [<ore:sand>, <ore:dirt>, <ore:sand>]]);
recipes.addShaped("BoP Loamy Dirt", <biomesoplenty:dirt> * 8, [[<ore:dirt>, <ore:dirt>, <ore:dirt>],[<ore:dirt>, <ore:listAllwater>, <ore:dirt>], [<ore:dirt>, <ore:dirt>, <ore:dirt>]]);

# Nametag
recipes.addShaped("Nametag", <minecraft:name_tag>, 
  [[null, <ore:paper>, <ore:cropFlax>],
   [<ore:paper>, <ore:dyeBlack>, <ore:paper>], 
   [<ore:paper>, <ore:paper>, null]]);
  
# Elytra
recipes.addShaped("Elytra", <minecraft:elytra>, 
  [[<minecraft:banner:15>, <minecraft:leather_chestplate>.anyDamage(), <minecraft:banner:15>],
   [<minecraft:banner:15>, <minecraft:chorus_fruit_popped>, <minecraft:banner:15>], 
   [null, null, null]]);
  
# Farming for Blockheads Market
recipes.remove(<farmingforblockheads:market>);
recipes.remove(<harvestcraft:market>);
recipes.addShaped("Farming for Blockheads Market", <farmingforblockheads:market>, 
  [[<ore:plankWood>, <ore:blockWool>, <ore:plankWood>],
   [<ore:logWood>, <ore:blockEmerald>, <ore:logWood>], 
   [<ore:logWood>, <ore:logWood>, <ore:logWood>]]);

# The Beneath's teleporter blockEmerald
recipes.addShaped("beneath teleporter", <beneath:teleporterbeneath>,
  [[<evilcraft:dark_block>, <evilcraft:dark_block>, <evilcraft:dark_block>],
   [<evilcraft:dark_block>, <twilightforest:castle_brick>, <evilcraft:dark_block>],
   [<evilcraft:dark_block>, <evilcraft:dark_block>, <evilcraft:dark_block>]]);
  
# Pam's Lavender Shortbread
recipes.remove(<harvestcraft:lavendershortbreaditem>);
recipes.addShapeless("Pam's Harvestcraft Lavender Shortbread", <harvestcraft:lavendershortbreaditem>, 
  [<ore:toolBakeware>, <ore:foodDough>, <ore:flowerLavender>]);
  
# Minecraft Boats from oreDict planks
recipes.addShaped("Boat", <minecraft:boat>, 
  [[<ore:plankWood>, null, <ore:plankWood>], 
   [<ore:plankWood>, <ore:plankWood>, <ore:plankWood>]]);

recipes.addShapeless("Wrought Iron", <gardenstuff:material:4>, [<ore:ingotIron>, <minecraft:flint>, <minecraft:flint>]);
recipes.addShapeless("chest2chest", <minecraft:chest> * 2, [<ore:chest>, <ore:chest>]);
recipes.addShapeless("slate", <rustic:slate> * 4, [<ore:stoneBasalt>, <ore:stoneBasalt>, <ore:stoneBasalt>, <ore:stoneLimestone>]);
recipes.addShapeless("Overgrown Stone1", <biomesoplenty:grass:1>, [<ore:materialStoneTool>, <minecraft:tallgrass:1>]);
recipes.addShapeless("Overgrown Stone2", <biomesoplenty:grass:1>, [<ore:materialStoneTool>, <ore:grass>]);
recipes.addShapeless("Overgrown Stone3", <biomesoplenty:grass:1>, [<ore:materialStoneTool>, <ore:vine>]);
  
rh(<gardenstuff:material>);
rh(<plustic:invarnugget>);
  
rh(<forestry:gear_tin>);
rh(<forestry:gear_bronze>);
rh(<forestry:gear_copper>); 

# Evil Egghead Trophy
mods.extendedcrafting.TableCrafting.addShaped(0, <simple_trophies:trophy>.withTag({TrophyColorGreen: 49, TrophyItem: {id: "contenttweaker:egghead", Count: 1 as byte, Damage: 0 as short}, TrophyColorBlue: 44, TrophyName: "Evil Egghead", TrophyColorRed: 179}),
	[[null, null, null, null, <ore:egg>, null, null, null, null], 
	 [null, null, null, null, <twilightforest:castle_brick>, <ore:egg>, null, null, null], 
	 [null, null, <ore:egg>, null, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <ore:egg>, null, null], 
	 [null, <ore:egg>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <ore:egg>, null], 
	 [null, <ore:egg>, <ore:fuelTBUOxide>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <twilightforest:castle_brick>, <ore:fuelTBUOxide>, <ore:egg>, null], 
	 [null, <ore:egg>, <ore:fuelTBUOxide>, <ore:fuelTBUOxide>, <twilightforest:castle_brick>, <ore:fuelTBUOxide>, <ore:fuelTBUOxide>, <ore:egg>, null], 
	 [null, null, <ore:egg>, <ore:egg>, <ore:egg>, <ore:egg>, <ore:egg>, null, null], 
	 [null, null, <extendedcrafting:singularity:3>, <extendedcrafting:singularity:1>, <extendedcrafting:singularity:5>, <extendedcrafting:singularity:7>, <extendedcrafting:singularity:2>, null, null], 
	 [<ore:blockBlackIron>, <ore:blockBlackIron>, <ore:blockBlackIron>, <ore:blockBlackIron>, <extendedcrafting:singularity:6>, <ore:blockBlackIron>, <ore:blockBlackIron>, <ore:blockBlackIron>, <ore:blockBlackIron>]]);
