# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading storagedrawers.zs");

# Creative Storage Upgrade
mods.extendedcrafting.TableCrafting.addShaped(0, <storagedrawers:upgrade_creative>,
    [[<storagedrawers:upgrade_storage:4>, <ore:stickWood>, <storagedrawers:upgrade_void>, <ore:stickWood>, <storagedrawers:upgrade_storage:4>], 
	 [<ore:stickWood>, <industrialforegoing:black_hole_controller>, <rebornstorage:storagecell:2>, <industrialforegoing:black_hole_controller>, <ore:stickWood>], 
	 [<storagedrawers:upgrade_void>, <rebornstorage:storagecell:3>, <bibliocraft:bookcasecreative>, <rebornstorage:storagecell:3>, <storagedrawers:upgrade_void>], 
	 [<ore:stickWood>, <industrialforegoing:black_hole_controller>, <rebornstorage:storagecell:3>, <industrialforegoing:black_hole_controller>, <ore:stickWood>], 
	 [<storagedrawers:upgrade_storage:4>, <ore:stickWood>, <storagedrawers:upgrade_void>, <ore:stickWood>, <storagedrawers:upgrade_storage:4>]]);

# Creative Vending Upgrade
recipes.addShaped("Creative Vending Upgrade", <storagedrawers:upgrade_creative:1>,
	[[<ore:stickWood>, <ore:stickWood>, <ore:stickWood>],
	 [<storagedrawers:upgrade_creative>, <thermalfoundation:upgrade:256>, <storagedrawers:upgrade_creative>],
	 [<ore:stickWood>, <ore:stickWood>, <ore:stickWood>]]);

# Createive Vending Dupe
recipes.addShaped("Creative Vending Dupe", <storagedrawers:upgrade_creative:1>*2, 
	[[null, null, null],
	 [null, <storagedrawers:upgrade_creative:1>, null],
	 [null, null, null]]);
