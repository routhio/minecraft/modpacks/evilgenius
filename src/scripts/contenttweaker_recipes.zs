# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#


print(">>> loading contenttweaker_recipes.zs");

# Evil Circuit
recipes.addShaped("evil circuit 1", <contenttweaker:chip>,
    [[<ore:dustRedstone>, <ore:ingotGold>, <ore:dustRedstone>],
     [<ore:dustRedstone>, <evilcraft:dark_gem>, <ore:dustRedstone>],
     [<ore:dustRedstone>, <ore:ingotGold>, <ore:dustRedstone>]]);

recipes.addShaped("evil circuit 2", <contenttweaker:chip>, 
    [[<ore:dustRedstone>, <ore:dustRedstone>, <ore:dustRedstone>],
     [<ore:ingotGold>, <evilcraft:dark_gem>, <ore:ingotGold>],
     [<ore:dustRedstone>, <ore:dustRedstone>, <ore:dustRedstone>]]);
