# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import crafttweaker.item.IItemStack as IItemStack;
import mods.jei.JEI.removeAndHide as rh;

print(">>> loading mysticalagriculture.zs");

# Infusion Crystal
rh(<mysticalagriculture:infusion_crystal>);

# Tiered Infusion Crystals
recipes.remove(<matc:inferiumcrystal>);
recipes.remove(<matc:prudentiumcrystal>);
recipes.remove(<matc:intermediumcrystal>);
recipes.remove(<matc:superiumcrystal>);
recipes.remove(<matc:supremiumcrystal>);
recipes.remove(<mysticalagriculture:master_infusion_crystal>);

mods.extendedcrafting.EnderCrafting.addShaped(<matc:inferiumcrystal>, 
    [[<mysticalagriculture:crafting:5>, <ore:essenceInferium>, <mysticalagriculture:crafting:5>],
     [<ore:essenceInferium>, <minecraft:diamond>, <ore:essenceInferium>],
     [<mysticalagriculture:crafting:5>, <ore:essenceInferium>, <mysticalagriculture:crafting:5>]]);

mods.extendedcrafting.EnderCrafting.addShaped(<matc:prudentiumcrystal>, 
    [[<ore:ingotThaumium>, <mysticalagriculture:crafting:1>, <mysticalagriculture:crafting:5>],
     [<mysticalagriculture:crafting:1>, <matc:inferiumcrystal>, <mysticalagriculture:crafting:1>],
     [<mysticalagriculture:crafting:5>, <mysticalagriculture:crafting:1>, <ore:ingotThaumium>]]);

mods.extendedcrafting.EnderCrafting.addShaped(<matc:intermediumcrystal>, 
    [[<ore:gemDarkPower>, <mysticalagriculture:crafting:2>, <mysticalagriculture:crafting:5>],
     [<mysticalagriculture:crafting:2>, <matc:prudentiumcrystal>, <mysticalagriculture:crafting:2>],
     [<mysticalagriculture:crafting:5>, <mysticalagriculture:crafting:2>, <ore:gemDarkPower>]]);

mods.extendedcrafting.EnderCrafting.addShaped(<matc:superiumcrystal>, 
    [[<ore:ingotVoid>, <mysticalagriculture:crafting:3>, <mysticalagriculture:crafting:5>],
     [<mysticalagriculture:crafting:3>, <matc:intermediumcrystal>, <mysticalagriculture:crafting:3>],
     [<mysticalagriculture:crafting:5>, <mysticalagriculture:crafting:3>, <ore:ingotVoid>]]);

mods.extendedcrafting.EnderCrafting.addShaped(<matc:supremiumcrystal>, 
    [[<bloodmagic:slate:2>, <mysticalagriculture:crafting:4>, <mysticalagriculture:crafting:5>],
     [<mysticalagriculture:crafting:4>, <matc:superiumcrystal>, <mysticalagriculture:crafting:4>],
     [<mysticalagriculture:crafting:5>, <mysticalagriculture:crafting:4>, <bloodmagic:slate:2>]]);

mods.extendedcrafting.EnderCrafting.addShaped(<mysticalagriculture:master_infusion_crystal>, 
    [[<ore:ingotDreadium>, <mysticalagradditions:insanium>, <mysticalagriculture:crafting:5>],
     [<mysticalagradditions:insanium>, <matc:supremiumcrystal>, <mysticalagradditions:insanium>],
     [<mysticalagriculture:crafting:5>, <mysticalagradditions:insanium>, <ore:ingotDreadium>]]);

# Crafting Seeds

recipes.remove(<mysticalagriculture:crafting:16>);
recipes.remove(<mysticalagriculture:crafting:17>);
recipes.remove(<mysticalagriculture:crafting:18>);
recipes.remove(<mysticalagriculture:crafting:19>);
recipes.remove(<mysticalagriculture:crafting:20>);
recipes.remove(<mysticalagriculture:crafting:21>);
recipes.remove(<mysticalagradditions:insanium:1>);

mods.extendedcrafting.EnderCrafting.addShaped(<mysticalagriculture:crafting:16>,
    [[null, <mysticalagriculture:crafting:5>, null],
     [<mysticalagriculture:crafting:5>, <minecraft:wheat_seeds>, <mysticalagriculture:crafting:5>],
     [null, <mysticalagriculture:crafting:5>, null]]);

mods.extendedcrafting.EnderCrafting.addShaped(<mysticalagriculture:crafting:17>,
    [[null, <ore:essenceInferium>, null],
     [<ore:essenceInferium>, <mysticalagriculture:crafting:16>, <ore:essenceInferium>],
     [null, <ore:essenceInferium>, null]]);

mods.extendedcrafting.EnderCrafting.addShaped(<mysticalagriculture:crafting:18>,
    [[null, <mysticalagriculture:crafting:1>, null],
     [<mysticalagriculture:crafting:1>, <mysticalagriculture:crafting:17>, <mysticalagriculture:crafting:1>],
     [null, <mysticalagriculture:crafting:1>, null]]);

mods.extendedcrafting.EnderCrafting.addShaped(<mysticalagriculture:crafting:19>,
    [[null, <mysticalagriculture:crafting:2>, null],
     [<mysticalagriculture:crafting:2>, <mysticalagriculture:crafting:18>, <mysticalagriculture:crafting:2>],
     [null, <mysticalagriculture:crafting:2>, null]]);

mods.extendedcrafting.EnderCrafting.addShaped(<mysticalagriculture:crafting:20>,
    [[null, <mysticalagriculture:crafting:3>, null],
     [<mysticalagriculture:crafting:3>, <mysticalagriculture:crafting:19>, <mysticalagriculture:crafting:3>],
     [null, <mysticalagriculture:crafting:3>, null]]);

mods.extendedcrafting.EnderCrafting.addShaped(<mysticalagriculture:crafting:21>,
    [[null, <mysticalagriculture:crafting:4>, null],
     [<mysticalagriculture:crafting:4>, <mysticalagriculture:crafting:20>, <mysticalagriculture:crafting:4>],
     [null, <mysticalagriculture:crafting:4>, null]]);

mods.extendedcrafting.EnderCrafting.addShaped(<mysticalagradditions:insanium:1>,
    [[null, <mysticalagradditions:insanium>, null],
     [<mysticalagradditions:insanium>, <mysticalagriculture:crafting:21>, <mysticalagradditions:insanium>],
     [null, <mysticalagradditions:insanium>, null]]);


# Tier 1 Seeds (Ender Crafting)
recipes.remove(<mysticalagriculture:tier1_inferium_seeds>);
recipes.remove(<mysticalagriculture:stone_seeds>);
recipes.remove(<mysticalagriculture:dirt_seeds>);
recipes.remove(<mysticalagriculture:nature_seeds>);
recipes.remove(<mysticalagriculture:wood_seeds>);
recipes.remove(<mysticalagriculture:water_seeds>);
recipes.remove(<mysticalagriculture:ice_seeds>);
recipes.remove(<mysticalagriculture:zombie_seeds>);

mods.extendedcrafting.EnderCrafting.addShaped(<mysticalagriculture:tier1_inferium_seeds>,
    [[<ore:essenceInferium>, <ore:essenceInferium>, <ore:essenceInferium>], 
	 [<ore:essenceInferium>, <minecraft:wheat_seeds>, <ore:essenceInferium>], 
	 [<ore:essenceInferium>, <ore:essenceInferium>, <ore:essenceInferium>]]);

mods.extendedcrafting.EnderCrafting.addShaped(<mysticalagriculture:stone_seeds>,
    [[<ore:stone>, <ore:essenceInferium>, <ore:stone>], 
	 [<ore:essenceInferium>, <mysticalagriculture:crafting:17>, <ore:essenceInferium>], 
	 [<ore:stone>, <ore:essenceInferium>, <ore:stone>]]);

mods.extendedcrafting.EnderCrafting.addShaped(<mysticalagriculture:dirt_seeds>,
    [[<ore:dirt>, <ore:essenceInferium>, <ore:dirt>], 
	 [<ore:essenceInferium>, <mysticalagriculture:crafting:17>, <ore:essenceInferium>], 
	 [<ore:dirt>, <ore:essenceInferium>, <ore:dirt>]]);

mods.extendedcrafting.EnderCrafting.addShaped(<mysticalagriculture:nature_seeds>,
	[[<mysticalagriculture:crafting:6>, <ore:essenceInferium>, <mysticalagriculture:crafting:6>],
	 [<ore:essenceInferium>, <mysticalagriculture:crafting:17>, <ore:essenceInferium>],
	 [<mysticalagriculture:crafting:6>, <ore:essenceInferium>, <mysticalagriculture:crafting:6>]]);

mods.extendedcrafting.EnderCrafting.addShaped(<mysticalagriculture:wood_seeds>,
    [[<ore:logWood>, <ore:essenceInferium>, <ore:logWood>], 
	 [<ore:essenceInferium>, <mysticalagriculture:crafting:17>, <ore:essenceInferium>], 
	 [<ore:logWood>, <ore:essenceInferium>, <ore:logWood>]]);

mods.extendedcrafting.EnderCrafting.addShaped(<mysticalagriculture:water_seeds>,
	[[<ore:listAllwater>, <ore:essenceInferium>, <ore:listAllwater>], 
	 [<ore:essenceInferium>, <mysticalagriculture:crafting:17>, <ore:essenceInferium>], 
	 [<ore:listAllwater>, <ore:essenceInferium>, <ore:listAllwater>]]);

mods.extendedcrafting.EnderCrafting.addShaped(<mysticalagriculture:ice_seeds>,
	[[<ore:ice>, <ore:essenceInferium>, <ore:ice>], 
	 [<ore:essenceInferium>, <mysticalagriculture:crafting:17>, <ore:essenceInferium>], 
	 [<ore:ice>, <ore:essenceInferium>, <ore:ice>]]);

mods.extendedcrafting.EnderCrafting.addShaped(<mysticalagriculture:zombie_seeds>,
	[[<mysticalagriculture:chunk:6>, <ore:essenceInferium>, <mysticalagriculture:chunk:6>], 
	 [<ore:essenceInferium>, <mysticalagriculture:crafting:17>, <ore:essenceInferium>], 
	 [<mysticalagriculture:chunk:6>, <ore:essenceInferium>, <mysticalagriculture:chunk:6>]]);

# Tier 2 Seeds (Starlight Crafting Alter)
recipes.remove(<mysticalagriculture:tier2_inferium_seeds>);
recipes.remove(<mysticalagriculture:fire_seeds>);
recipes.remove(<mysticalagriculture:dye_seeds>);
recipes.remove(<mysticalagriculture:nether_seeds>);
recipes.remove(<mysticalagriculture:coal_seeds>);
recipes.remove(<mysticalagriculture:pig_seeds>);
recipes.remove(<mysticalagriculture:chicken_seeds>);
recipes.remove(<mysticalagriculture:cow_seeds>);
recipes.remove(<mysticalagriculture:sheep_seeds>);
recipes.remove(<mysticalagriculture:slime_seeds>);
recipes.remove(<mysticalagriculture:silicon_seeds>);
recipes.remove(<mysticalagriculture:sulfur_seeds>);
recipes.remove(<mysticalagriculture:aluminum_seeds>);
recipes.remove(<mysticalagriculture:copper_seeds>);
recipes.remove(<mysticalagriculture:aluminum_brass_seeds>);
recipes.remove(<mysticalagriculture:marble_seeds>);
recipes.remove(<mysticalagriculture:limestone_seeds>);
recipes.remove(<mysticalagriculture:basalt_seeds>);
recipes.remove(<mysticalagriculture:apatite_seeds>);
recipes.remove(<mysticalagriculture:menril_seeds>);
recipes.remove(<mysticalagriculture:slate_seeds>);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("tier 2 inferium seeds", "", 20, [<aspect:terra>], <mysticalagriculture:tier2_inferium_seeds>,
    [[<ore:essencePrudentium>, <ore:essencePrudentium>, <ore:essencePrudentium>],
     [<ore:essencePrudentium>, <mysticalagriculture:tier1_inferium_seeds>, <ore:essencePrudentium>],
     [<ore:essencePrudentium>, <ore:essencePrudentium>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("fire seeds", "", 20, [<aspect:terra>], <mysticalagriculture:fire_seeds>,
    [[<ore:essencePrudentium>, <minecraft:lava_bucket>, <ore:essencePrudentium>],
     [<minecraft:lava_bucket>, <mysticalagriculture:crafting:18>, <minecraft:lava_bucket>],
     [<ore:essencePrudentium>, <minecraft:lava_bucket>, <ore:essencePrudentium>]]);
     
mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("dye seeds", "",20, [<aspect:terra>], <mysticalagriculture:dye_seeds>,
    [[<ore:essencePrudentium>, <mysticalagriculture:crafting:7>, <ore:essencePrudentium>],
     [<mysticalagriculture:crafting:7>, <mysticalagriculture:crafting:18>, <mysticalagriculture:crafting:7>],
     [<ore:essencePrudentium>, <mysticalagriculture:crafting:7>, <ore:essencePrudentium>]]);
     
mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("nether seeds", "",20, [<aspect:terra>], <mysticalagriculture:nether_seeds>,
    [[<ore:essencePrudentium>, <mysticalagriculture:crafting:8>, <ore:essencePrudentium>],
     [<mysticalagriculture:crafting:8>, <mysticalagriculture:crafting:18>, <mysticalagriculture:crafting:8>],
     [<ore:essencePrudentium>, <mysticalagriculture:crafting:8>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("coal seeds", "",20, [<aspect:terra>], <mysticalagriculture:coal_seeds>,
    [[<ore:essencePrudentium>, <minecraft:coal>, <ore:essencePrudentium>],
     [<minecraft:coal>, <mysticalagriculture:crafting:18>, <minecraft:coal>],
     [<ore:essencePrudentium>, <minecraft:coal>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("pig seeds", "",20, [<aspect:terra>], <mysticalagriculture:pig_seeds>,
    [[<ore:essencePrudentium>, <mysticalagriculture:chunk:7>, <ore:essencePrudentium>],
     [<mysticalagriculture:chunk:7>, <mysticalagriculture:crafting:18>, <mysticalagriculture:chunk:7>],
     [<ore:essencePrudentium>, <mysticalagriculture:chunk:7>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("chicken seeds", "",20, [<aspect:terra>], <mysticalagriculture:chicken_seeds>,
    [[<ore:essencePrudentium>, <mysticalagriculture:chunk:8>, <ore:essencePrudentium>],
     [<mysticalagriculture:chunk:8>, <mysticalagriculture:crafting:18>, <mysticalagriculture:chunk:8>],
     [<ore:essencePrudentium>, <mysticalagriculture:chunk:8>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("cow seeds", "",20, [<aspect:terra>], <mysticalagriculture:cow_seeds>,
    [[<ore:essencePrudentium>, <mysticalagriculture:chunk:9>, <ore:essencePrudentium>],
     [<mysticalagriculture:chunk:9>, <mysticalagriculture:crafting:18>, <mysticalagriculture:chunk:9>],
     [<ore:essencePrudentium>, <mysticalagriculture:chunk:9>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("sheep seeds", "",20, [<aspect:terra>], <mysticalagriculture:sheep_seeds>,
    [[<ore:essencePrudentium>, <mysticalagriculture:chunk:10>, <ore:essencePrudentium>],
     [<mysticalagriculture:chunk:10>, <mysticalagriculture:crafting:18>, <mysticalagriculture:chunk:10>],
     [<ore:essencePrudentium>, <mysticalagriculture:chunk:10>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("slime seeds", "",20, [<aspect:terra>], <mysticalagriculture:slime_seeds>,
    [[<ore:essencePrudentium>, <mysticalagriculture:chunk:11>, <ore:essencePrudentium>],
     [<mysticalagriculture:chunk:11>, <mysticalagriculture:crafting:18>, <mysticalagriculture:chunk:11>],
     [<ore:essencePrudentium>, <mysticalagriculture:chunk:11>, <ore:essencePrudentium>]]);
     
mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("silicon seeds", "",20, [<aspect:terra>], <mysticalagriculture:silicon_seeds>,
    [[<ore:essencePrudentium>, <ore:itemSilicon>, <ore:essencePrudentium>],
     [<ore:itemSilicon>, <mysticalagriculture:crafting:18>, <ore:itemSilicon>],
     [<ore:essencePrudentium>, <ore:itemSilicon>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("sulfur seeds", "",20, [<aspect:terra>], <mysticalagriculture:sulfur_seeds>,
    [[<ore:essencePrudentium>, <ore:dustSulfur>, <ore:essencePrudentium>],
     [<ore:dustSulfur>, <mysticalagriculture:crafting:18>, <ore:dustSulfur>],
     [<ore:essencePrudentium>, <ore:dustSulfur>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("aluminum seeds", "",20, [<aspect:terra>], <mysticalagriculture:aluminum_seeds>,
    [[<ore:essencePrudentium>, <ore:ingotAluminum>, <ore:essencePrudentium>],
     [<ore:ingotAluminum>, <mysticalagriculture:crafting:18>, <ore:ingotAluminum>],
     [<ore:essencePrudentium>, <ore:ingotAluminum>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("copper seeds", "",20, [<aspect:terra>], <mysticalagriculture:copper_seeds>,
    [[<ore:essencePrudentium>, <ore:ingotCopper>, <ore:essencePrudentium>],
     [<ore:ingotCopper>, <mysticalagriculture:crafting:18>, <ore:ingotCopper>],
     [<ore:essencePrudentium>, <ore:ingotCopper>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("aluminumbrass seeds", "",20, [<aspect:terra>], <mysticalagriculture:aluminum_brass_seeds>,
    [[<ore:essencePrudentium>, <ore:ingotAlubrass>, <ore:essencePrudentium>],
     [<ore:ingotAlubrass>, <mysticalagriculture:crafting:18>, <ore:ingotAlubrass>],
     [<ore:essencePrudentium>, <ore:ingotAlubrass>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("marble seeds", "",20, [<aspect:terra>], <mysticalagriculture:marble_seeds>,
    [[<ore:essencePrudentium>, <ore:stoneMarble>, <ore:essencePrudentium>],
     [<ore:stoneMarble>, <mysticalagriculture:crafting:18>, <ore:stoneMarble>],
     [<ore:essencePrudentium>, <ore:stoneMarble>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("limestone seeds", "",20, [<aspect:terra>], <mysticalagriculture:limestone_seeds>,
    [[<ore:essencePrudentium>, <ore:stoneLimestone>, <ore:essencePrudentium>],
     [<ore:stoneLimestone>, <mysticalagriculture:crafting:18>, <ore:stoneLimestone>],
     [<ore:essencePrudentium>, <ore:stoneLimestone>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("basalt seeds", "",20, [<aspect:terra>], <mysticalagriculture:basalt_seeds>,
    [[<ore:essencePrudentium>, <ore:stoneBasalt>, <ore:essencePrudentium>],
     [<ore:stoneBasalt>, <mysticalagriculture:crafting:18>, <ore:stoneBasalt>],
     [<ore:essencePrudentium>, <ore:stoneBasalt>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("apatite seeds", "",20, [<aspect:terra>], <mysticalagriculture:apatite_seeds>,
    [[<ore:essencePrudentium>, <ore:gemApatite>, <ore:essencePrudentium>],
     [<ore:gemApatite>, <mysticalagriculture:crafting:18>, <ore:gemApatite>],
     [<ore:essencePrudentium>, <ore:gemApatite>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("menril seeds", "",20, [<aspect:terra>], <mysticalagriculture:menril_seeds>,
    [[<ore:essencePrudentium>, <integrateddynamics:menril_log>, <ore:essencePrudentium>],
     [<integrateddynamics:menril_log>, <mysticalagriculture:crafting:18>, <integrateddynamics:menril_log>],
     [<ore:essencePrudentium>, <integrateddynamics:menril_log>, <ore:essencePrudentium>]]);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("slate seeds", "",20, [<aspect:terra>], <mysticalagriculture:slate_seeds>,
    [[<ore:essencePrudentium>, <ore:slate>, <ore:essencePrudentium>],
     [<ore:slate>, <mysticalagriculture:crafting:18>, <ore:slate>],
     [<ore:essencePrudentium>, <ore:slate>, <ore:essencePrudentium>]]);

# Tier 3 Seeds (5x5 crafting table)
recipes.remove(<mysticalagriculture:tier3_inferium_seeds>);
recipes.remove(<mysticalagriculture:iron_seeds>);
recipes.remove(<mysticalagriculture:nether_quartz_seeds>);
recipes.remove(<mysticalagriculture:glowstone_seeds>);
recipes.remove(<mysticalagriculture:redstone_seeds>);
recipes.remove(<mysticalagriculture:obsidian_seeds>);
recipes.remove(<mysticalagriculture:skeleton_seeds>);
recipes.remove(<mysticalagriculture:creeper_seeds>);
recipes.remove(<mysticalagriculture:spider_seeds>);
recipes.remove(<mysticalagriculture:rabbit_seeds>);
recipes.remove(<mysticalagriculture:guardian_seeds>);
recipes.remove(<mysticalagriculture:saltpeter_seeds>);
recipes.remove(<mysticalagriculture:tin_seeds>);
recipes.remove(<mysticalagriculture:bronze_seeds>);
recipes.remove(<mysticalagriculture:brass_seeds>);
recipes.remove(<mysticalagriculture:silver_seeds>);
recipes.remove(<mysticalagriculture:lead_seeds>);
recipes.remove(<mysticalagriculture:graphite_seeds>);
recipes.remove(<mysticalagriculture:blizz_seeds>);
recipes.remove(<mysticalagriculture:blitz_seeds>);
recipes.remove(<mysticalagriculture:basalz_seeds>);
recipes.remove(<mysticalagriculture:knightslime_seeds>);
recipes.remove(<mysticalagriculture:ardite_seeds>);
recipes.remove(<mysticalagriculture:quicksilver_seeds>);
recipes.remove(<mysticalagriculture:thaumium_seeds>);
recipes.remove(<mysticalagriculture:steeleaf_seeds>);
recipes.remove(<mysticalagriculture:ironwood_seeds>);
recipes.remove(<mysticalagriculture:coralium_seeds>);
recipes.remove(<mysticalagriculture:dark_gem_seeds>);
recipes.remove(<mysticalagriculture:ender_biotite_seeds>);
recipes.remove(<mysticalagriculture:quartz_enriched_iron_seeds>);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:tier3_inferium_seeds>,
    [[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:essenceIntermedium>, <ore:essenceIntermedium>, <mysticalagriculture:tier2_inferium_seeds>, <ore:essenceIntermedium>, <ore:essenceIntermedium>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:iron_seeds>,
    [[null, null, <ore:ingotIron>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:ingotIron>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:ingotIron>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:ingotIron>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:nether_quartz_seeds>,
    [[null, null, <ore:gemQuartz>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:gemQuartz>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:gemQuartz>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:gemQuartz>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:glowstone_seeds>,
    [[null, null, <ore:dustGlowstone>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:dustGlowstone>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:dustGlowstone>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:dustGlowstone>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:redstone_seeds>,
    [[null, null, <ore:dustRedstone>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:dustRedstone>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:dustRedstone>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:dustRedstone>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:obsidian_seeds>,
    [[null, null, <ore:obsidian>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:obsidian>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:obsidian>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:obsidian>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:skeleton_seeds>,
    [[null, null, <mysticalagriculture:chunk:12>, null, null],
	 [null, null, <ore:essenceIntermedium>, null, null],
	 [<mysticalagriculture:chunk:12>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <mysticalagriculture:chunk:12>],
	 [null, null, <ore:essenceIntermedium>, null, null],
	 [null, null, <mysticalagriculture:chunk:12>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:creeper_seeds>,
    [[null, null, <mysticalagriculture:chunk:13>, null, null],
	 [null, null, <ore:essenceIntermedium>, null, null],
	 [<mysticalagriculture:chunk:13>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <mysticalagriculture:chunk:13>],
	 [null, null, <ore:essenceIntermedium>, null, null],
	 [null, null, <mysticalagriculture:chunk:13>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:spider_seeds>,
    [[null, null, <mysticalagriculture:chunk:14>, null, null],
	 [null, null, <ore:essenceIntermedium>, null, null],
	 [<mysticalagriculture:chunk:14>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <mysticalagriculture:chunk:14>],
	 [null, null, <ore:essenceIntermedium>, null, null],
 	 [null, null, <mysticalagriculture:chunk:14>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:rabbit_seeds>,
    [[null, null, <mysticalagriculture:chunk:15>, null, null],
	 [null, null, <ore:essenceIntermedium>, null, null],
	 [<mysticalagriculture:chunk:15>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <mysticalagriculture:chunk:15>],
	 [null, null, <ore:essenceIntermedium>, null, null],
	 [null, null, <mysticalagriculture:chunk:15>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:guardian_seeds>,
    [[null, null, <mysticalagriculture:chunk:16>, null, null],
	 [null, null, <ore:essenceIntermedium>, null, null],
	 [<mysticalagriculture:chunk:16>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <mysticalagriculture:chunk:16>],
	 [null, null, <ore:essenceIntermedium>, null, null],
	 [null, null, <mysticalagriculture:chunk:16>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:saltpeter_seeds>,
    [[null, null, <ore:dustSaltpeter>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:dustSaltpeter>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:dustSaltpeter>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:dustSaltpeter>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:tin_seeds>,
    [[null, null, <ore:ingotTin>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:ingotTin>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:ingotTin>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:ingotTin>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:bronze_seeds>,
    [[null, null, <ore:ingotBronze>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:ingotBronze>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:ingotBronze>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:ingotBronze>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:brass_seeds>,
    [[null, null, <ore:ingotBrass>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:ingotBrass>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:ingotBrass>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:ingotBrass>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:silver_seeds>,
    [[null, null, <ore:ingotSilver>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:ingotSilver>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:ingotSilver>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:ingotSilver>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:lead_seeds>,
    [[null, null, <ore:ingotLead>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:ingotLead>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:ingotLead>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:ingotLead>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:graphite_seeds>,
    [[null, null, <ore:ingotGraphite>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:ingotGraphite>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:ingotGraphite>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:ingotGraphite>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:blizz_seeds>,
    [[null, null, <mysticalagriculture:chunk:21>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<mysticalagriculture:chunk:21>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <mysticalagriculture:chunk:21>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <mysticalagriculture:chunk:21>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:blitz_seeds>,
    [[null, null, <mysticalagriculture:chunk:22>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<mysticalagriculture:chunk:22>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <mysticalagriculture:chunk:22>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <mysticalagriculture:chunk:22>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:basalz_seeds>,
    [[null, null, <mysticalagriculture:chunk:23>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<mysticalagriculture:chunk:23>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <mysticalagriculture:chunk:23>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <mysticalagriculture:chunk:23>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:knightslime_seeds>,
    [[null, null, <ore:ingotKnightslime>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:ingotKnightslime>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:ingotKnightslime>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:ingotKnightslime>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:ardite_seeds>,
    [[null, null, <ore:ingotArdite>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:ingotArdite>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:ingotArdite>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:ingotArdite>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:quicksilver_seeds>,
    [[null, null, <ore:quicksilver>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:quicksilver>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:quicksilver>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:quicksilver>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:thaumium_seeds>,
    [[null, null, <ore:ingotThaumium>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:ingotThaumium>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:ingotThaumium>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:ingotThaumium>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:steeleaf_seeds>,
    [[null, null, <ore:ingotSteeleaf>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:ingotSteeleaf>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:ingotSteeleaf>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:ingotSteeleaf>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:ironwood_seeds>,
    [[null, null, <ore:ingotIronwood>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:ingotIronwood>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:ingotIronwood>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:ingotIronwood>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:coralium_seeds>,
    [[null, null, <abyssalcraft:coralium>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<abyssalcraft:coralium>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <abyssalcraft:coralium>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <abyssalcraft:coralium>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:dark_gem_seeds>,
    [[null, null, <evilcraft:dark_gem>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<evilcraft:dark_gem>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <evilcraft:dark_gem>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <evilcraft:dark_gem>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:ender_biotite_seeds>,
    [[null, null, <ore:gemEnderBiotite>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<ore:gemEnderBiotite>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <ore:gemEnderBiotite>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <ore:gemEnderBiotite>, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:quartz_enriched_iron_seeds>,
    [[null, null, <refinedstorage:quartz_enriched_iron>, null, null],
	[null, null, <ore:essenceIntermedium>, null, null],
	[<refinedstorage:quartz_enriched_iron>, <ore:essenceIntermedium>, <mysticalagriculture:crafting:19>, <ore:essenceIntermedium>, <refinedstorage:quartz_enriched_iron>],
	[null, null, <ore:essenceIntermedium>, null, null],
	[null, null, <refinedstorage:quartz_enriched_iron>, null, null]]);

# Tier 4 Seeds (Infusion Altar)
recipes.remove(<mysticalagriculture:tier4_inferium_seeds>);
recipes.remove(<mysticalagriculture:gold_seeds>);
recipes.remove(<mysticalagriculture:lapis_lazuli_seeds>);
recipes.remove(<mysticalagriculture:end_seeds>);
recipes.remove(<mysticalagriculture:experience_seeds>);
recipes.remove(<mysticalagriculture:blaze_seeds>);
recipes.remove(<mysticalagriculture:ghast_seeds>);
recipes.remove(<mysticalagriculture:enderman_seeds>);
recipes.remove(<mysticalagriculture:steel_seeds>);
recipes.remove(<mysticalagriculture:nickel_seeds>);
recipes.remove(<mysticalagriculture:constantan_seeds>);
recipes.remove(<mysticalagriculture:electrum_seeds>);
recipes.remove(<mysticalagriculture:invar_seeds>);
recipes.remove(<mysticalagriculture:mithril_seeds>);
recipes.remove(<mysticalagriculture:ruby_seeds>);
recipes.remove(<mysticalagriculture:sapphire_seeds>);
recipes.remove(<mysticalagriculture:peridot_seeds>);
recipes.remove(<mysticalagriculture:amber_seeds>);
recipes.remove(<mysticalagriculture:topaz_seeds>);
recipes.remove(<mysticalagriculture:malachite_seeds>);
recipes.remove(<mysticalagriculture:tanzanite_seeds>);
recipes.remove(<mysticalagriculture:signalum_seeds>);
recipes.remove(<mysticalagriculture:lumium_seeds>);
recipes.remove(<mysticalagriculture:fluxed_electrum_seeds>);
recipes.remove(<mysticalagriculture:cobalt_seeds>);
recipes.remove(<mysticalagriculture:void_metal_seeds>);
recipes.remove(<mysticalagriculture:alumite_seeds>);
recipes.remove(<mysticalagriculture:knightmetal_seeds>);
recipes.remove(<mysticalagriculture:fiery_ingot_seeds>);
recipes.remove(<mysticalagriculture:thorium_seeds>);
recipes.remove(<mysticalagriculture:boron_seeds>);
recipes.remove(<mysticalagriculture:lithium_seeds>);
recipes.remove(<mysticalagriculture:magnesium_seeds>);
recipes.remove(<mysticalagriculture:abyssalnite_seeds>);

mods.thaumcraft.Infusion.registerRecipe("tier 4 inferium seeds", "", <mysticalagriculture:tier4_inferium_seeds>, 3, 
    [<aspect:herba>*60, <aspect:desiderium>*10 ], 
    <mysticalagriculture:tier3_inferium_seeds>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>]);

mods.thaumcraft.Infusion.registerRecipe("Gold Seeds", "", <mysticalagriculture:gold_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:desiderium>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotGold>, <ore:ingotGold>, <ore:ingotGold>, <ore:ingotGold> ]);

mods.thaumcraft.Infusion.registerRecipe("Lapis Lazuli Seeds", "", <mysticalagriculture:lapis_lazuli_seeds>, 3, 
    [<aspect:herba>*40, <aspect:praecantatio>*10, <aspect:vitreus>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:gemLapis>, <ore:gemLapis>, <ore:gemLapis>, <ore:gemLapis> ]);

mods.thaumcraft.Infusion.registerRecipe("End Seeds", "", <mysticalagriculture:end_seeds>, 3, 
    [<aspect:herba>*40, <aspect:vacuos>*10, <aspect:alienis>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <mysticalagriculture:crafting:9>, <mysticalagriculture:crafting:9>, <mysticalagriculture:crafting:9>, <mysticalagriculture:crafting:9> ]);

mods.thaumcraft.Infusion.registerRecipe("Experience Seeds", "", <mysticalagriculture:experience_seeds>, 3, 
    [<aspect:herba>*40, <aspect:auram>*10, <aspect:humanus>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <mysticalagriculture:chunk:5>, <mysticalagriculture:chunk:5>, <mysticalagriculture:chunk:5>, <mysticalagriculture:chunk:5> ]);

mods.thaumcraft.Infusion.registerRecipe("Blaze Seeds", "", <mysticalagriculture:blaze_seeds>, 4, 
    [<aspect:herba>*40, <aspect:spiritus>*10, <aspect:ignis>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <mysticalagriculture:chunk:17>, <mysticalagriculture:chunk:17>, <mysticalagriculture:chunk:17>, <mysticalagriculture:chunk:17> ]);

mods.thaumcraft.Infusion.registerRecipe("Ghast Seeds", "", <mysticalagriculture:ghast_seeds>, 4, 
    [<aspect:herba>*40, <aspect:spiritus>*10, <aspect:exanimis>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <mysticalagriculture:chunk:18>, <mysticalagriculture:chunk:18>, <mysticalagriculture:chunk:18>, <mysticalagriculture:chunk:18> ]);

mods.thaumcraft.Infusion.registerRecipe("Enderman Seeds", "", <mysticalagriculture:enderman_seeds>, 4, 
    [<aspect:herba>*40, <aspect:humanus>*10, <aspect:alienis>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <mysticalagriculture:chunk:19>, <mysticalagriculture:chunk:19>, <mysticalagriculture:chunk:19>, <mysticalagriculture:chunk:19> ]);

mods.thaumcraft.Infusion.registerRecipe("Steel Seeds", "", <mysticalagriculture:steel_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:ordo>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotSteel>, <ore:ingotSteel>, <ore:ingotSteel>, <ore:ingotSteel> ]);

mods.thaumcraft.Infusion.registerRecipe("Nickel Seeds", "", <mysticalagriculture:nickel_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:ordo>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotNickel>, <ore:ingotNickel>, <ore:ingotNickel>, <ore:ingotNickel> ]);

mods.thaumcraft.Infusion.registerRecipe("Constantan Seeds", "", <mysticalagriculture:constantan_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:permutatio>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotConstantan>, <ore:ingotConstantan>, <ore:ingotConstantan>, <ore:ingotConstantan> ]);

mods.thaumcraft.Infusion.registerRecipe("Electrum Seeds", "", <mysticalagriculture:electrum_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:permutatio>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotElectrum>, <ore:ingotElectrum>, <ore:ingotElectrum>, <ore:ingotElectrum> ]);

mods.thaumcraft.Infusion.registerRecipe("Invar Seeds", "", <mysticalagriculture:invar_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:permutatio>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotInvar>, <ore:ingotInvar>, <ore:ingotInvar>, <ore:ingotInvar> ]);

mods.thaumcraft.Infusion.registerRecipe("Mithril Seeds", "", <mysticalagriculture:mithril_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:praecantatio>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotMithril>, <ore:ingotMithril>, <ore:ingotMithril>, <ore:ingotMithril> ]);

mods.thaumcraft.Infusion.registerRecipe("Ruby Seeds", "", <mysticalagriculture:ruby_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:vitreus>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:gemRuby>, <ore:gemRuby>, <ore:gemRuby>, <ore:gemRuby> ]);

mods.thaumcraft.Infusion.registerRecipe("Sapphire Seeds", "", <mysticalagriculture:sapphire_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:vitreus>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:gemSapphire>, <ore:gemSapphire>, <ore:gemSapphire>, <ore:gemSapphire> ]);

mods.thaumcraft.Infusion.registerRecipe("Peridot Seeds", "", <mysticalagriculture:peridot_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:vitreus>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:gemPeridot>, <ore:gemPeridot>, <ore:gemPeridot>, <ore:gemPeridot> ]);

mods.thaumcraft.Infusion.registerRecipe("Amber Seeds", "", <mysticalagriculture:amber_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:vitreus>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:gemAmber>, <ore:gemAmber>, <ore:gemAmber>, <ore:gemAmber> ]);

mods.thaumcraft.Infusion.registerRecipe("Topaz Seeds", "", <mysticalagriculture:topaz_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:vitreus>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:gemTopaz>, <ore:gemTopaz>, <ore:gemTopaz>, <ore:gemTopaz> ]);

mods.thaumcraft.Infusion.registerRecipe("Malachite Seeds", "", <mysticalagriculture:malachite_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:vitreus>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:gemMalachite>, <ore:gemMalachite>, <ore:gemMalachite>, <ore:gemMalachite> ]);

mods.thaumcraft.Infusion.registerRecipe("Tanzanite Seeds", "", <mysticalagriculture:tanzanite_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:vitreus>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:gemTanzanite>, <ore:gemTanzanite>, <ore:gemTanzanite>, <ore:gemTanzanite> ]);

mods.thaumcraft.Infusion.registerRecipe("Signalum Seeds", "", <mysticalagriculture:signalum_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:permutatio>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotSignalum>, <ore:ingotSignalum>, <ore:ingotSignalum>, <ore:ingotSignalum> ]);

mods.thaumcraft.Infusion.registerRecipe("Lumium Seeds", "", <mysticalagriculture:lumium_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:permutatio>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotLumium>, <ore:ingotLumium>, <ore:ingotLumium>, <ore:ingotLumium> ]);

mods.thaumcraft.Infusion.registerRecipe("Fluxed Electrum Seeds", "", <mysticalagriculture:fluxed_electrum_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:perditio>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotElectrumFlux>, <ore:ingotElectrumFlux>, <ore:ingotElectrumFlux>, <ore:ingotElectrumFlux> ]);

mods.thaumcraft.Infusion.registerRecipe("Cobalt Seeds", "", <mysticalagriculture:cobalt_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:desiderium>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotCobalt>, <ore:ingotCobalt>, <ore:ingotCobalt>, <ore:ingotCobalt> ]);

mods.thaumcraft.Infusion.registerRecipe("Void Metal Seeds", "", <mysticalagriculture:void_metal_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:praecantatio>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotVoid>, <ore:ingotVoid>, <ore:ingotVoid>, <ore:ingotVoid> ]);

mods.thaumcraft.Infusion.registerRecipe("Alumite Seeds", "", <mysticalagriculture:alumite_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:permutatio>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotAlumite>, <ore:ingotAlumite>, <ore:ingotAlumite>, <ore:ingotAlumite> ]);

mods.thaumcraft.Infusion.registerRecipe("Knightmetal Seeds", "", <mysticalagriculture:knightmetal_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:instrumentum>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotKnightmetal>, <ore:ingotKnightmetal>, <ore:ingotKnightmetal>, <ore:ingotKnightmetal> ]);

mods.thaumcraft.Infusion.registerRecipe("Fiery Ingot Seeds", "", <mysticalagriculture:fiery_ingot_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:ignis>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotFiery>, <ore:ingotFiery>, <ore:ingotFiery>, <ore:ingotFiery> ]);

mods.thaumcraft.Infusion.registerRecipe("Thorium Seeds", "", <mysticalagriculture:thorium_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:alkimia>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotThorium>, <ore:ingotThorium>, <ore:ingotThorium>, <ore:ingotThorium> ]);

mods.thaumcraft.Infusion.registerRecipe("Boron Seeds", "", <mysticalagriculture:boron_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:alkimia>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotBoron>, <ore:ingotBoron>, <ore:ingotBoron>, <ore:ingotBoron> ]);

mods.thaumcraft.Infusion.registerRecipe("Lithium Seeds", "", <mysticalagriculture:lithium_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:alkimia>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotLithium>, <ore:ingotLithium>, <ore:ingotLithium>, <ore:ingotLithium> ]);

mods.thaumcraft.Infusion.registerRecipe("Magnesium Seeds", "", <mysticalagriculture:magnesium_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:alkimia>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotMagnesium>, <ore:ingotMagnesium>, <ore:ingotMagnesium>, <ore:ingotMagnesium> ]);

mods.thaumcraft.Infusion.registerRecipe("Abyssalnite Seeds", "", <mysticalagriculture:abyssalnite_seeds>, 3, 
    [<aspect:herba>*40, <aspect:metallum>*10, <aspect:alkimia>*10 ], 
    <mysticalagriculture:crafting:20>, 
    [<ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>, <ore:essenceSuperium>,
     <ore:ingotAbyssalnite>, <ore:ingotAbyssalnite>, <ore:ingotAbyssalnite>, <ore:ingotAbyssalnite> ]);


# Tier 5 Seeds (Blood Magic crafting thing?)
recipes.remove(<mysticalagriculture:tier5_inferium_seeds>);
recipes.remove(<mysticalagriculture:diamond_seeds>);
recipes.remove(<mysticalagriculture:emerald_seeds>);
recipes.remove(<mysticalagriculture:wither_skeleton_seeds>);
recipes.remove(<mysticalagriculture:uranium_seeds>);
recipes.remove(<mysticalagriculture:platinum_seeds>);
recipes.remove(<mysticalagriculture:iridium_seeds>);
recipes.remove(<mysticalagriculture:enderium_seeds>);
recipes.remove(<mysticalagriculture:manyullyn_seeds>);
recipes.remove(<mysticalagriculture:ender_amethyst_seeds>);
recipes.remove(<mysticalagriculture:draconium_seeds>);
recipes.remove(<mysticalagriculture:dreadium_seeds>);

mods.abyssalcraft.InfusionRitual.addRitual("tier5_inferium_seeds", 0, -1, 1000, false, 
    <mysticalagriculture:tier5_inferium_seeds>, <mysticalagriculture:tier4_inferium_seeds>, 
    [<ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>,
     <ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>]);

mods.abyssalcraft.InfusionRitual.addRitual("diamond_seeds", 0, -1, 1000, false, 
    <mysticalagriculture:diamond_seeds>, <mysticalagriculture:crafting:21>, 
    [<ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>,
     <ore:gemDiamond>, <ore:gemDiamond>, <ore:gemDiamond>, <ore:gemDiamond>]);

mods.abyssalcraft.InfusionRitual.addRitual("emerald_seeds", 0, -1, 1000, false, 
    <mysticalagriculture:emerald_seeds>, <mysticalagriculture:crafting:21>, 
    [<ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>,
     <ore:gemEmerald>, <ore:gemEmerald>, <ore:gemEmerald>, <ore:gemEmerald>]);

mods.abyssalcraft.InfusionRitual.addRitual("wither_skeleton_seeds", 0, -1, 1000, false, 
    <mysticalagriculture:wither_skeleton_seeds>, <mysticalagriculture:crafting:21>, 
    [<ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>,
     <mysticalagriculture:chunk:20>, <mysticalagriculture:chunk:20>, <mysticalagriculture:chunk:20>, <mysticalagriculture:chunk:20>]);

mods.abyssalcraft.InfusionRitual.addRitual("uranium_seeds", 0, -1, 1000, false, 
    <mysticalagriculture:uranium_seeds>, <mysticalagriculture:crafting:21>, 
    [<ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>,
     <ore:ingotUranium>, <ore:ingotUranium>, <ore:ingotUranium>, <ore:ingotUranium>]);

mods.abyssalcraft.InfusionRitual.addRitual("platinum_seeds", 0, -1, 1000, false, 
    <mysticalagriculture:platinum_seeds>, <mysticalagriculture:crafting:21>, 
    [<ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>,
     <ore:ingotPlatinum>, <ore:ingotPlatinum>, <ore:ingotPlatinum>, <ore:ingotPlatinum>]);

mods.abyssalcraft.InfusionRitual.addRitual("iridium_seeds", 0, -1, 1000, false, 
    <mysticalagriculture:iridium_seeds>, <mysticalagriculture:crafting:21>, 
    [<ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>,
     <ore:ingotIridium>, <ore:ingotIridium>, <ore:ingotIridium>, <ore:ingotIridium>]);

mods.abyssalcraft.InfusionRitual.addRitual("enderium_seeds", 0, -1, 1000, false, 
    <mysticalagriculture:enderium_seeds>, <mysticalagriculture:crafting:21>, 
    [<ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>,
     <ore:ingotEnderium>, <ore:ingotEnderium>, <ore:ingotEnderium>, <ore:ingotEnderium>]);

mods.abyssalcraft.InfusionRitual.addRitual("manyullyn_seeds", 0, -1, 1000, false, 
    <mysticalagriculture:manyullyn_seeds>, <mysticalagriculture:crafting:21>, 
    [<ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>,
     <ore:ingotManyullyn>, <ore:ingotManyullyn>, <ore:ingotManyullyn>, <ore:ingotManyullyn>]);

mods.abyssalcraft.InfusionRitual.addRitual("ender_amethyst_seeds", 0, -1, 1000, false, 
    <mysticalagriculture:ender_amethyst_seeds>, <mysticalagriculture:crafting:21>, 
    [<ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>,
     <ore:gemAmethyst>, <ore:gemAmethyst>, <ore:gemAmethyst>, <ore:gemAmethyst>]);

mods.abyssalcraft.InfusionRitual.addRitual("dreadium_seeds", 0, -1, 1000, false, 
    <mysticalagriculture:dreadium_seeds>, <mysticalagriculture:crafting:21>, 
    [<ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>, <ore:essenceSupremium>,
     <ore:ingotDreadium>, <ore:ingotDreadium>, <ore:ingotDreadium>, <ore:ingotDreadium>]);

# Tier 5 1/2 (7x7 crafting)
mods.extendedcrafting.TableCrafting.addShaped(<mysticalagriculture:draconium_seeds>,
	[[null, null, null, <draconicevolution:draconic_core>, null, null, null], 
	 [null, null, null, <ore:ingotDraconium>, null, null, null], 
	 [null, null, <ore:ingotDraconium>, <ore:essenceSupremium>, <ore:ingotDraconium>, null, null], 
	 [<draconicevolution:draconic_core>, <ore:ingotDraconium>, <ore:essenceSupremium>, <mysticalagriculture:crafting:21>, <ore:essenceSupremium>, <ore:ingotDraconium>, <draconicevolution:draconic_core>], 
	 [null, null, <ore:ingotDraconium>, <ore:essenceSupremium>, <ore:ingotDraconium>, null, null], 
	 [null, null, null, <ore:ingotDraconium>, null, null, null], 
	 [null, null, null, <draconicevolution:draconic_core>, null, null, null]]);

# Tier 6 Seeds (7x7 Crafting)
recipes.remove(<mysticalagradditions:tier6_inferium_seeds>);
recipes.remove(<mysticalagradditions:nether_star_seeds>);
recipes.remove(<mysticalagradditions:dragon_egg_seeds>);
recipes.remove(<mysticalagradditions:awakened_draconium_seeds>);

mods.extendedcrafting.TableCrafting.addShaped(<mysticalagradditions:tier6_inferium_seeds>,
	[[null, null, null, <draconicevolution:draconic_core>, null, null, null], 
	 [null, null, null, <ore:essenceInsanium>, null, null, null], 
	 [null, null, <ore:essenceInsanium>, <ore:essenceInsanium>, <ore:essenceInsanium>, null, null], 
	 [<draconicevolution:draconic_core>, <ore:essenceInsanium>, <ore:essenceInsanium>, <mysticalagriculture:tier5_inferium_seeds>, <ore:essenceInsanium>, <ore:essenceInsanium>, <draconicevolution:draconic_core>], 
	 [null, null, <ore:essenceInsanium>, <ore:essenceInsanium>, <ore:essenceInsanium>, null, null], 
	 [null, null, null, <ore:essenceInsanium>, null, null, null], 
	 [null, null, null, <draconicevolution:draconic_core>, null, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(<mysticalagradditions:nether_star_seeds>,
	[[null, null, null, <draconicevolution:draconic_core>, null, null, null], 
	 [null, null, null, <ore:netherStar>, null, null, null], 
	 [null, null, <ore:netherStar>, <ore:essenceInsanium>, <ore:netherStar>, null, null], 
	 [<draconicevolution:draconic_core>, <ore:netherStar>, <ore:essenceInsanium>, <mysticalagradditions:insanium:1>, <ore:essenceInsanium>, <ore:netherStar>, <draconicevolution:draconic_core>], 
	 [null, null, <ore:netherStar>, <ore:essenceInsanium>, <ore:netherStar>, null, null], 
	 [null, null, null, <ore:netherStar>, null, null, null], 
	 [null, null, null, <draconicevolution:draconic_core>, null, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(<mysticalagradditions:dragon_egg_seeds>,
	[[null, null, null, <draconicevolution:draconic_core>, null, null, null], 
	 [null, null, null, <mysticalagradditions:stuff:3>, null, null, null], 
	 [null, null, <mysticalagradditions:stuff:3>, <ore:essenceInsanium>, <mysticalagradditions:stuff:3>, null, null], 
	 [<draconicevolution:draconic_core>, <mysticalagradditions:stuff:3>, <ore:essenceInsanium>, <mysticalagradditions:insanium:1>, <ore:essenceInsanium>, <mysticalagradditions:stuff:3>, <draconicevolution:draconic_core>], 
	 [null, null, <mysticalagradditions:stuff:3>, <ore:essenceInsanium>, <mysticalagradditions:stuff:3>, null, null], 
	 [null, null, null, <mysticalagradditions:stuff:3>, null, null, null], 
	 [null, null, null, <draconicevolution:draconic_core>, null, null, null]]);

mods.extendedcrafting.TableCrafting.addShaped(<mysticalagradditions:awakened_draconium_seeds>,
	[[null, null, null, <draconicevolution:draconic_core>, null, null, null], 
	 [null, null, null, <ore:ingotDraconiumAwakened>, null, null, null], 
	 [null, null, <ore:ingotDraconiumAwakened>, <ore:essenceInsanium>, <ore:ingotDraconiumAwakened>, null, null], 
	 [<draconicevolution:draconic_core>, <ore:ingotDraconiumAwakened>, <ore:essenceInsanium>, <mysticalagradditions:insanium:1>, <ore:essenceInsanium>, <ore:ingotDraconiumAwakened>, <draconicevolution:draconic_core>], 
	 [null, null, <ore:ingotDraconiumAwakened>, <ore:essenceInsanium>, <ore:ingotDraconiumAwakened>, null, null], 
	 [null, null, null, <ore:ingotDraconiumAwakened>, null, null, null], 
	 [null, null, null, <draconicevolution:draconic_core>, null, null, null]]);

## Molten Base Smelting
recipes.remove(<mysticalagriculture:crafting:32>);
mods.tconstruct.Melting.addRecipe(<liquid:base_essence>*36, <mysticalagriculture:crafting:5>);
mods.thermalexpansion.Crucible.addRecipe(<liquid:base_essence>*36, <mysticalagriculture:crafting:5>, 2000);
mods.nuclearcraft.melter.addRecipe(<mysticalagriculture:crafting:5>, <liquid:base_essence>*36);

## Inferium Smelting
recipes.remove(<mysticalagriculture:crafting:33>);
mods.tconstruct.Melting.addRecipe(<liquid:inferium>*72, <mysticalagriculture:crafting>);
mods.thermalexpansion.Crucible.addRecipe(<liquid:inferium>*72, <mysticalagriculture:crafting>, 2000);
mods.nuclearcraft.melter.addRecipe(<mysticalagriculture:crafting>, <liquid:inferium>*72);

## Prudentium Smelting
recipes.remove(<mysticalagriculture:crafting:34>);
mods.tconstruct.Melting.addRecipe(<liquid:prudentium>*72, <mysticalagriculture:crafting:1>);
mods.thermalexpansion.Crucible.addRecipe(<liquid:prudentium>*72, <mysticalagriculture:crafting:1>, 2000);
mods.nuclearcraft.melter.addRecipe(<mysticalagriculture:crafting:1>, <liquid:prudentium>*72);

## Intermedium Smelting
recipes.remove(<mysticalagriculture:crafting:35>);
mods.tconstruct.Melting.addRecipe(<liquid:intermedium>*72, <mysticalagriculture:crafting:2>);
mods.thermalexpansion.Crucible.addRecipe(<liquid:intermedium>*72, <mysticalagriculture:crafting:2>, 2000);
mods.nuclearcraft.melter.addRecipe(<mysticalagriculture:crafting:2>, <liquid:intermedium>*72);

## Superium Smelting
recipes.remove(<mysticalagriculture:crafting:36>);
mods.tconstruct.Melting.addRecipe(<liquid:superium>*72, <mysticalagriculture:crafting:3>);
mods.thermalexpansion.Crucible.addRecipe(<liquid:superium>*72, <mysticalagriculture:crafting:3>, 2000);
mods.nuclearcraft.melter.addRecipe(<mysticalagriculture:crafting:3>, <liquid:superium>*72);

## Supremium Smelting
recipes.remove(<mysticalagriculture:crafting:37>);
mods.tconstruct.Melting.addRecipe(<liquid:supremium>*72, <mysticalagriculture:crafting:4>);
mods.thermalexpansion.Crucible.addRecipe(<liquid:supremium>*72, <mysticalagriculture:crafting:4>, 2000);
mods.nuclearcraft.melter.addRecipe(<mysticalagriculture:crafting:4>, <liquid:supremium>*72);

## Insanium Ingot Crafting
recipes.remove(<mysticalagradditions:insanium:2>);
mods.thaumcraft.Infusion.registerRecipe("Insanium Ingots", "", <mysticalagradditions:insanium:2>, 3,
    [<aspect:metallum>*50, <aspect:vitreus>*50, <aspect:desiderium>*50], 
    <mysticalagriculture:crafting:37>,
    [<mysticalagradditions:insanium>, <mysticalagradditions:insanium>, <mysticalagradditions:insanium>]);

## Soulstone (no crafting recipe please)
recipes.remove(<mysticalagriculture:soulstone>);

## Growth Accelerator
recipes.remove(<mysticalagriculture:growth_accelerator>);
recipes.addShaped("Growth Accelerator", <mysticalagriculture:growth_accelerator>,
    [[<ore:blockInferiumEssence>, <ore:blockInferium>, <ore:blockInferiumEssence>],
     [<ore:blockInferium>, <mysticalagriculture:fertilized_essence>, <ore:blockInferium>],
     [<ore:blockInferiumEssence>, <ore:blockInferium>, <ore:blockInferiumEssence>]]);


## Creative essence
mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagradditions:stuff:69>,
	[[null, null, <ore:blockInsanium>, null, null, null, <ore:blockInsanium>, null, null], 
	 [null, <ore:blockInsanium>, null, null, <ore:blockInsanium>, null, null, null, <ore:blockInsanium>], 
	 [null, <ore:blockInsanium>, null, <ore:blockInsanium>, <integrateddynamics:crystalized_chorus_block>, <ore:blockInsanium>, null, <ore:blockInsanium>, null], 
	 [null, <ore:blockInsanium>, <ore:blockInsanium>, <integrateddynamics:crystalized_chorus_block>, <extendedcrafting:singularity:50>, <ore:blockInsanium>, <ore:blockInsanium>, <ore:blockInsanium>, <ore:fuelHECm243Oxide>], 
	 [<ore:fuelHECm243Oxide>, <integrateddynamics:crystalized_chorus_block>, <integrateddynamics:crystalized_chorus_block>, <extendedcrafting:singularity:65>, <extendedcrafting:singularity:66>, <extendedcrafting:singularity:65>, <ore:blockInsanium>, <integrateddynamics:crystalized_chorus_block>, <ore:fuelHECm243Oxide>], 
	 [<ore:fuelHECm243Oxide>, <integrateddynamics:crystalized_chorus_block>, <ore:blockInsanium>, <integrateddynamics:crystalized_chorus_block>, <extendedcrafting:singularity:50>, <ore:blockInsanium>, <ore:blockInsanium>, <ore:blockInsanium>, <ore:fuelHECm243Oxide>], 
	 [<ore:fuelHECm243Oxide>, <integrateddynamics:crystalized_chorus_block>, <integrateddynamics:crystalized_chorus_block>, <integrateddynamics:crystalized_chorus_block>, <evilcraft:vengeance_essence>, <ore:blockInsanium>, <ore:blockInsanium>, <integrateddynamics:crystalized_chorus_block>, <ore:fuelHECm243Oxide>], 
	 [null, <ore:fuelHECm243Oxide>, <integrateddynamics:crystalized_chorus_block>, <integrateddynamics:crystalized_chorus_block>, <integrateddynamics:crystalized_chorus_block>, <ore:blockInsanium>, <ore:blockInsanium>, <ore:fuelHECm243Oxide>, null], 
	 [null, null, <ore:fuelHECm243Oxide>, <ore:fuelHECm243Oxide>, <ore:fuelHECm243Oxide>, <ore:fuelHECm243Oxide>, <ore:fuelHECm243Oxide>, null, null]]);

## Adding missing recipes for uranium and quartz enriched iron
recipes.addShaped("MA Quartz Enriched Iron", <refinedstorage:quartz_enriched_iron> * 4,
    [[<mysticalagriculture:quartz_enriched_iron_essence>, <mysticalagriculture:quartz_enriched_iron_essence>, <mysticalagriculture:quartz_enriched_iron_essence>],
     [<mysticalagriculture:quartz_enriched_iron_essence>, null, <mysticalagriculture:quartz_enriched_iron_essence>],
     [<mysticalagriculture:quartz_enriched_iron_essence>, <mysticalagriculture:quartz_enriched_iron_essence>, <mysticalagriculture:quartz_enriched_iron_essence>]]);

recipes.addShaped("MA Uranium Ingot", <nuclearcraft:ingot:4> * 2,
    [[<mysticalagriculture:uranium_essence>, <mysticalagriculture:uranium_essence>, <mysticalagriculture:uranium_essence>],
     [<mysticalagriculture:uranium_essence>, null, <mysticalagriculture:uranium_essence>],
     [<mysticalagriculture:uranium_essence>, <mysticalagriculture:uranium_essence>, <mysticalagriculture:uranium_essence>]]);

## THE END (whew)