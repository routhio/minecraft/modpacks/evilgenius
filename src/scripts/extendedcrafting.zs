# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import crafttweaker.item.IItemStack as IItemStack;
import mods.jei.JEI.removeAndHide as rh;

print(">>> loading extendedcrafting.zs");

# Black Iron Ingot
recipes.remove(<extendedcrafting:material>);
mods.thermalexpansion.InductionSmelter.addRecipe(<extendedcrafting:material>, 
    <thermalfoundation:material:770>*4, <thermalfoundation:material>, 4000);
mods.nuclearcraft.alloy_furnace.addRecipe(<thermalfoundation:material:770>*4, <thermalfoundation:material>, <extendedcrafting:material>);

# Black Iron Slate
recipes.remove(<extendedcrafting:material:2>);
mods.thermalexpansion.Compactor.addPressRecipe(<extendedcrafting:material:2>, <extendedcrafting:material>, 4000);
mods.nuclearcraft.pressurizer.addRecipe(<extendedcrafting:material>, <extendedcrafting:material:2>);

# Ender Ingot
recipes.remove(<extendedcrafting:material:36>);
mods.thermalexpansion.InductionSmelter.addRecipe(<extendedcrafting:material:36>, 
    <minecraft:ender_pearl>, <thermalfoundation:material>, 4000);
mods.nuclearcraft.alloy_furnace.addRecipe(<minecraft:ender_pearl>, <thermalfoundation:material>, <extendedcrafting:material:36>);

# Cyrstaltine
mods.extendedcrafting.TableCrafting.remove(<extendedcrafting:material:24>);
mods.extendedcrafting.TableCrafting.addShaped(0, <extendedcrafting:material:24>,
    [[<ore:ingotEnderium>, <ore:gemDiamond>, <ore:gemDiamond>, <ore:ingotTough>, <ore:ingotTough>, <ore:ingotTough>, <ore:gemDiamond>, <ore:gemDiamond>, <ore:ingotEnderium>], 
	 [<ore:gemDiamond>, <ore:ingotEthaxium>, <extendedcrafting:material:36>, <ore:ingotCobalt>, <ore:gemDarkPower>, <ore:ingotArdite>, <extendedcrafting:material:36>, <ore:ingotKnightmetal>, <ore:gemDiamond>], 
	 [<ore:gemDiamond>, <ore:ingotEthaxium>, <ore:ingotBronze>, <bloodmagic:slate:3>, <draconicevolution:awakened_core>, <bloodmagic:slate:3>, <ore:ingotBronze>, <ore:ingotKnightmetal>, <ore:gemDiamond>], 
	 [<ore:gemDiamond>, <ore:ingotEthaxium>, <extendedcrafting:material:36>, <ore:ingotArdite>, <ore:gemDarkPower>, <ore:ingotCobalt>, <extendedcrafting:material:36>, <ore:ingotKnightmetal>, <ore:gemDiamond>], 
	 [<ore:ingotEnderium>, <ore:gemDiamond>, <ore:gemDiamond>, <ore:ingotThaumium>, <ore:ingotThaumium>, <ore:ingotThaumium>, <ore:gemDiamond>, <ore:gemDiamond>, <ore:ingotEnderium>]]);

# Unused crafting machines
rh(<extendedcrafting:crafting_table>);
rh(<extendedcrafting:crafting_core>);
rh(<extendedcrafting:pedestal>);

# Ender Crafter
recipes.remove(<extendedcrafting:ender_crafter>);
recipes.addShaped("Ender Crafter", <extendedcrafting:ender_crafter>,
    [[<minecraft:ender_eye>, <ore:workbench>, <minecraft:ender_eye>],
     [<ore:blockBlackIron>, <extendedcrafting:storage:5>, <ore:blockBlackIron>],
     [<ore:blockBlackIron>, <ore:blockBlackIron>, <ore:blockBlackIron>]]);

# Ender Alternator
recipes.remove(<extendedcrafting:ender_alternator>);
recipes.addShaped("Ender Alternator", <extendedcrafting:ender_alternator>,
    [[null, <minecraft:ender_eye>, null],
     [null, <extendedcrafting:storage:5>, null], 
     [<ore:blockBlackIron>, <ore:blockBlackIron>, <ore:blockBlackIron>]]);

# Quantom Compressor
recipes.remove(<extendedcrafting:compressor>);
recipes.addShaped("Quantum Compressor", <extendedcrafting:compressor>,
    [[<ore:ingotBlackIron>, <extendedcrafting:material:18>, <ore:ingotBlackIron>],
     [<extendedcrafting:material:12>, <extendedcrafting:frame>, <extendedcrafting:material:12>],
     [<ore:ingotBlackIron>, <extendedcrafting:material:2>, <ore:ingotBlackIron>]]);

## Crafting Tables

# 3x3
rh(<extendedcrafting:material:8>);
rh(<extendedcrafting:material:14>);
rh(<extendedcrafting:table_basic>);

# 5x5
recipes.remove(<extendedcrafting:table_advanced>);
recipes.addShaped("advanced crafting table", <extendedcrafting:table_advanced>,
    [[<extendedcrafting:material:15>, <extendedcrafting:material:9>, <extendedcrafting:material:15>],
     [<ore:workbench>, <ore:blockGold>, <ore:workbench>],
     [<extendedcrafting:material:15>, <extendedcrafting:material:2>, <extendedcrafting:material:15>]]);
