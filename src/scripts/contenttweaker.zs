# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

#loader contenttweaker

import mods.contenttweaker.VanillaFactory;
import mods.contenttweaker.Item;

print(">>> loading contenttweaker.zs");

# Chip item
var itemChip = VanillaFactory.createItem("chip");
itemChip.register();

# Egghead item
var itemEgghead = VanillaFactory.createItem("egghead");
itemEgghead.register();
