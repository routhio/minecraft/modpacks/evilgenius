# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading simplyjetpacks.zs");

# Leadstone Thruster
recipes.remove(<simplyjetpacks:metaitemmods:26>);
recipes.addShaped("leadstone thruster", <simplyjetpacks:metaitemmods:26>,
    [[<ore:ingotLead>, <evilcraft:broom>.anyDamage(), <ore:ingotLead>],
     [<thermaldynamics:duct_0>, <thermalexpansion:dynamo:3>, <thermaldynamics:duct_0>],
     [<ore:ingotLead>, <ore:dustRedstone>, <ore:ingotLead>]]);

# Hardened Thruster
recipes.remove(<simplyjetpacks:metaitemmods:27>);
recipes.addShaped("hardened thruster", <simplyjetpacks:metaitemmods:27>,
    [[<ore:ingotInvar>, <evilcraft:broom>.anyDamage(), <ore:ingotInvar>],
     [<thermaldynamics:duct_0>, <thermalexpansion:dynamo:3>, <thermaldynamics:duct_0>],
     [<ore:ingotInvar>, <ore:dustRedstone>, <ore:ingotInvar>]]);

# Reinforced Thruster
recipes.remove(<simplyjetpacks:metaitemmods:28>);
recipes.addShaped("reinforced thruster", <simplyjetpacks:metaitemmods:28>,
    [[<ore:ingotElectrum>, <evilcraft:broom>.anyDamage(), <ore:ingotElectrum>],
     [<thermaldynamics:duct_0:2>, <thermalexpansion:dynamo:1>, <thermaldynamics:duct_0:2>],
     [<ore:ingotElectrum>, <forge:bucketfilled>.withTag({FluidName: "redstone", Amount: 1000}), <ore:ingotElectrum>]]);


# Resonant Thruster
recipes.remove(<simplyjetpacks:metaitemmods:29>);
recipes.addShaped("resonant thruster", <simplyjetpacks:metaitemmods:29>,
    [[<ore:ingotEnderium>, <evilcraft:broom>.anyDamage(), <ore:ingotEnderium>],
     [<thermaldynamics:duct_0:4>, <thermalexpansion:dynamo:4>, <thermaldynamics:duct_0:4>],
     [<ore:ingotEnderium>, <forge:bucketfilled>.withTag({FluidName: "redstone", Amount: 1000}), <ore:ingotEnderium>]]);

# Creative Flux Pack
mods.extendedcrafting.TableCrafting.addShaped(0, <simplyjetpacks:itemfluxpack>.withTag({Energy: 200000}),
    [[null, null, null, null, null, null, null], 
	 [null, <extendedcrafting:singularity:27>, <ore:plateDenseAbyssalnite>, <mysticalagradditions:stuff:69>, <ore:plateDenseAbyssalnite>, <extendedcrafting:singularity:27>, null], 
	 [null, <ore:plateDenseAbyssalnite>, <ore:blockDreadium>, <ore:blockDreadium>, <ore:blockDreadium>, <ore:plateDenseAbyssalnite>, null], 
	 [<ore:blockRockwool>, <mysticalagradditions:stuff:69>, <ore:blockDreadium>, <thermalexpansion:capacitor:32000>, <ore:blockDreadium>, <mysticalagradditions:stuff:69>, <ore:blockRockwool>], 
	 [null, <ore:plateDenseAbyssalnite>, <ore:blockDreadium>, <ore:blockDreadium>, <ore:blockDreadium>, <ore:plateDenseAbyssalnite>, null], 
	 [null, <extendedcrafting:singularity:27>, <ore:plateDenseAbyssalnite>, <mysticalagradditions:stuff:69>, <ore:plateDenseAbyssalnite>, <extendedcrafting:singularity:27>, null], 
	 [null, null, null, null, null, null, null]]);

# Creative Jetpack
mods.extendedcrafting.TableCrafting.addShaped(0, <simplyjetpacks:itemjetpack>.withTag({Energy: 200000, JetpackParticleType: 0}),
	[[null, null, <ore:blockBlackIron>, null, null, null, <ore:blockBlackIron>, null, null], 
	 [null, <ore:blockBlackIron>, <ore:blockDraconium>, <ore:blockBlackIron>, null, <ore:blockBlackIron>, <ore:blockDraconium>, <ore:blockBlackIron>, null], 
	 [null, <ore:blockBlackIron>, <ore:blockDraconium>, <ore:blockBlackIron>, null, <ore:blockBlackIron>, <ore:blockDraconium>, <ore:blockBlackIron>, null], 
	 [null, <ore:blockBlackIron>, <ore:blockDraconium>, <ore:blockBlackIron>, null, <ore:blockBlackIron>, <ore:blockDraconium>, <ore:blockBlackIron>, null], 
	 [null, <ore:blockBlackIron>, <abyssalcraft:oblivionshard>, <evilcraft:dark_power_gem_block>, <ore:blockBlackIron>, <evilcraft:dark_power_gem_block>, <abyssalcraft:oblivionshard>, <ore:blockBlackIron>, null], 
	 [null, <ore:blockBlackIron>, <ore:blockDraconium>, <simplyjetpacks:itemjetpack:18>, <simplyjetpacks:itemfluxpack>, <simplyjetpacks:itemjetpack:18>, <ore:blockDraconium>, <ore:blockBlackIron>, null], 
	 [null, null, <ore:blockBlackIron>, <biomesoplenty:gem_block:4>, <ore:blockBlackIron>, <biomesoplenty:gem_block:4>, <ore:blockBlackIron>, null, null], 
	 [null, <ore:gemShadow>, <biomesoplenty:gem_block>, <ore:gemShadow>, null, <ore:gemShadow>, <biomesoplenty:gem_block>, <ore:gemShadow>, null], 
	 [null, null, <ore:gemShadow>, null, null, null, <ore:gemShadow>, null, null]]);
     