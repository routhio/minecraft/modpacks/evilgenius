# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import crafttweaker.item.IItemStack as IItemStack;

print(">>> loading gears.zs");

# Remove all gear crafting recipes. Gears must be made in a compactor
var gearsToRemove = [
    <jaopca:item_gearabyssalnite>,
    <jaopca:item_gearcoralium>,
    <jaopca:item_geardark>,
    <jaopca:item_gearenderbiotite>,
    <redstonearsenal:material:96>,
    <thermalfoundation:material:24>,
    <thermalfoundation:material:25>,
    <thermalfoundation:material:26>,
    <thermalfoundation:material:27>,
    <thermalfoundation:material:256>,
    <thermalfoundation:material:257>,
    <thermalfoundation:material:258>,
    <thermalfoundation:material:259>,
    <thermalfoundation:material:260>,
    <thermalfoundation:material:261>,
    <thermalfoundation:material:262>,
    <thermalfoundation:material:263>,
    <thermalfoundation:material:264>,
    <thermalfoundation:material:288>,
    <thermalfoundation:material:289>,
    <thermalfoundation:material:290>,
    <thermalfoundation:material:291>,
    <thermalfoundation:material:292>,
    <thermalfoundation:material:293>,
    <thermalfoundation:material:294>,
    <thermalfoundation:material:295>
] as IItemStack[];

for gear in gearsToRemove {
    recipes.remove(gear);
}
