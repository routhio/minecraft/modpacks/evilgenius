# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading vampirism.zs");
	
# Sunscreen Beacon
mods.extendedcrafting.TableCrafting.addShaped(0, <vampirism:sunscreen_beacon>,
	[[<ore:blockGlassHardened>, <ore:blockGlassHardened>, <ore:blockGlassHardened>, <ore:blockGlassHardened>, <ore:blockGlassHardened>], 
	 [<ore:blockGlassHardened>, <minecraft:end_crystal>, <minecraft:end_crystal>, <minecraft:end_crystal>, <ore:blockGlassHardened>], 
	 [<ore:blockGlassHardened>, <minecraft:end_crystal>, <mysticalagradditions:stuff:69>, <minecraft:end_crystal>, <ore:blockGlassHardened>], 
	 [<ore:blockGlassHardened>, <bibliocraft:bookcasecreative>, <bibliocraft:bookcasecreative>, <bibliocraft:bookcasecreative>, <ore:blockGlassHardened>], 
	 [<ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>, <ore:blockThaumium>]]);
