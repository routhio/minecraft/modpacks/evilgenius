# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import crafttweaker.item.IItemStack as IItemStack;
import mods.jei.JEI.removeAndHide as rh;
import mods.jei.JEI.addDescription as ad;

print(">>> loading thermal.zs");

var craftingOnly = format.red("Only for crafting. Use Tinker's Tools instead.");

# Tools to completely remove from the game
var toolsToRemove = [
    <forestry:bronze_pickaxe>,
    <forestry:bronze_shovel>
] as IItemStack[];

for tool in toolsToRemove {
    rh(tool);
}

# Nerf Vanilla tools
var toolsToNerf = [
    <minecraft:wooden_sword>,
    <minecraft:stone_sword>,
    <minecraft:iron_sword>,
    <minecraft:golden_sword>,
    <minecraft:diamond_sword>,
    <minecraft:wooden_pickaxe>,
    <minecraft:stone_pickaxe>,
    <minecraft:iron_pickaxe>,
    <minecraft:golden_pickaxe>,
    <minecraft:diamond_pickaxe>,
    <minecraft:wooden_shovel>,
    <minecraft:stone_shovel>,
    <minecraft:iron_shovel>,
    <minecraft:golden_shovel>,
    <minecraft:diamond_shovel>,
    <minecraft:wooden_axe>,
    <minecraft:stone_axe>,
    <minecraft:iron_axe>,
    <minecraft:golden_axe>,
    <minecraft:diamond_axe>,
    <minecraft:wooden_hoe>,
    <minecraft:stone_hoe>,
    <minecraft:iron_hoe>,
    <minecraft:golden_hoe>,
    <minecraft:diamond_hoe>
] as IItemStack[];

for tool in toolsToNerf {
    tool.maxDamage = 1;
    tool.addTooltip(craftingOnly);
}
