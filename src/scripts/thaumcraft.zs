# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import crafttweaker.item.IItemStack as IItemStack;
import crafttweaker.item.IItemTransformer as IItemTransformer;
import mods.jei.JEI.removeAndHide as rh;

print(">>> loading thaumcraft.zs");

# Plates
recipes.remove(<thaumcraft:plate>);
rh(<thaumcraft:plate:1>);
recipes.remove(<thaumcraft:plate:2>);
recipes.remove(<thaumcraft:plate:3>);

# Thaumnomicon
<thaumcraft:thaumonomicon>.addTooltip(format.red(format.bold("WARNING: ")) + format.aqua("Recipes shown may not be correct."));
<thaumcraft:thaumonomicon>.addTooltip(format.aqua("Always refer to JEI when crafting."));

# Scribing Tools (remove the glass bottle version of the recipe)
recipes.removeShapeless(<thaumcraft:scribing_tools>,
    [<minecraft:glass_bottle>, <minecraft:feather>, <ore:dyeBlack>]);

# Phial
recipes.remove(<thaumcraft:phial>);
recipes.addShaped("phial", <thaumcraft:phial> * 8, 
    [[null, <minecraft:clay_ball>, null],
     [<ore:blockGlassHardened>, null, <ore:blockGlassHardened>],
     [null, <ore:blockGlassHardened>, null]]);

# Thaumometer
mods.thaumcraft.ArcaneWorkbench.removeRecipe(<thaumcraft:thaumometer>);
mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("thaumometer", "", 
    20, [<aspect:aer>,<aspect:terra>,<aspect:ignis>,<aspect:aqua>,<aspect:ordo>,<aspect:perditio>], 
    <thaumcraft:thaumometer>, 
    [[null, <minecraft:gold_ingot>, null],
     [<nuclearcraft:ingot:8>, <minecraft:glass_pane>, <nuclearcraft:ingot:8>],
     [null, <minecraft:gold_ingot>, null]]);

# Alchemical Brass
mods.thaumcraft.Crucible.removeRecipe(<thaumcraft:ingot:2>);
mods.thaumcraft.Crucible.registerRecipe("Brass", "METALLURGY@1", <thaumcraft:ingot:2>, <ore:ingotAlubrass>, [<aspect:instrumentum>*5]);

# Thaumium
mods.thaumcraft.Crucible.removeRecipe(<thaumcraft:ingot>);
mods.thaumcraft.Crucible.registerRecipe("Thaumium", "METALLURGY@2", <thaumcraft:ingot>, <ore:ingotElectrumFlux>, [<aspect:praecantatio>*5, <aspect:terra>*5]);

# Runic Matrix
mods.thaumcraft.ArcaneWorkbench.removeRecipe(<thaumcraft:infusion_matrix>);
recipes.addShaped("Runic Matrix", <thaumcraft:infusion_matrix>,
    [[<thaumcraft:stone_arcane_brick>, <thaumcraft:stone_arcane>, <thaumcraft:stone_arcane_brick>],
     [<thaumcraft:stone_arcane>, <twilightforest:lamp_of_cinders>.reuse(), <thaumcraft:stone_arcane>],
     [<thaumcraft:stone_arcane_brick>, <thaumcraft:stone_arcane>, <thaumcraft:stone_arcane_brick>]]);

# Flux Sponge
mods.extendedcrafting.TableCrafting.addShaped(0, <thaumcraft:creative_flux_sponge>,
	[[null, null, null, null, <minecraft:sponge>, <minecraft:sponge>, null, <minecraft:sponge>, null], 
	 [null, null, <minecraft:sponge>, <minecraft:sponge>, <ore:dustDull>, <minecraft:sponge>, null, <minecraft:sponge>, <minecraft:sponge>], 
	 [null, <minecraft:sponge>, <ore:dustDull>, <ore:dustDull>, <ore:dustDull>, <ore:dustDull>, <minecraft:sponge>, <minecraft:sponge>, null], 
	 [<minecraft:sponge>, <ore:dustDull>, <ore:dustZirconium>, <ore:dustZirconium>, <ore:dustZirconium>, <ore:dustZirconium>, <ore:dustZirconium>, <minecraft:sponge>, null], 
	 [<minecraft:sponge>, <ore:dustDull>, <tconstruct:materials:50>, <ore:dustDimensionalShard>, <evilcraft:garmonbozia>, <ore:dustDimensionalShard>, <tconstruct:materials:50>, <minecraft:sponge>, null], 
	 [<minecraft:sponge>, <ore:dustZirconium>, <ore:dustTopaz>, <ore:dustTopaz>, <ore:dustTopaz>, <ore:dustTopaz>, <ore:dustPeridot>, <minecraft:sponge>, null], 
	 [<minecraft:sponge>, <ore:dustZirconium>, <ore:dustPeridot>, <ore:dustPeridot>, <ore:dustPeridot>, <ore:dustPeridot>, <ore:dustPeridot>, <minecraft:sponge>, null], 
	 [<mysticalagradditions:stuff:69>, <minecraft:sponge>, <minecraft:sponge>, <minecraft:sponge>, <minecraft:sponge>, <minecraft:sponge>, <minecraft:sponge>, null, null], 
	 [<mysticalagradditions:stuff:69>, <mysticalagradditions:stuff:69>, null, null, null, null, null, null, null]]);
