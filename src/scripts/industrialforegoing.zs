# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading industrialforegoing.zs");

# Material Stonework Factory
recipes.remove(<industrialforegoing:material_stonework_factory>);
recipes.addShaped("if material stonework factory", <industrialforegoing:material_stonework_factory>, 
    [[<ore:itemRubber>, <ore:workbench>, <ore:itemRubber>],
     [<minecraft:iron_pickaxe>, <nuclearcraft:cobblestone_generator_compact>, <minecraft:furnace>], 
     [<ore:ingotInvar>, <industrialforegoing:pink_slime>, <ore:ingotInvar>]]);

## All the IF machines use nuclearcraft machine chassis' as their base component

recipes.remove(<industrialforegoing:petrified_fuel_generator>);
recipes.addShaped("IF petrified fuel generator", <industrialforegoing:petrified_fuel_generator>, 
    [[<ore:itemRubber>, <ore:gemDiamond>, <ore:itemRubber>],
     [<ore:gearGold>, <nuclearcraft:part:10>, <ore:gearGold>],
     [<ore:itemRubber>, <thermalexpansion:machine>, <ore:itemRubber>]]);

recipes.remove(<industrialforegoing:enchantment_refiner>);
recipes.addShaped("IF enchantment refiner", <industrialforegoing:enchantment_refiner>, 
    [[<ore:itemRubber>, <ore:enderpearl>, <ore:itemRubber>],
     [<minecraft:book>, <nuclearcraft:part:10>, <minecraft:enchanted_book>],
     [<ore:itemRubber>, <ore:gearDiamond>, <ore:itemRubber>]]);

recipes.remove(<industrialforegoing:enchantment_extractor>);
recipes.addShaped("IF enchantment extractor", <industrialforegoing:enchantment_extractor>, 
    [[<ore:itemRubber>, <minecraft:nether_brick>, <ore:itemRubber>],
     [<minecraft:book>, <nuclearcraft:part:10>, <minecraft:book>],
     [<ore:gemDiamond>, <ore:gearGold>, <ore:gemDiamond>]]);

recipes.remove(<industrialforegoing:enchantment_aplicator>);
recipes.addShaped("IF enchantment aplicator", <industrialforegoing:enchantment_aplicator>, 
    [[<ore:itemRubber>, <ore:itemRubber>, <ore:itemRubber>],
     [<minecraft:anvil>, <nuclearcraft:part:10>, <minecraft:anvil>],
     [<ore:gearIron>, <minecraft:anvil>, <ore:gearIron>]]);

recipes.remove(<industrialforegoing:mob_relocator>);
recipes.addShaped("IF mob relocator", <industrialforegoing:mob_relocator>, 
    [[<ore:itemRubber>, <minecraft:iron_sword>, <ore:itemRubber>],
     [<minecraft:book>, <nuclearcraft:part:10>, <minecraft:book>],
     [<ore:gearGold>, <ore:dustRedstone>, <ore:gearGold>]]);

recipes.remove(<industrialforegoing:potion_enervator>);
recipes.addShaped("IF potion enervator", <industrialforegoing:potion_enervator>, 
    [[<ore:itemRubber>, <minecraft:brewing_stand>, <ore:itemRubber>],
     [<ore:gearGold>, <nuclearcraft:part:10>, <ore:gearGold>],
     [<minecraft:repeater>, <ore:gearGold>, <minecraft:repeater>]]);

recipes.remove(<industrialforegoing:animal_independence_selector>);
recipes.addShaped("IF animal independence selector", <industrialforegoing:animal_independence_selector>, 
    [[<ore:itemRubber>, <ore:gemEmerald>, <ore:itemRubber>],
     [<ore:gemEmerald>, <nuclearcraft:part:10>, <ore:gemEmerald>],
     [<ore:dyePurple>, <ore:gearGold>, <ore:dyePurple>]]);

recipes.remove(<industrialforegoing:animal_stock_increaser>);
recipes.addShaped("IF animal stock increaser", <industrialforegoing:animal_stock_increaser>, 
    [[<ore:itemRubber>, <minecraft:golden_apple>, <ore:itemRubber>],
     [<minecraft:golden_carrot>, <nuclearcraft:part:10>, <minecraft:golden_carrot>],
     [<ore:dyePurple>, <ore:gearIron>, <ore:dyePurple>]]);

recipes.remove(<industrialforegoing:crop_sower>);
recipes.addShaped("IF crop sower", <industrialforegoing:crop_sower>, 
    [[<ore:itemRubber>, <minecraft:flower_pot>, <ore:itemRubber>],
     [<minecraft:piston>, <nuclearcraft:part:10>, <minecraft:piston>],
     [<ore:gearIron>, <ore:dustRedstone>, <ore:gearIron>]]);

recipes.remove(<industrialforegoing:crop_enrich_material_injector>);
recipes.addShaped("IF crop enrich material injector", <industrialforegoing:crop_enrich_material_injector>, 
    [[<ore:itemRubber>, <minecraft:glass_bottle>, <ore:itemRubber>],
     [<ore:leather>, <nuclearcraft:part:10>, <ore:leather>],
     [<ore:gearIron>, <ore:dustRedstone>, <ore:gearIron>]]);

recipes.remove(<industrialforegoing:crop_recolector>);
recipes.addShaped("IF crop recolector", <industrialforegoing:crop_recolector>, 
    [[<ore:itemRubber>, <minecraft:diamond_hoe>, <ore:itemRubber>],
     [<minecraft:iron_axe>, <nuclearcraft:part:10>, <minecraft:iron_axe>],
     [<ore:gearGold>, <ore:dustRedstone>, <ore:gearGold>]]);

recipes.remove(<industrialforegoing:black_hole_unit>);
recipes.addShaped("IF black hole unit", <industrialforegoing:black_hole_unit>, 
    [[<ore:itemRubber>, <ore:itemRubber>, <ore:itemRubber>],
     [<minecraft:ender_eye>, <ore:enderpearl>, <minecraft:ender_eye>],
     [<ore:chest>, <nuclearcraft:part:10>, <ore:chest>]]);

recipes.remove(<industrialforegoing:water_condensator>);
recipes.addShaped("IF water condensator", <industrialforegoing:water_condensator>, 
    [[<ore:itemRubber>, <ore:listAllwater>, <ore:itemRubber>],
     [<minecraft:piston>, <nuclearcraft:part:10>, <minecraft:piston>],
     [<ore:gearIron>, <ore:dustRedstone>, <ore:gearIron>]]);

recipes.remove(<industrialforegoing:water_resources_collector>);
recipes.addShaped("IF water resources collector", <industrialforegoing:water_resources_collector>, 
    [[<ore:itemRubber>, <minecraft:fishing_rod>, <ore:itemRubber>],
     [<minecraft:bucket>, <nuclearcraft:part:10>, <minecraft:bucket>],
     [<ore:gearIron>, <ore:dustRedstone>, <ore:gearIron>]]);

recipes.remove(<industrialforegoing:animal_resource_harvester>);
recipes.addShaped("IF animal resource harvester", <industrialforegoing:animal_resource_harvester>, 
    [[<ore:itemRubber>, <ore:itemRubber>, <ore:itemRubber>],
     [<minecraft:shears>, <minecraft:bucket>, <minecraft:shears>],
     [<ore:gearGold>, <nuclearcraft:part:10>, <ore:gearGold>]]);

recipes.remove(<industrialforegoing:mob_slaughter_factory>);
recipes.addShaped("IF mob slaughter factory", <industrialforegoing:mob_slaughter_factory>, 
    [[<ore:itemRubber>, <ore:gearGold>, <ore:itemRubber>],
     [<minecraft:iron_sword>, <nuclearcraft:part:10>, <minecraft:iron_sword>],
     [<minecraft:iron_axe>, <ore:dustRedstone>, <minecraft:iron_axe>]]);

recipes.remove(<industrialforegoing:mob_duplicator>);
recipes.addShaped("IF mob duplicator", <industrialforegoing:mob_duplicator>, 
    [[<ore:itemRubber>, <ore:cropNetherWart>, <ore:itemRubber>],
     [<minecraft:magma_cream>, <nuclearcraft:part:10>, <minecraft:magma_cream>],
     [<ore:gemEmerald>, <ore:dustRedstone>, <ore:gemEmerald>]]);

recipes.remove(<industrialforegoing:block_destroyer>);
recipes.addShaped("IF block destroyer", <industrialforegoing:block_destroyer>, 
    [[<ore:itemRubber>, <ore:gearGold>, <ore:itemRubber>],
     [<minecraft:iron_pickaxe>, <nuclearcraft:part:10>, <minecraft:iron_shovel>],
     [<ore:gearIron>, <ore:dustRedstone>, <ore:gearIron>]]);

recipes.remove(<industrialforegoing:block_placer>);
recipes.addShaped("IF block placer", <industrialforegoing:block_placer>, 
    [[<ore:itemRubber>, <minecraft:dropper>, <ore:itemRubber>],
     [<minecraft:dropper>, <nuclearcraft:part:10>, <minecraft:dropper>],
     [<ore:itemRubber>, <ore:dustRedstone>, <ore:itemRubber>]]);

recipes.remove(<industrialforegoing:tree_fluid_extractor>);
recipes.addShaped("IF tree fluid extractor", <industrialforegoing:tree_fluid_extractor>, 
    [[<ore:stone>, <ore:dustRedstone>, <ore:stone>],
     [<ore:stone>, <thermalexpansion:machine>, <ore:stone>],
     [<ore:stone>, <ore:gearIron>, <ore:stone>]]);

recipes.remove(<industrialforegoing:latex_processing_unit>);
recipes.addShaped("IF latex processing unit", <industrialforegoing:latex_processing_unit>, 
    [[<ore:ingotIron>, <ore:dustRedstone>, <ore:ingotIron>],
     [<thermalexpansion:machine>, <nuclearcraft:part:10>, <thermalexpansion:machine>],
     [<ore:ingotIron>, <ore:gearIron>, <ore:ingotIron>]]);

recipes.remove(<industrialforegoing:sewage_composter_solidifier>);
recipes.addShaped("IF sewege composter solidifier", <industrialforegoing:sewage_composter_solidifier>, 
    [[<ore:itemRubber>, <thermalexpansion:machine>, <ore:itemRubber>],
     [<minecraft:piston>, <nuclearcraft:part:10>, <minecraft:piston>],
     [<ore:ingotBrick>, <ore:dustRedstone>, <ore:ingotBrick>]]);

recipes.remove(<industrialforegoing:animal_byproduct_recolector>);
recipes.addShaped("IF animal byproduct recolector", <industrialforegoing:animal_byproduct_recolector>, 
    [[<ore:itemRubber>, <minecraft:bucket>, <ore:itemRubber>],
     [<ore:ingotBrick>, <nuclearcraft:part:10>, <ore:ingotBrick>],
     [<ore:ingotBrick>, <ore:dustRedstone>, <ore:ingotBrick>]]);

recipes.remove(<industrialforegoing:sludge_refiner>);
recipes.addShaped("IF sludge refinery", <industrialforegoing:sludge_refiner>, 
    [[<ore:itemRubber>, <minecraft:bucket>, <ore:itemRubber>],
     [<thermalexpansion:machine>, <nuclearcraft:part:10>, <thermalexpansion:machine>],
     [<ore:gearIron>, <ore:gearGold>, <ore:gearIron>]]);

recipes.remove(<industrialforegoing:mob_detector>);
recipes.addShaped("IF mob detector", <industrialforegoing:mob_detector>, 
    [[<ore:itemRubber>, <ore:itemRubber>, <ore:itemRubber>],
     [<minecraft:repeater>, <minecraft:comparator>, <minecraft:repeater>],
     [<minecraft:observer>, <nuclearcraft:part:10>, <minecraft:observer>]]);

recipes.remove(<industrialforegoing:lava_fabricator>);
recipes.addShaped("IF lava fabricator", <industrialforegoing:lava_fabricator>, 
    [[<ore:itemRubber>, <ore:obsidian>, <ore:itemRubber>],
     [<minecraft:magma_cream>, <nuclearcraft:part:10>, <minecraft:magma_cream>],
     [<ore:rodBlaze>, <ore:blockRedstone>, <ore:rodBlaze>]]);

recipes.remove(<industrialforegoing:bioreactor>);
recipes.addShaped("IF bioreactor", <industrialforegoing:bioreactor>, 
    [[<ore:itemRubber>, <minecraft:fermented_spider_eye>, <ore:itemRubber>],
     [<ore:slimeball>, <nuclearcraft:part:10>, <ore:slimeball>],
     [<ore:ingotBrick>, <ore:listAllsugar>, <ore:ingotBrick>]]);

recipes.remove(<industrialforegoing:biofuel_generator>);
recipes.addShaped("IF biofuel generator", <industrialforegoing:biofuel_generator>, 
    [[<ore:itemRubber>, <thermalexpansion:machine>, <ore:itemRubber>],
     [<minecraft:piston>, <nuclearcraft:part:10>, <minecraft:piston>],
     [<ore:rodBlaze>, <minecraft:piston>, <ore:rodBlaze>]]);

recipes.remove(<industrialforegoing:laser_base>);
recipes.addShaped("IF laser base", <industrialforegoing:laser_base>, 
    [[<ore:itemRubber>, <ore:blockGlowstone>, <ore:itemRubber>],
     [<ore:gearGold>, <ore:blockGlowstone>, <ore:gearGold>],
     [<ore:gearDiamond>, <nuclearcraft:part:10>, <ore:gearDiamond>]]);

recipes.remove(<industrialforegoing:laser_drill>);
recipes.addShaped("IF laser drill", <industrialforegoing:laser_drill>, 
    [[<ore:itemRubber>, <industrialforegoing:laser_lens>, <ore:itemRubber>],
     [<ore:blockGlassColorless>, <ore:blockGlowstone>, <ore:blockGlassColorless>],
     [<ore:gearDiamond>, <nuclearcraft:part:10>, <ore:gearDiamond>]]);

recipes.remove(<industrialforegoing:ore_processor>);
recipes.addShaped("IF ore processor", <industrialforegoing:ore_processor>, 
    [[<ore:itemRubber>, <minecraft:piston>, <ore:itemRubber>],
     [<minecraft:iron_pickaxe>, <nuclearcraft:part:10>, <minecraft:iron_pickaxe>],
     [<minecraft:book>, <ore:dustRedstone>, <minecraft:book>]]);

recipes.remove(<industrialforegoing:black_hole_controller>);
recipes.addShaped("IF black hole controller", <industrialforegoing:black_hole_controller>, 
    [[<ore:itemRubber>, <ore:blockDiamond>, <ore:itemRubber>],
     [<industrialforegoing:pink_slime_ingot>, <minecraft:ender_chest>, <industrialforegoing:pink_slime_ingot>],
     [<ore:itemRubber>, <nuclearcraft:part:10>, <ore:itemRubber>]]);

recipes.remove(<industrialforegoing:dye_mixer>);
recipes.addShaped("IF dye mixer", <industrialforegoing:dye_mixer>, 
    [[<ore:itemRubber>, <ore:dyeGreen>, <ore:itemRubber>],
     [<ore:dyeRed>, <nuclearcraft:part:10>, <ore:gemLapis>],
     [<ore:itemRubber>, <ore:gearGold>, <ore:itemRubber>]]);

recipes.remove(<industrialforegoing:enchantment_invoker>);
recipes.addShaped("IF enchantment invoker", <industrialforegoing:enchantment_invoker>, 
    [[<ore:itemRubber>, <minecraft:book>, <ore:itemRubber>],
     [<ore:gemDiamond>, <nuclearcraft:part:10>, <ore:gemDiamond>],
     [<ore:obsidian>, <ore:obsidian>, <ore:obsidian>]]);

recipes.remove(<industrialforegoing:spores_recreator>);
recipes.addShaped("IF spores recreator", <industrialforegoing:spores_recreator>, 
    [[<ore:itemRubber>, <ore:itemRubber>, <ore:itemRubber>],
     [<ore:listAllmushroom>, <nuclearcraft:part:10>, <ore:listAllmushroom>],
     [<ore:itemRubber>, <ore:gearIron>, <ore:itemRubber>]]);

recipes.remove(<industrialforegoing:animal_growth_increaser>);
recipes.addShaped("IF animal growth increaser", <industrialforegoing:animal_growth_increaser>, 
    [[<ore:itemRubber>, <ore:listAllgrain>, <ore:itemRubber>],
     [<ore:listAllgrain>, <nuclearcraft:part:10>, <ore:listAllgrain>],
     [<ore:dyePurple>, <ore:gearGold>, <ore:dyePurple>]]);

recipes.remove(<industrialforegoing:black_hole_tank>);
recipes.addShaped("IF black hole tank", <industrialforegoing:black_hole_tank>, 
    [[<ore:itemRubber>, <ore:itemRubber>, <ore:itemRubber>],
     [<minecraft:ender_eye>, <ore:enderpearl>, <minecraft:ender_eye>],
     [<minecraft:bucket>, <nuclearcraft:part:10>, <minecraft:bucket>]]);

recipes.remove(<industrialforegoing:resourceful_furnace>);
recipes.addShaped("IF resourceful furnace", <industrialforegoing:resourceful_furnace>, 
    [[<ore:itemRubber>, <minecraft:bucket>, <ore:itemRubber>],
     [<thermalexpansion:machine>, <nuclearcraft:part:10>, <thermalexpansion:machine>],
     [<ore:itemRubber>, <ore:gearGold>, <ore:itemRubber>]]);

recipes.remove(<industrialforegoing:villager_trade_exchanger>);
recipes.addShaped("IF villager trade exchanger", <industrialforegoing:villager_trade_exchanger>, 
    [[<ore:itemRubber>, <ore:ingotGold>, <ore:itemRubber>],
     [<ore:gemEmerald>, <nuclearcraft:part:10>, <ore:gemEmerald>],
     [<ore:itemRubber>, <ore:gearGold>, <ore:itemRubber>]]);

recipes.remove(<industrialforegoing:energy_field_provider>);
recipes.addShaped("IF energy field provider", <industrialforegoing:energy_field_provider>, 
    [[<ore:ingotGold>, <industrialforegoing:energy_field_addon>, <ore:ingotGold>],
     [<ore:ingotGold>, <nuclearcraft:part:10>, <ore:ingotGold>],
     [<minecraft:repeater>, <ore:gearDiamond>, <minecraft:repeater>]]);

recipes.remove(<industrialforegoing:oredictionary_converter>);
recipes.addShaped("IF oredictionary converter", <industrialforegoing:oredictionary_converter>, 
    [[<ore:itemRubber>, <ore:itemRubber>, <ore:itemRubber>],
     [<ore:itemRubber>, <nuclearcraft:part:10>, <ore:itemRubber>],
     [<ore:nuggetIron>, <ore:ingotIron>, <ore:blockIron>]]);

recipes.remove(<industrialforegoing:protein_reactor>);
recipes.addShaped("IF protein reactor", <industrialforegoing:protein_reactor>, 
    [[<ore:itemRubber>, <minecraft:porkchop>, <ore:itemRubber>],
     [<minecraft:egg>, <nuclearcraft:part:10>, <minecraft:egg>],
     [<ore:ingotBrick>, <minecraft:rabbit_foot>, <ore:ingotBrick>]]);

recipes.remove(<industrialforegoing:protein_generator>);
recipes.addShaped("IF protein generator", <industrialforegoing:protein_generator>, 
    [[<ore:itemRubber>, <thermalexpansion:machine>, <ore:itemRubber>],
     [<minecraft:piston>, <nuclearcraft:part:10>, <minecraft:piston>],
     [<minecraft:spider_eye>, <minecraft:piston>, <minecraft:spider_eye>]]);

recipes.remove(<industrialforegoing:hydrator>);
recipes.addShaped("IF hydrator", <industrialforegoing:hydrator>, 
    [[<ore:itemRubber>, <ore:listAllwater>, <ore:itemRubber>],
     [<ore:fertilizer>, <nuclearcraft:part:10>, <ore:fertilizer>],
     [<ore:gearIron>, <minecraft:piston>, <ore:gearIron>]]);

recipes.remove(<industrialforegoing:wither_builder>);
recipes.addShaped("IF wither builder", <industrialforegoing:wither_builder>, 
    [[<ore:itemRubber>, <ore:netherStar>, <ore:itemRubber>],
     [<minecraft:skull:1>, <nuclearcraft:part:10>, <minecraft:skull:1>],
     [<minecraft:soul_sand>, <minecraft:soul_sand>, <minecraft:soul_sand>]]);

recipes.remove(<industrialforegoing:fluid_pump>);
recipes.addShaped("IF fluid pump", <industrialforegoing:fluid_pump>, 
    [[<ore:itemRubber>, <minecraft:bucket>, <ore:itemRubber>],
     [<minecraft:lava_bucket>, <nuclearcraft:part:10>, <ore:listAllwater>],
     [<ore:itemRubber>, <ore:gearGold>, <ore:itemRubber>]]);

recipes.remove(<industrialforegoing:fluid_crafter>);
recipes.addShaped("IF fluid crafter", <industrialforegoing:fluid_crafter>, 
    [[<ore:itemRubber>, <ore:workbench>, <ore:itemRubber>],
     [<minecraft:bucket>, <nuclearcraft:part:10>, <minecraft:bucket>],
     [<ore:itemRubber>, <ore:gearGold>, <ore:itemRubber>]]);

recipes.remove(<industrialforegoing:plant_interactor>);
recipes.addShaped("IF plant interactor", <industrialforegoing:plant_interactor>, 
    [[<ore:itemRubber>, <minecraft:diamond_hoe>, <ore:itemRubber>],
     [<minecraft:diamond_hoe>, <nuclearcraft:part:10>, <minecraft:diamond_hoe>],
     [<ore:gearGold>, <ore:dustRedstone>, <ore:gearGold>]]);

recipes.remove(<industrialforegoing:item_splitter>);
recipes.addShaped("IF item splitter", <industrialforegoing:item_splitter>, 
    [[<ore:itemRubber>, <ore:chest>, <ore:itemRubber>],
     [<ore:hopper>, <nuclearcraft:part:10>, <ore:hopper>],
     [<ore:itemRubber>, <ore:gearIron>, <ore:itemRubber>]]);

recipes.remove(<industrialforegoing:fluiddictionary_converter>);
recipes.addShaped("IF fluiddictionary converter", <industrialforegoing:fluiddictionary_converter>, 
    [[<ore:itemRubber>, <ore:itemRubber>, <ore:itemRubber>],
     [<ore:blockGlassColorless>, <nuclearcraft:part:10>, <ore:blockGlassColorless>],
     [<minecraft:bucket>, <ore:gearIron>, <minecraft:bucket>]]);

recipes.remove(<industrialforegoing:froster>);
recipes.addShaped("IF froster", <industrialforegoing:froster>, 
    [[<ore:itemRubber>, <minecraft:ice>, <ore:itemRubber>],
     [<minecraft:snowball>, <nuclearcraft:part:10>, <minecraft:snowball>],
     [<ore:itemRubber>, <ore:gearGold>, <ore:itemRubber>]]);

recipes.remove(<industrialforegoing:ore_washer>);
recipes.addShaped("IF ore washer", <industrialforegoing:ore_washer>, 
    [[<industrialforegoing:pink_slime_ingot>, <industrialforegoing:meat_feeder>, <industrialforegoing:pink_slime_ingot>],
     [<ore:itemRubber>, <nuclearcraft:part:10>, <ore:itemRubber>],
     [<industrialforegoing:conveyor>, <ore:gearDiamond>, <industrialforegoing:conveyor>]]);

recipes.remove(<industrialforegoing:ore_fermenter>);
recipes.addShaped("IF ore fermenter", <industrialforegoing:ore_fermenter>, 
    [[<minecraft:iron_bars>, <ore:logWood>, <minecraft:iron_bars>],
     [<ore:logWood>, <ore:gearIron>, <ore:logWood>],
     [<minecraft:iron_bars>, <nuclearcraft:part:10>, <minecraft:iron_bars>]]);

recipes.remove(<industrialforegoing:ore_sieve>);
recipes.addShaped("IF ore sieve", <industrialforegoing:ore_sieve>, 
    [[<ore:itemRubber>, <industrialforegoing:pink_slime>, <ore:itemRubber>],
     [<minecraft:iron_bars>, <minecraft:iron_bars>, <minecraft:iron_bars>],
     [<ore:gearGold>, <nuclearcraft:part:10>, <ore:gearGold>]]);

