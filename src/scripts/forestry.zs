# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import crafttweaker.item.IItemStack as IItemStack;
import mods.jei.JEI.removeAndHide as rh;

print(">>> loading forestry.zs");

# Remove the bronze kits
rh(<forestry:carton>);
rh(<forestry:kit_pickaxe>);
rh(<forestry:kit_shovel>);

# Bee house
recipes.remove(<forestry:bee_house>);

# Sturdy Casing
recipes.remove(<forestry:sturdy_machine>);
recipes.addShapedMirrored("Sturdy Casing", <forestry:sturdy_machine>, 
    [[<ore:gearCopper>, <ore:ingotBronze>, <ore:ingotBronze>],
     [<ore:ingotBronze>, null, <ore:ingotBronze>], 
     [<ore:ingotBronze>, <ore:ingotBronze>, <ore:gearCopper>]]);

# Clockwork Engine
recipes.remove(<forestry:engine_clockwork>);
recipes.addShaped("Clockwork Engine", <forestry:engine_clockwork>,
    [[<ore:plankWood>, <ore:plankWood>, <ore:plankWood>],
     [null, <forestry:sturdy_machine>, null],
     [<ore:gearCopper>, <minecraft:piston>, <ore:gearCopper>]]);

## Reference:

# mods.forestry.Carpenter.addRecipe(IItemStack output, IIngredient[][] ingredients, int packagingTime, @Optional ILiquidStack fluidInput, @Optional IItemStack box)
# mods.forestry.Carpenter.removeRecipe(IItemStack output, @Optional ILiquidStack fluidInput);

# mods.forestry.Centrifuge.addRecipe(WeightedItemStack[] output, IItemStack ingredients, int packagingTime);
# mods.forestry.Centrifuge.removeRecipe(IIngredient input);

# mods.forestry.Fermenter.addRecipe(ILiquidStack fluidOutput, IItemStack resource, ILiquidStack fluidInput, int fermentationValue, float fluidOutputModifier);
# mods.forestry.Fermenter.removeRecipe(IIngredient input);

# mods.forestry.Moistener.addRecipe(IItemStack output, IItemStack input, int packagingTime); 
# mods.forestry.Moistener.removeRecipe(IIngredient output);

# mods.forestry.Squeezer.addRecipe(ILiquidStack fluidOutput, IItemStack[] ingredients, int timePerItem, @Optional WeightedItemStack itemOutput);
# mods.forestry.Squeezer.removeRecipe(ILiquidStack liquid, @Optional IIngredient[] ingredients);

# mods.forestry.Still.addRecipe(ILiquidStack fluidOutput, ILiquidStack fluidInput, int timePerUnit);
# mods.forestry.Still.removeRecipe(ILiquidStack output, @Optional ILiquidStack fluidInput);

# mods.forestry.ThermionicFabricator.addCast(IItemStack output, IIngredient[][] ingredients, ILiquidStack liquidStack, @Optional IItemStack plan);
# mods.forestry.ThermionicFabricator.removeCast(IIngredient product);

# mods.forestry.ThermionicFabricator.addSmelting(ILiquidStack liquidStack, IItemStack itemInput, int meltingPoint);
# mods.forestry.ThermionicFabricator.removeSmelting(IIngredient itemInput);
