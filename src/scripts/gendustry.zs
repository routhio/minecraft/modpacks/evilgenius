# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import crafttweaker.item.IItemStack as IItemStack;
import mods.jei.JEI.removeAndHide as rh;

print(">>> loading gendustry.zs");

# recipe removals for gendustry is done in its own configuration file

# Mutagen Tank
recipes.addShaped("gendustry mutagen tank", <gendustry:mutagen_tank>,
    [[<ore:ingotTin>, <ore:paneGlassColorless>, <ore:ingotTin>],
     [<ore:ingotTin>, <thermalexpansion:tank:*>, <ore:ingotTin>],
     [<ore:ingotTin>, <ore:paneGlassColorless>, <ore:ingotTin>]]);

# Power Module
recipes.addShaped("gendustry power module", <gendustry:power_module>,
    [[<ore:gearBronze>, <ore:ingotGold>, <ore:gearBronze>],
     [<minecraft:piston>, <thermalexpansion:frame:128>, <minecraft:piston>],
     [<ore:gearBronze>, <ore:ingotGold>, <ore:gearBronze>]]);


# Bee Receptable
recipes.addShaped("gendustry bee receptacle", <gendustry:bee_receptacle>,
    [[<ore:ingotBronze>, <ore:ingotBronze>, <ore:ingotBronze>],
    [<ore:ingotBronze>, <forestry:bee_drone_ge:*>, <ore:ingotBronze>],
    [<ore:dustRedstone>, <minecraft:light_weighted_pressure_plate>, <ore:dustRedstone>]]);

# Genetics processor
recipes.addShaped("gendustry genetics processor", <gendustry:genetics_processor>,
    [[<ore:gemDiamond>, <ore:gemQuartz>, <ore:gemDiamond>],
     [<ore:dustRedstone>, <refinedstorage:processor:4>, <ore:dustRedstone>],
     [<ore:gemDiamond>, <ore:gemQuartz>, <ore:gemDiamond>]]);

# Environmental processor
recipes.addShaped("gendustry env processor", <gendustry:env_processor>,
    [[<ore:gemDiamond>, <ore:gemLapis>, <ore:gemDiamond>],
     [<ore:gemLapis>, <refinedstorage:processor:4>, <ore:gemLapis>],
     [<ore:gemDiamond>, <ore:gemLapis>, <minecraft:diamond>]]);

# Upgrade frame
recipes.addShaped("gendustry upgrade frame", <gendustry:upgrade_frame>,
    [[<thermalfoundation:material:129>, <minecraft:gold_nugget>, <thermalfoundation:material:129>],
     [<minecraft:redstone>, <thermalexpansion:frame:64>, <minecraft:redstone>],
     [<thermalfoundation:material:129>, <minecraft:gold_nugget>, <thermalfoundation:material:129>]]);
