# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading recipeconflicts.zs");

val marble = <ore:stoneMarble>;
	
# Pam's Cotton
recipes.remove(<harvestcraft:cottonitem>);

# Pam's Market
recipes.remove(<harvestcraft:market>);
recipes.addShaped("harvestrcraft market", <harvestcraft:market>,
  [[<ore:logWood>, <ore:logWood>, <ore:logWood>],
   [<ore:logWood>, <ore:blockEmerald>, <ore:logWood>],
   [<ore:plankWood>, <ore:wool>, <ore:plankWood>]]);
		
# Peking Duck
recipes.remove(<harvestcraft:pekingduckitem>);
recipes.addShapeless("Peking Duck", <harvestcraft:pekingduckitem>, 
  [<ore:toolBakeware>, <ore:listAllduckraw>, <harvestcraft:onionitem>, 
   <harvestcraft:garlicitem>, <ore:cropRice>, <minecraft:apple>, 
   <minecraft:carrot>, <harvestcraft:gingeritem>]);

# Soft Pretzel
recipes.remove(<harvestcraft:softpretzelitem>);
recipes.addShapeless("Soft Pretzel", <harvestcraft:softpretzelitem>, 
  [<ore:toolBakeware>, <ore:foodDough>, <ore:foodButter>, 
   <ore:itemSalt>, <ore:itemSalt>]);
			
# Rustic Crop Stake
recipes.remove(<rustic:crop_stake>);
recipes.addShapedMirrored("Rustic Crop Stake", <rustic:crop_stake>, 
  [[null, null, <ore:stickWood>],
   [null, <ore:stickWood>, null], 
   [<ore:stickWood>, null, null]]);
	
# Rustic Iron Lattice
recipes.remove(<rustic:iron_lattice>);
recipes.addShaped("Rustic Iron Lattice", <rustic:iron_lattice> * 8, 
  [[null, <ore:stickIron>, null],
   [<ore:stickIron>, <ore:stickIron>, <ore:stickIron>], 
   [null, <ore:stickIron>, null]]);
	
# *======= Metal Blocks =======*

recipes.remove(<forestry:resource_storage:3>);
recipes.remove(<forestry:resource_storage:1>);
recipes.remove(<forestry:resource_storage:2>);
recipes.remove(<plustic:invarblock>);

