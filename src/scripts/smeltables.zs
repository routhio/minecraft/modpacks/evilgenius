# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading smeltables.zs");

furnace.addRecipe(<thermalfoundation:material:160>, <thermalfoundation:material:96>);

furnace.setFuel(<ore:dustSulfur>, 300);
furnace.setFuel(<thermalfoundation:material:832>, 800);
furnace.setFuel(<forestry:resource_storage>, 500);
furnace.setFuel(<forestry:apatite>, 50);
