# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import crafttweaker.item.IItemStack as IItemStack;
import mods.jei.JEI.removeAndHide as rh;

print(">>> loading xnet.zs");

# Cables
recipes.remove(<xnet:netcable>);
recipes.remove(<xnet:netcable:1>);
recipes.remove(<xnet:netcable:2>);
recipes.remove(<xnet:netcable:3>);
recipes.addShaped("blue xnet cable", <xnet:netcable>.withTag({display: {LocName: "tile.xnet.netcable_blue.name"}}) * 16, 
    [[<ore:plateGold>, <ore:dyeBlue>, <ore:plateGold>],
     [<thermaldynamics:duct_16:1>, <thermaldynamics:duct_0>, <thermaldynamics:duct_32:1>],
      [<ore:plateGold>, <ore:dyeBlue>, <ore:plateGold>]]);
recipes.addShaped("red xnet cable", <xnet:netcable:1>.withTag({display: {LocName: "tile.xnet.netcable_red.name"}}) * 16, 
    [[<ore:plateGold>, <ore:dyeRed>, <ore:plateGold>],
     [<thermaldynamics:duct_16:1>, <thermaldynamics:duct_0>, <thermaldynamics:duct_32:1>], 
     [<ore:plateGold>, <ore:dyeRed>, <ore:plateGold>]]);
recipes.addShaped("yellow xnet cable", <xnet:netcable:2>.withTag({display: {LocName: "tile.xnet.netcable_yellow.name"}}) * 16, 
    [[<ore:plateGold>, <ore:dyeYellow>, <ore:plateGold>],
     [<thermaldynamics:duct_16:1>, <thermaldynamics:duct_0>, <thermaldynamics:duct_32:1>], 
     [<ore:plateGold>, <ore:dyeYellow>, <ore:plateGold>]]);
recipes.addShaped("green xnet cable", <xnet:netcable:3>.withTag({display: {LocName: "tile.xnet.netcable_green.name"}}) * 16, 
    [[<ore:plateGold>, <ore:dyeGreen>, <ore:plateGold>],
     [<thermaldynamics:duct_16:1>, <thermaldynamics:duct_0>, <thermaldynamics:duct_32:1>], 
     [<ore:plateGold>, <ore:dyeGreen>, <ore:plateGold>]]);

# Connectors
recipes.remove(<xnet:connector>);
recipes.remove(<xnet:connector:1>);
recipes.remove(<xnet:connector:2>);
recipes.remove(<xnet:connector:3>);
recipes.addShaped("blue xnet connector", <xnet:connector>.withTag({display: {LocName: "tile.xnet.connector_blue.name"}}), 
    [[<ore:dyeBlue>, <ore:chest>, <ore:dyeBlue>],
     [<ore:dustRedstone>, <ore:plateGold>, <ore:dustRedstone>], 
     [<ore:dyeBlue>, <thermaldynamics:servo>, <ore:dyeBlue>]]);
recipes.addShaped("red xnet connector", <xnet:connector:1>.withTag({display: {LocName: "tile.xnet.connector_red.name"}}), 
    [[<ore:dyeRed>, <ore:chest>, <ore:dyeRed>],
     [<ore:dustRedstone>, <ore:plateGold>, <ore:dustRedstone>], 
     [<ore:dyeRed>, <thermaldynamics:servo>, <ore:dyeRed>]]);
recipes.addShaped("yellow xnet connector", <xnet:connector:2>.withTag({display: {LocName: "tile.xnet.connector_yellow.name"}}), 
    [[<ore:dyeYellow>, <ore:chest>, <ore:dyeYellow>],
     [<ore:dustRedstone>, <ore:plateGold>, <ore:dustRedstone>], 
     [<ore:dyeYellow>, <thermaldynamics:servo>, <ore:dyeYellow>]]);
recipes.addShaped("green xnet connector", <xnet:connector:3>.withTag({display: {LocName: "tile.xnet.connector_green.name"}}), 
    [[<ore:dyeGreen>, <ore:chest>, <ore:dyeGreen>],
     [<ore:dustRedstone>, <ore:plateGold>, <ore:dustRedstone>], 
     [<ore:dyeGreen>, <thermaldynamics:servo>, <ore:dyeGreen>]]);

# Controller
recipes.remove(<xnet:controller>);
recipes.addShaped("xnet controller", <xnet:controller>, 
    [[<minecraft:repeater>, <minecraft:comparator>, <minecraft:repeater>],
     [<ore:blockRedstone>, <nuclearcraft:part:10>, <ore:blockRedstone>], 
     [<ore:plateIron>, <ore:plateGold>, <ore:plateIron>]]);
     