# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading chisel.zs");

mods.chisel.Carving.addVariation("basalt", <quark:basalt:1>);
mods.chisel.Carving.addVariation("basalt", <quark:basalt>);
